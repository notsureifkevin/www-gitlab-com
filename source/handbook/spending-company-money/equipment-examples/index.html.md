---
layout: markdown_page
title: "Spending Company Money - Equipment Examples"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Adapters and cables

#### USB adapters
  * TOTU 8-in-1 USB-C Hub - [US](http://a.co/d/8V80kOP)
  * FLYLAND Hub, 9-in-1 - [Germany](https://www.amazon.de/dp/B00OJY12BY/ref=cm_sw_r_tw_dp_U_x_lvgaCb2Y6M9YV)
  * VAVA USB-C Hub 8-in-1 Adapter - [Australia](https://www.amazon.com.au/dp/B07JCKCZGJ/ref=cm_sw_r_cp_ep)
  * UGREEN Ethernet to USB 3.0 Adapter - [US](http://a.co/d/1hO4hO4)
  * Yinboti USB-C Hub for New Macbook Pros - [US](https://www.amazon.com/gp/product/B07FMNJC6J/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)
  * Kensington UH4000 4 Port USB Hub 3.0 - [US](https://www.amazon.com/Kensington-UH4000-Port-USB-3-0/dp/B00O9RPP28/)
  * YXwin USB C Hub 6-in-1 Adapter including Ethernet - [UK](https://www.amazon.co.uk/YXwin-Adapter-Delivery-1000mbps-Ethernet/dp/B07PSM6RQS)

#### USB Docks
  * CalDigit TS3 Plus (**Will require Manager Approval to expense due to cost**) - [US](http://shop.caldigit.com/us/index.php?route=product/product&product_id=170), [UK](http://shop.caldigit.com/uk/index.php?route=product/product&product_id=174)
    * Enables *stable* Dual Monitor Support for Engineers
      * Extended Desktop Support for DVI monitors requires ['active' displayport adaptors](http://www.cablematters.com/pc-33-33-cable-matters-gold-plated-displayport-to-dvi-male-to-female-adapter.aspx)
      * MacOS does not support [Multi-Stream Transport over DisplayPort](https://www.displayport.org/cables/driving-multiple-displays-from-a-single-displayport-output/)
    * Recharges Laptop over USB-C
    * Provides USB-A support for peripherals

#### Cables
  * AmazonBasics High-speed HDMI Cable - [US](http://a.co/d/acNQ9ij)
  * Rankie DisplayPort Cable - [UK](https://www.amazon.co.uk/gp/product/B00YOP0T7G)

### Notebook carrying bags
  * tomtoc 360° Protective Sleeve - [US](http://a.co/d/fGoBGYK)
  * NIDOO 15" - [Germany](https://www.amazon.de/dp/B072LVYC91/ref=cm_sw_r_tw_dp_U_x_eCgaCb15Q5S7Q)
  * Mosiso Sleeve - [Australia](https://www.amazon.com.au/dp/B01N0W1YIK/ref=cm_sw_r_cp_ep_dp_FDgaCb200161T)
  * SLOTRA Slim Anti-Theft Laptop Backpack - [UK](https://www.amazon.co.uk/SLOTRA-Lightweight-Resistant-Multipurpose-Anti-Theft/dp/B01DKLOOLG)

### Monitors

#### Desktop monitors
  * Acer S242HLDBID 24" - [Germany](https://www.amazon.de/dp/B01AJTVCA8/)
  * ASUS VS248H-P 24" 1080p - [US](https://www.amazon.com/dp/B0058UUR6E/)
  * ASUS PB277Q 27" 1440p - [US](https://www.amazon.com/gp/product/B01EN3Z7QQ/)
  * LG 27UD58-B 27" 4K - [US](https://www.amazon.com/dp/B01IRQAYPE)
  * SAMSUNG F350 23.6" 1080p - [Germany](https://www.amazon.com.au/dp/B0771J3HXV/)
  * Lenovo ThinkVision P27h-10 27" 1440p - [Switzerland](https://www.digitec.ch/de/s1/product/lenovo-thinkvision-p27h-10-27-2560-x-1440-pixels-monitor-6611407)
    * Connects over USB-C and also acts as a hub with 4 USB3.0 ports (on the back), works great on Linux including audio passthrough!

#### Portable monitors
  * USB Touchscreen, 11.6" - [US](http://a.co/d/8pkwPSr)
  * Kenowa 15.6" - [Germany](https://www.amazon.de/dp/B07FZ5PNDV/ref=cm_sw_r_tw_dp_U_x_hRgaCb2K3A8BD)
  * Asus Zenscreen 15,6" - [Netherlands](https://www.coolblue.nl/product/787645/asus-zenscreen-mb16ac.html)

#### Privacy screens
  * Macbook Pro 15 inch - [US](https://www.amazon.com/gp/product/B07GV71FF5/)
  * Macbook Pro 13 inch - [US](https://www.amazon.com/gp/product/B07GV71FF5/)

### Headphones and earbuds
  * Mpow 059 Bluetooth Over Ear Headphones - [US](https://www.amazon.com/dp/B077XT82DD/ref=cm_sw_r_tw_dp_U_x_7VgaCbHHH1318)
  * JBL T450BT On-ear Bluetooth Headphones - [Germany](https://www.amazon.de/dp/B01M6WNWR6/ref=cm_sw_r_tw_dp_U_x_pXgaCb1RYXQK4)
  * Apple AirPods (though battery life may be limited) - [US](https://www.apple.com/shop/accessories/all-accessories/headphones-speakers)  

Note: Open ear headphones can often be worn longer than in-ear or closed headphones.

### Office Furniture

#### Desks
  * Autonomous SmartDesk 2 - Home Edition - [US and Europe](https://www.autonomous.ai/standing-desks/smart-desk-2-home)
  * Jarvis electric adjustable height desk - [US and Canada](https://www.fully.com/standing-desks/jarvis.html)

#### Chairs
  * Hbada Ergonomic Office Chair - [US](https://www.amazon.com/dp/B01N0XPBB3/ref=cm_sw_r_tw_dp_U_x_73gaCbMT53PW5)
  * INTEY Ergonomic Office Chair - [Germany](https://www.amazon.de/dp/B0744GS6LR/ref=cm_sw_r_tw_dp_U_x_94gaCbG9F1CRB)
  * Kolina Ergonic Mesh Office Chair - [Australia](https://www.amazon.com.au/dp/B07BK7XDV8/ref=cm_sw_r_tw_dp_U_x_x6gaCbH91QM8K )
  * IKEA MARKUS Office Chair - [UK](https://www.ikea.com/gb/en/products/chairs-stools-benches/desk-chairs/markus-office-chair-glose-black-art-20103101)

#### Laptop Stands
  * BoYata Adjustable Laptop Stand - [UK](https://www.amazon.co.uk/gp/product/B07H89V3BB)
  * Roost Laptop Stand - [Worldwide](https://www.therooststand.com/)

#### Wrist Rests
  * GIM Wrist Rest Set - [UK](https://www.amazon.co.uk/Keyboard-GIM-Support-Ergonomic-Computer/dp/B072K41FC1)

### Something else?
  * No problem! Consider adding it to this list if others can benefit from it.
