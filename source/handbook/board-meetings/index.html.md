---
layout: markdown_page
title: "Board of Directors and Governance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Governance Documents

1. [Amended and Restated Bylaws of GitLab Inc. (January 31, 2019)](/handbook/board-meetings/bylaws.html)

## Board Selection Process

### Roles and Responsibilities
* [Candidate Experience Manager](/job-families/people-ops/candidate-experience-specialist/) - Owner of the overall process, coordination, and documentation. Drafting communication to potential board members on behalf of CEO (selected and decline among others). Email communication with Nomination and Governance Committee when questions arise, guidance is needed.
* [CEO's Executive Assistant](/job-families/people-ops/executive-assistant/) - Scheduling, committee tracking.
* [VP of Legal](/job-families/legal/vp-legal-commercial-ip-compliance/) - Nomination and Governance Committee Secretary.
* [Nomination and Governance Committee](/handbook/board-meetings/#nominations-and-governance) - Adding recommendations to the GitLab Board of Directors sheet, meeting with potential new board members to vet. Email Candidate Experience Manager notes after meetings, including thoughts and next steps. Setting requirements for potential board members and position on the board/committee priority.
* GitLab Executives - Meeting with potential new board members to vet. Email Candidate Experience Manager notes after meetings, including thoughts and next steps.

### Interview Process

#### Audit Chair
1. First meeting with the nominating board member;  50-minute meeting.
1. CEO 50 minute meeting.
1. GitLab C-Suite: CRO, CFO.
1. Other Board Members: Sue, Matt and Bruce.
1. Any additional dictated by the position they will fill and if they request to speak to anyone.

#### Independent - former/current CEO
1. First meeting with the nominating board member;  50-minute meeting.
1. CEO, Matt, Sue; 50 minute meetings.
1. GitLab C-Suite: CRO, CFO, and other executives as requested
1. Other Board Members: Bruce, Karen and Dave.
1. Any additional dictated by the position they will fill and if they request to speak to anyone.

### What is Required Going Forward to be Successful
* Cc Candidate Experience Manager and CEO's Executive Assistant on all correspondence related to the nomination and governance committee.
* Monthly 25-minute meetings through IPO.
* Continue to define requirements for individuals to be considered for the board.
    * [GitLab Handbook](/handbook/board-meetings/#nomination-and-governance-committee-charter-adopted-2019-01-31) see point 3
* Establish Board On-Boarding.

## Board Renewal Process

The board as a whole determines which class each director will be a part of.
Assuming three classes, director can belong to class 1, class 2 or class 3.
Some directors will be in class 1 and will be up for reelection as soon as 1 year after [going public](/handbook/being-a-public-company/), while others will not be up for reelection until 3 years after going public.
Typically the decision as to who will go into which class of director is done very close to the IPO.

The decision is based on the preferences of all the directors as to when they would prefer to leave the board.

For example, some VC directors may prefer to leave the board earlier in the
process as they may liquidate their positions over the couple of years
following the IPO.

## Board Meeting Process
For those preparing materials or involved in running the meeting, consider reviewing [How to run an Effective Board Meeting and make an Effective Board Deck](http://delian.io/lessons-4) by Keith Rabois.

1. The CFO is responsible for scheduling the meeting, preparing the agenda and recording minutes.
1. Collaborate on public webpages such as [/strategy](/company/strategy/) as much as possible.
1. Financial information and other non public items go into a shared Google Sheet and/or Google Presentation.
1. The CFO will send a reminder to those who are requested to prepare materials two weeks in advance of the meeting.
1. Final draft presentations are due one week prior to the meeting.
1. Board materials are distributed the friday before the meeting.
1. There will be two deep dives on the agenda for each board meeting:
    - 30 minutes allotted to a single functional area (i.e. sales, marketing, engineering, product, g&a, etc) on a rotation basis
    - 30 minutes allotted to a topic(s) of strategic or operational importance to the Company.
1. Discussion points are clearly marked, each get a time allotment, five minutes unless otherwise approved.
1. Board members are assumed to have studied the materials.
1. For now the whole executive team is present during the meeting.
1. There is a closed session at the end to cover administrative items and board only discussion.
1. No presentation during the meeting, only discussion items and conversation about unclear items.
1. Follow up with updated materials.

The board meeting is [all-remote](/company/culture/all-remote/) because half remote is a bad experience for remote participants, see [video calls](/handbook/communication/#video-calls) point 10.


## Board and Committee Composition

* [Board of Directors Job Description](/job-families/board-of-directors/board_member/)

### Board of Directors

**Members:** Sid Sijbrandij, Larry Augustin, Bruce Armstrong, Matthew Jacobson, David Hornik, Sue Bostrom

### Audit Committee

**Members:** Bruce Armstrong, Larry Augustin, David Hornik
**Management DRI:** Chief Financial Officer

### Compensation Committee

**Members:** Bruce Armstrong, Matthew Jacobson, Sue Bostrom
**Management DRI:** Chief People Officer

### Nominations and Governance Committee

**Members:** Sid Sijbrandij, Matthew Jacobson, Sue Bostrom
**Management DRI:** Chief of Staff

## Schedule
1. Board of Directors meetings are held quarterly and they are all remote, everyone joins with their own video conference setup from a separate location.
1. Meetings are scheduled as close to the fourth thursday following the end of the quarter, depending on availability of the directors.
1. The 2019 schedule of board meetings is as follows:
* 2019-01-31
* 2019-04-25
* 2019-09-13
* 2019-12-03

EA shall ensure that there are three calendar invites for all attendees (in each session) that include the following:
* Exact meeting time blocked (ie: Start at 9am PST, End at 5pm PST)
* Zoom Link
* Agenda (the agenda should also include the zoom link at the top) or presentation
* Notes doc (should be linked under the agenda in the invite)

The management DRI shall ensure the following:
* meeting has an agenda
* required attendees have been invited
* materials circulated to committee members at least 72 hours in advance of meeting
* notes are taken during the meeting
* minutes are recorded, signed and forwarded to the Corporate Secretary for filing

### Audit Committee Agenda Planner

We review the below topics no less frequently than the schedule below.


Management - Accounting and Reporting

|  Topics                                                                               | FY Q1 | FY Q2  |  FY Q3  |  FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| Accounting Policies                                                                        |    |  X   |           |  X     |
| Significant estimates and judgements                                 |          |           |     | X|
| New accounting standards – impact and implementation plan            |        X |    X       | X | X      |
| Review of Financial Statements                                       |        X   |           |   |  X    |
| Related Party Transactions                                           |          |           |   |  X    |
| Global staffing update, succession plan and continuous improvement   |          |           |   |   X   |

 Management - Risk and Compliance

| Topics                          | FY Q1  |   FY Q2  |  FY Q3 | FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| Compliance to business conduct (including hotline complaints and code of conduct violations) |        |           | |   X |
| ERM – Risk assessment updates                              |          |     |  X    |       |
| ERM – Cyber risk assessment         |         |         |    |   X  |
| ERM – Review of financial statement  risk factors                                     | X         |           |  |   X  |
| Insurance coverage update                                       |          |           |   | X   |
| Regulatory Compliance  |          |   |    X  |      |

 Internal Audit

| Topics                          | FY Q1 | FY Q2 |  FY Q3  |  FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| Internal audit and global annual plan                           |        X         |        |          X |      |
| Internal audit activity report and annual plan update            |                  | X         |     X      |   X  |
| Internal control over financial reporting assessment and deficiencies status update  |    X | X      | X     |  X       |
| Internal audit charter review                                     |         |   X     |   |   X    |
| Fraud Risk assessment                                    |          |           |      | X|
| Annual assessment of internal audit   |          |           |        | X|


 External Audit

| Topics                          | FY Q1 | FY Q2  |   FY Q3  |  FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| Global audit plan and fees/Appoint External Auditor | X| | |X |
| Year-end audit results and required communications                                         |       |  |    X    | X |
| Annual assessment of audit firm, engagement team and lead audit partner                               |          | |        |    X   |
| Independence review | | | | X |

 General

| Topics                          | FY Q1 | FY Q2  |  FY Q3  |  FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| Executive session - as needed  | X | X | X |X |
| Approval of minutes                                        |X  | X     | X        |X      |
| Committee annual assessment                            |          |           |      |X|
| Private session as needed with CFO, CIO, tax leader, legal, internal audit, external audit        |X |    X | X     |X         |


 Special topics and deep dives (on an “as need basis”)

| Topics                          | FY Q1 | FY Q2  |   FY Q3  |  FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| IT implementation projects and initiatives                                   |      |        |           |   X   |
| IT security update                              |          |           |    | X  |
| Income taxes       |         |  |    X   |         |
| Treasury                                 |          |           | |    X  |
| Regulatory environment                                      |          | | X       |       |
| Payroll and hiring |          |           |     | X |



## Quarterly Q&A with the Board
We will have one board member per quarter conduct an AMA session with the GitLab team.

## References

1. [AVC post](http://avc.com/2016/02/do-you-want-better-board-meetings-then-work-the-phone/)
1. [AVC comment](http://avc.com/2016/02/do-you-want-better-board-meetings-then-work-the-phone/#comment-2489615046)
1. [Techcrunch article](http://techcrunch.com/2016/02/01/1270130/)


## Audit Committee Charter (adopted 2018-04-26)

1.	Purpose. The purpose of the Audit Committee of the Board of Directors (the “Board”) of GitLab Inc. (the “Company”) is to assist the Board’s oversight of:
    - The integrity of the Company’s financial statements;
    - The performance, qualifications and independence of the Company’s registered public accounting firm (the “external auditors”);
    - The performance of the Company’s internal financial, accounting and reporting controls and other processes.
    - The company's process for monitoring compliance with laws and regulations and the code of conduct.

1.	Structure and Membership
    - Members. the Audit Committee shall consist of at least two members of the Board, each of whom shall be independent.
    - Financial Literacy. Each member of the Audit Committee must be financially literate, as such qualification is interpreted by the Board in its business judgment, or must become financially literate within a reasonable period of time after his or her appointment to the Audit Committee.
    - Chair. Unless the Board elects a Chair of the Audit Committee, the Audit Committee shall elect a Chair by majority vote.
    - Selection and Removal. Members of the Audit Committee shall be appointed by the Board.
1.	Authority and Responsibilities
    - General. The Audit Committee shall discharge its responsibilities, and shall assess the information provided by the Company’s management and the external auditors, in accordance with its business judgment. Management is responsible for the preparation, presentation, and integrity of the Company’s financial statements and for the appropriateness of the accounting principles and reporting policies that are used by the Company. The external auditors are responsible for auditing the Company’s financial statements. The authority and responsibilities set forth in this Charter do not reflect or create any duty or obligation of the Audit Committee to plan or conduct any audit, to determine or certify that the Company’s financial statements are complete, accurate, fairly presented, or in accordance with generally accepted accounting principles or applicable law, or to guarantee the external auditors’ reports.
    - Oversight of Integrity of Financial Statements
    - Review and Discussion. The Audit Committee shall meet to review and discuss with the Company’s management and external auditors the Company’s audited financial statements.
    - Related-Person Transactions. The Audit Committee shall review related-person transactions under the Company’s Related Person Transaction Policy and applicable accounting standards on an ongoing basis and such transactions shall be approved by the Audit Committee.
    - Oversight of Performance, Qualification and Independence of External Auditors
    - Consider the effectiveness of the company's internal control system, including information technology security and control.  
    - Understand the scope of internal and external auditors' review of internal control over financial reporting, and obtain reports on significant findings and recommendations, together with management's responses.
    - Review fraud risk assessment of the entity
    - Approve the internal audit charter
    - Approve the annual audit plan and all major changes to the plan. Review the internal audit activity’s performance relative to its plan.  
    - Review the effectiveness of the system for monitoring compliance with laws and regulations and the results of management's investigation and follow-up (including disciplinary action) of any instances of noncompliance.  
    - Review the findings of any examinations by regulatory agencies, and any auditor observations.  
    - Review the process for communicating the code of conduct to company personnel, and for monitoring compliance therewith.  
    - Obtain regular updates from management and company legal counsel regarding compliance matters.
    - Review and assess the adequacy of the audit committee charter annually, requesting board approval for proposed changes, and ensure appropriate disclosure as may be required by law or regulation.
    - Review new accounting standards- impact and implementation plan, code of conduct violations including hotline complaints , cybersecurity risk assessment conducted by management , Key contracts and IT initiatives taken by management



1.	Selection. The Audit Committee shall be responsible for appointing, evaluating and, when necessary, terminating the engagement of the external auditors. The Audit Committee may, in its discretion, seek stockholder ratification of the external auditors it appoints.
1.	Independence. The Audit Committee shall assist the Board in its assessment of the independence of the external auditors. In connection with this assessment, the Audit Committee shall, at least annually, obtain and review a report from the external auditors describing relationships between the external auditors and the Company, including the disclosures required by the applicable requirements of the Public Company Accounting Oversight Board regarding the external auditors’ independence. The Audit Committee shall actively engage in dialogue with the external auditors concerning any disclosed relationships or services that might impact the objectivity and independence of the external auditors.
1.	Compensation. The Audit Committee shall be directly responsible for setting the compensation of the external auditors. The Audit Committee is empowered, without further action by the Board, to cause the Company to pay the compensation of the external auditors established by the Audit Committee.
1.	Oversight. The external auditors shall report directly to the Audit Committee and the Audit Committee shall be directly responsible for overseeing the work of the external auditors, including resolution of disagreements between Company management and the external auditors regarding financial reporting.
1.	Procedures and Administration
    - Meetings. The Audit Committee shall meet in person or telephonically as often as it deems necessary in order to perform its responsibilities. The Audit Committee may also act by unanimous written consent in lieu of a meeting. The Audit Committee shall periodically meet separately with: (i) the external auditors and (ii) Company management. The Audit Committee shall keep minutes of its meetings and provide those to the Board of Directors.
1. 	Independent Advisors. The Audit Committee shall have the authority, without further action by the Board, to engage and determine funding for such independent legal, accounting and other advisors as it deems necessary or appropriate to carry out its responsibilities. Such independent advisors may be the regular advisors to the Company. The Audit Committee is empowered, without further action by the Board, to cause the Company to pay the compensation of such advisors as established by the Audit Committee.
1.	Investigations. The Audit Committee shall have the authority to conduct or authorize investigations into any matter within the scope of its responsibilities, as it shall deem appropriate, including the authority to request any officer, employee or advisor of the Company to meet with the Audit Committee or any advisors engaged by the Audit Committee.
1.	Additional Powers. The Audit Committee shall have such other duties as may be delegated.

## Compensation Committee Charter (adopted 2018-04-26)
1.	Purpose
The purpose of the Compensation Committee of the Board of Directors (the “Board”) of GitLab Inc. (the “Company”) is to assist the Board in the performance of its responsibilities relating to the Company’s compensation programs in general and specifically, but not limited to, its’ executive officers.
1.	Structure and Membership
    - Number. The Compensation Committee shall consist of at least two members of the Board.
    - Independence. At least two members of the Compensation Committee shall not have management responsibilities.
    - Chair. Unless the Board elects a Chair of the Compensation Committee, the Compensation Committee shall elect a Chair by majority vote.
    - Compensation. The compensation of Compensation Committee members shall be as determined by the Board.
    - Selection and Removal. Members of the Compensation Committee shall be appointed by the Board. The Board may remove members of the Compensation Committee from such committee, with or without cause, at any time that it determines to do so.
1.	Authority and Responsibilities
    - General. The Compensation Committee shall perform its responsibilities, and shall assess the information provided by the Company's management, in accordance with its business judgment.
    - Compensation Matters
    - CEO Compensation and Performance. The Compensation Committee shall annually review and approve corporate goals and objectives relevant to the compensation of the Company’s Chief Executive Officer (the “CEO”), evaluate the CEO’s performance in light of those goals and objectives, and, either as a committee or together with the other independent directors (as directed from time to time by the Board), determine and approve the CEO’s compensation based on this evaluation.
    - Executive Officer Compensation. The Compensation Committee shall review and approve, or recommend for approval by the Board, executive officer (including the CEO) compensation, including salary, bonus and incentive compensation levels; deferred compensation; executive perquisites; equity compensation (including awards to induce employment); severance arrangements;
change-in-control benefits and other forms of executive officer compensation. The Compensation Committee shall meet without the presence of executive officers when approving or deliberating on CEO compensation but may, in its discretion, invite the CEO to be present during approval of, or deliberations with respect to, other executive officer compensation.
1.  Plan Recommendations and Approvals. The Compensation Committee shall periodically review and make recommendations to the Board with respect to incentive-compensation plans and equity-based plans that are subject to approval by the Board.
1.  Director Compensation. The Compensation Committee shall periodically review and make recommendations to the Board of Directors with respect to director compensation.
1.  Additional Powers. The Compensation Committee shall take such other action with respect to compensation matters as may be delegated from time to time by the Board.
1.  Procedures and Administration
    - Meetings. The Compensation Committee shall meet in person or telephonically as often as it deems necessary in order to perform its responsibilities. The Compensation Committee may also act by unanimous written consent in lieu of a meeting. The Compensation Committee shall keep such records of its meetings and furnish the minutes of such meetings to the Board of Directors.
    - Charter. The Compensation Committee shall periodically review and reassess the adequacy of this Charter and recommend any proposed changes to the Board for approval.
    - Compensation Consultants, Legal Counsel and Other Advisors. The Compensation Committee may, in its sole discretion, retain, terminate or obtain the advice of compensation consultants, legal counsel or other advisors. The Compensation Committee shall be directly responsible for the appointment, compensation and oversight of the work of any compensation consultant, legal counsel and other advisor retained by the Compensation Committee. The Compensation Committee is empowered, without further action by the Board, to cause the Company to pay the compensation, as determined by the Compensation Committee, of any
compensation consultant, legal counsel and other advisor retained by the Compensation Committee. The Compensation Committee may select, or receive advice from, a compensation consultant, legal counsel or other advisor, only after taking into consideration, as applicable, all factors relevant to that person’s independence from management.
1.	Investigations. The Compensation Committee shall have the authority to conduct or authorize investigations into any matters within the scope of its responsibilities as it shall deem appropriate, including the authority to request any officer, employee or advisor of the Company to meet with the Compensation Committee or any advisors engaged by the Compensation Committee.

## Nomination and Governance Committee Charter (adopted 2019-01-31)
1. Purpose
The purpose of the Nominating and Governance Committee (the “Committee”) of the Board of Directors (the “Board”) of GITLAB, INC. (the “Company”) is to ensure that the Board is properly constituted to meet its fiduciary obligations to stockholders and the Company, and to assist the Board with respect to corporate governance matters, including:
    - identifying, considering and nominating candidates for membership on the Board; and
    - advising the Board on corporate governance matters and Board performance matters, including recommendations regarding the structure and composition of the Board and Board committees.
This charter (the “Charter”) sets forth the authority and responsibilities of the Committee in fulfilling its purpose.
1. Structure and Membership
The Committee will consist of two or more members of the Board, with the exact number determined from time to time by the Board.  Each member of the Committee will:
    - be free from any relationship that, in the opinion of the Board, would interfere with the exercise of independent judgment as a Committee member; and
    - meet any other requirements imposed by applicable law, regulations or rules, subject to any applicable exemptions.
All members of the Committee will be appointed by, and will serve at the discretion of, the Board.  The Board may appoint a member of the Committee to serve as the chairperson of the Committee (the “Chair”).  If the Board does not appoint a Chair, the Committee members may designate a Chair by their majority vote.  The Chair will work with management to set the agenda for Committee meetings and conduct the proceedings of those meetings.
1. Authority and Responsibilities
The principal responsibilities and duties of the Committee in serving the purposes outlined in Section I of this Charter are set forth below. Autho These duties are set forth as a guide, with the understanding that the Committee will carry them out in a manner that is appropriate given the Company’s needs and circumstances.  The Committee may supplement them as appropriate and may establish policies and procedures from time to time that it deems necessary or advisable in fulfilling its responsibilities.
The responsibilities and authority of the Committee will include:
   - *Nominating Duties*:
     1.	Develop the director nomination processes.  Determine or recommend to the Board for determination the desired qualifications, expertise and characteristics of Board members, with the goal of developing a diverse, experienced and highly qualified Board.  On an ongoing basis, the Committee will consider Board composition factors, including independence, integrity, diversity, age, skills, financial and other expertise, breadth of experience, knowledge about the Company’s business or industry and willingness and ability to devote adequate time and effort to Board responsibilities in the context of the existing composition, other areas that are expected to contribute to the Board’s overall effectiveness and needs of the Board and its committees.
     2. Identify and recruit qualified candidates for Board membership, consistent with criteria approved by the Board.
     3. Oversee inquiries into the backgrounds and qualifications of potential candidates for membership on the Board, including review of the independence of the non-employee directors and members of the Committee, the Audit Committee, the Compensation Committee and other independent committees of the Board.
     4.	Propose recommendations as to the size of the Board.
   - Corporate Governance Duties
     1. Periodically review the business interests and business activities of members of the Board and management.
     2. Recommend that the Board establish special committees as may be desirable or necessary from time to time in order to address interested director, ethical, legal or other matters that may arise.
     3. Consider the Board’s leadership structure, including the separation of the Chairman and Chief Executive Officer roles and/or appointment of a lead independent director of the Board, either permanently or for specific purposes, and make such recommendations to the Board with respect thereto as the Committee deems appropriate.
     4. Make such recommendations to the Board and its committees as the Committee may consider necessary or appropriate and consistent with its purpose, and take such other actions and perform such other services as may be referred to it from time to time by the Board.
     5. From time to time, review this Charter and the Committee’s performance, and in the event that the Company intends to begin preparation for an initial public offering or as a result of such review, make recommendations to the Board regarding revisions to this Charter as appropriate.
1. Studies and Advisors
The Committee, in discharging its responsibilities, may conduct, direct, supervise or authorize studies of, or investigations into, matters within the Committee’s scope of responsibility, with full and unrestricted access to all books, records, documents, facilities and personnel of the Company.  The Committee has the sole authority and right, at the expense of the Company, to retain legal counsel and other consultants, accountants, experts and advisors of its choice to assist the Committee in connection with its functions, including any studies or investigations.  The Committee will have the sole authority to approve the fees and other retention terms of such advisors.  The Company will provide for appropriate funding, as determined by the Committee, for:
    -	payment of compensation to any search firm, legal counsel and other consultants, accountants, experts and advisors retained by the Committee; and
    -	ordinary administrative expenses of the Committee that are necessary and appropriate in carrying out its functions.
Irrespective of the retention of legal and other consultants, accountants, experts and other advisors to assist the Committee, the Committee shall exercise its own judgment in fulfillment of its functions.
1. Meetings, Actions Without A Meeting And Staff
The Committee will meet with such frequency as is determined appropriate by the Committee.  The Chair, in consultation with the other member(s) of the Committee, will set the dates, times and places of such meetings.  The Chair or any other member of the Committee may call meetings of the Committee by notice in accordance with the Company’s Bylaws.  A quorum of the Committee for the transaction of business will be a majority of its members.  Meetings may be held via tele- or video-conference.  The Committee may also act by unanimous written consent in lieu of a meeting in accordance with the Company’s Bylaws.  Subject to the requirements of this Charter, and applicable law, rules and regulations, the Committee and the Chair may invite any director, executive or employee of the Company, or such other person, as it deems appropriate in order to carry out its responsibilities, to attend and participate (in a non-voting capacity) in all or a portion of any Committee meeting.  The Committee may exclude from all or a portion of its meetings any person it deems appropriate in order to carry out its responsibilities.  The Chair will designate a secretary for each meeting, who need not be a member of the Committee.  The Company will provide the Committee such staff support as it may require.
1. Minutes and Reports
The Committee will maintain written minutes of its meetings and copies of its actions by written consent, and will make such minutes and copies of written consents available to the other members of the Board and cause them to be filed with the minutes of the meetings of the Board.  The Chair will report to the Board from time to time with respect to the activities of the Committee, including on significant matters related to the Committee’s responsibilities and the Committee’s deliberations and actions.
1. Delegation of Authority
The Committee may from time to time, as it deems appropriate and to the extent permitted under applicable law, and the Company’s Certificate of Incorporation and Bylaws, form and delegate authority to subcommittees.
1. Compensation
Members of the Committee will receive such fees, if any, for their service as Committee members as may be determined by the Board, which may include additional compensation for the Chair.
