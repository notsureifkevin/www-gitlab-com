---
layout: markdown_page
title: "Sales Onboarding"
---

## Sales Onboarding Process 

Sales onboarding at GitLab is focused on what new sales team members need to **KNOW**, **DO**, and **BE ABLE TO ARTICULATE** within their first month or so on the job so they are "customer conversation ready" and they are comfortable, competent, and confident leading discovery calls to begin building pipeline by or before the end of their first several weeks on the job. 

Topics covered include but are not limited to:
* Understanding the GitLab Buyer
* Understanding the Industry in Which We Compete
* Salesforce Basics
* Introduction to GitLab Sales Process & Tools
* Why GitLab and the GitLab Portfolio
* The Competition
* Critical Sales Skills & Behaviors
* GitLab Product Tiers

The process:
*  The GitLab Candidate Experience team initiates a general GitLab onboarding issue for every new GitLab team member
   - See the [People Ops onboarding issue template](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md)
*  In the ["Sales Division" section](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding_tasks/division_sales.md) of that issue, Sales Managers are instructed to 
   1.  Create a Sales Role-Based Access Request template (learn more [here](/handbook/business-ops/it-ops-team/access-requests/#sales-role-based-access-request-templates)) 
   1.  Create a new issue in the [Sales Onboarding Issue Tracker](https://gitlab.com/gitlab-com/sales-team/sales-onboarding/issues/) using the `sales_onboarding.md` template, and 
         - Note: This should be completed within the first 2 days of a new sales team member's start date
   1.  Notify the new sales team member that the above will be their sales onboarding training 
*  For an overview of the process, watch this [4.5 minute overview video](https://drive.google.com/open?id=1N56QJn7v1pUVtKoUtGZ62kfFLqJp7X0W)
*  Note: The above issue is focused on sales training; sales tool provisioning, account & territory assignments, and other Ops tasks remain in the general GitLab onboarding issue at this time in the "For Sales" section
*  Targeted roles for the sales onboarding training issue include Enterprise and Public Sector Strategic Account Leaders (SALs), Mid-Market Account Executives, SMB Customer Advocates, and Inside Sales Reps
   - Customer Success (e.g. Solution Architects, Technical Account Managers, and Professional Services Engineers) and Sales Development Rep (SDR) roles will continue to maintain their own separate onboarding process, but we will be collaborating and sharing best practices
*  In addition to the sales onboarding training issue, new sales team members participate in an interactive 3-day Sales QuickStart workshop full of mock calls, role plays, and group discussions. This format allows new sales team members to practice applying this new knowledge in a fun and challenging way. As a result, new sales team members exit Sales QuickStart more confident and capable in their ability to lead an effective discovery call with a customer or prospect. _(Note: The Results Value is our highest priority and we strive to deliver sales training and enablement remotely as much as possible and reserve in-person formats only for circumstances when we can't obtain the desired results via remote/virtual delivery). Iteration is expected to keep moving what we can to remote models once we can achieve the same or better results that way_.
   - We conduct a hands-on exercise to help new team members get more comfortable navigating issues and submitting merge requests on GitLab 
   - Participants benefit by establishing a community of similar-tenured peers to support each others' growth and development at GitLab
   - We take time to collect feedback on how we can continue to iterate and improve the overall sales onboarding experience
   - For an overview and feedback summary of Sales QuickStart 1, [click here](https://docs.google.com/presentation/d/1SwbhVZTMgJ9w2jQXWM8go9RxvTrCIBkScCEyhRsaZLA/edit?usp=sharing) 
*  Future iteration of this process will also begin to define what sales team members need to **KNOW**, **DO**, and **BE ABLE TO ARTICULATE** (by sales role, customer segment, and geo as appropriate) after they complete the above and support those learning objectives as they continue their ramp to GitLab sales performance excellence!
