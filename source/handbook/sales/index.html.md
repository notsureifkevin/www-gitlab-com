---
layout: markdown_page
title: "Sales Handbook"
---

## Reaching the Sales Team (internally)

- [**Public Issue Tracker**](https://gitlab.com/gitlab-com/sales/issues/) please use confidential issues for topics that should only be visible to team members at GitLab.
- Please use the 'Email, Slack, and GitLab Groups and Aliases' document for the appropriate alias.
- [**Chat channel**](https://gitlab.slack.com/archives/sales) please use the `#sales` chat channel for questions that don't seem appropriate to use the issue tracker or the internal email address for.
- **Chatter @sales-ops** on the specific record in Salesforce you need support related to Incremental ACV calculations, technical issues, or other problems you are having.
- **Chatter @deal-desk** on the specific record in Salesforce where you might need quote assistance.
- **Chatter @contracts** on the specific record in Salesforce where you will need assistance from the legal team.
- Please avoid contacting individual directly so that the discussion can be available for everyone to review.

---
## On this page
{:.no_toc}

- TOC
{:toc}

---

## Quick Reference Guide


| **GETTING STARTED** | **SALES PROCESS** | **MARKETING** |
| :------: | :------: | :------: |
| **Getting Started**<br>* [Sales Onboarding](/handbook/sales/onboarding)<br> * [Sales Tools](/handbook/business-ops/tech-stack-applications/#tech-stack-applications)<br>* [Team Org Chart](/company/team/org-chart/)<br> * [Team Structure and Roles](#team-structure--roles)<br> * [Commercial](/handbook/sales/commercial)<br> * [Territories](/handbook/sales/territories)<br>* [Bi-Weekly WW Field Sales Call](#bi-weekly-monday-sales-call)<br><br>**Sales Enablement Resources**<br>* [Sales Onboarding](/handbook/sales/onboarding)<br>* [Sales Training](/handbook/sales-training/)<br>* [Force Management](/handbook/marketing/product-marketing/sales-resources/#force-management-command-of-the-message)<br>* [Most Commonly Used Sales Resources](/handbook/marketing/product-marketing/sales-resources/)<br>* [GitLab University](https://docs.gitlab.com/ee/university/) (note: some of the content in GitLab University may be out of date)<br> | **Fundamentals**<br>* [Account Ownership Rules of Engagement](/handbook/business-ops/resources/#account-ownership-rules-of-engagement)<br> * [Parent-Child Accounts](#parent-and-child-accounts)<br> * [Opportunity Stages](/handbook/business-ops/resources/#opportunity-stages)<br>* [Record Ownership](/handbook/business-ops/resources/#record-ownership)<br>* [Forecasting Definitions and Processes](#weekly-forecasting)<br>* [Associating Emails to Salesforce](#associating-emails-to-opportunities)<br>* [Additional Metrics and Definitions](#definitions)<br>[Commissions](/handbook/sales/commissions/)<br><br>**Top of the Funnel**<br>* See the [Marketing section](/handbook/marketing)<br><br> **Early-to Mid-Stage** <br> * [Opportunity Types](/handbook/business-ops/resources/#opportunity-types)<br> * [Opportunity Naming Conventions](/handbook/business-ops/resources/#opportunity-naming-convention)<br>* [How to Create an Opportunity](/handbook/business-ops/resources/#how-to-create-an-opportunity)<br>* [MEDDPICC qualification](#capturing-meddpic-questions-for-deeper-qualification)<br>* [Additional Discovery Questions](/handbook/sales-qualification-questions/)<br>* [GitLab ROI Calculator](/roi/?team_size=100&developer_cost=75)<br>* [Technical Evaluation/Proof of Concept Guidelines](/handbook/sales/POC/)<br>* [Creating a Quote in Salesforce](/handbook/business-ops/order-processing/#step-4---zquotes--new-quote)<br>* [Quote Approval Matrix](https://docs.google.com/document/d/1-CH-uH_zr0qaVaV1QbmVZ1rF669DsaUeq9w-q1QiKPE/edit?usp=sharing)<br>* [Delivering a Quote via Sertifi](/handbook/business-ops/order-processing/#step-6--send-for-signature-via-sertifi)<br><br>**Late Stage**<br>* [Submitting an Opportunity for Approval](/handbook/business-ops/order-processing/#step-7--submitting-an-opportunity-for-approval)<br>* EULA Generation (Coming Soon)<br>* Licensing and Provisioning (Coming Soon)<br><br>**Post-Sales Processes**<br>* [Renewing/Amending a Subscription](/handbook/business-ops/order-processing/#returning-customer-creation-processupgraderenewalscancellations)<br>* [True Ups](#true-up)<br>* Contract Resets (Coming Soon)<br>* Consolidation of Subscriptions (Coming Soon)<br> | **Marketing Resources**<br>* [GitLab Messaging](/handbook/marketing/revenue-marketing/xdr/#gitlab-messaging)<br>* [Lead Gen Content Resources](/resources/)<br>* [Routing Rules](/handbook/business-ops/resources/#routing-1)<br>* [Lead and Contact Statuses](/handbook/business-ops/resources/#lead--contact-statuses)<br><br>**Product Marketing**<br>* [Product Marketing Handbook](/handbook/marketing/product-marketing/)<br>* [Product Marketing Manager Assignments](/handbook/marketing/product-marketing/#which-product-marketing-manager-should-i-contact)<br>* [Customer-Facing Presentations](/handbook/marketing/product-marketing/#customer-facing-presentations)<br>* [Print Collateral](/handbook/marketing/product-marketing/#print-collateral)<br>* [Sales Collateral Google Drive](https://drive.google.com/open?id=0B-ytP5bMib9TaUZQeDRzcE9idVk)<br><br>**Sales Development**<br>* [Sales Development Handbook](/handbook/marketing/revenue-marketing/xdr/)<br><br>**Business Development**<br>* [How to BDR](/handbook/marketing/revenue-marketing/xdr/#how-to-bdr)<br><br>  |
| **FIELD OPERATIONS** | **PRODUCT** |  **FINANCE** |
| **Field Operations Homepage** (Coming Soon)<br><br>**Sales Operations** (Coming Soon)<br><br>**Sales Systems** (Coming Soon)<br><br>**Sales Strategy and Analytics** (Coming Soon)<br><br>**Sales and Customer Enablement** (Coming Soon)<br><br> | **Product Resouces**<br>* [Product Team Handbook](/handbook/product/)<br>* [Engaging Product Management](/handbook/product/how-to-engage/)<br>* [Asking Questions/Giving Feedback on a Feature](/handbook/product/categories/#devops-stages)<br>* [Product Categories](/handbook/product/categories/)<br>* [Other Product Handbook Pages](/handbook/product/#other-product-handbook-pages)<br> |  **Finance Processes**<br>* [Deal Desk: Assistance with Quotes](/handbook/business-ops/order-processing/#assistance-with-quotes)<br>* [Deal Desk: Approval Process](/handbook/business-ops/order-processing/#deal-desk-approval-process)<br>* [Sending a Quote to Zuora](/handbook/business-ops/order-processing/#step-8--sending-the-quote-to-zuora)<br>* [How to Initiate a Refund](/handbook/finance/accounting/#7-invoice-cancellations-and-refunds)<br><br>**Legal Processes**<br>* [Legal Page](/handbook/legal/)<br>* [Engaging Legal in Deals](/handbook/business-ops/order-processing/#process-for-agreement-terms-negotiations-when-applicable)<br><br>**Business Operations Resources**<br>* [Business Operations Handbook](/handbook/business-ops)<br>* [Data Team Handbook](/handbook/business-ops/data-team/)<br>* [Data Quality Process](/handbook/business-ops/data-quality-process/)<br>* [IT Ops Handbook](/handbook/business-ops/it-ops-team/)<br>* [Access Requests](https://gitlab.com/gitlab-com/access-requests)<br> |
| **CUSTOMER SUCCESS** | **OTHER RESOURCES** |   |
| <br>**Customer Success Resources**<br>* [Customer Success Groups](/handbook/customer-success/#customer-success-groups)<br>* [How to Engage a Solutions Architect](/handbook/customer-success/solutions-architects#when-and-how-to-engage-a-solutions-architect)<br>* [Account Planning Template for Large Accounts](https://docs.google.com/presentation/d/1yQ6W7I30I4gW5Vi-TURIz8ZxnmL88uksCl0u9oyRrew/edit?ts=58b89146#slide=id.g1c9fcf1d5b_0_24)<br> | **Other Sales Topics in Handbook**<br>* [FAQ From Prospects](/handbook/sales-faq-from-prospects/)<br>* [Client Use Cases](/handbook/use-cases/)<br>* [Dealing with Security Questions From Prospects](/handbook/engineering/security/#security-questionnaires-for-customers)<br>* [How to conduct an executive meeting](https://www.youtube.com/watch?v=PSGMyoEFuMY&feature=youtu.be)<br>* [CEO Preferences when speaking w/ prospects/customers](/handbook/people-operations/ceo-preferences/#sales-meetings)<br><br>**Sales Resources Outside of the Sales Handbook**<br>* [Resellers Handbook](/handbook/resellers/)<br>* [Customer Reference Sheet](https://docs.google.com/a/gitlab.com/spreadsheets/d/1Off9pVkc2krT90TyOEevmr4ZtTEmutMj8dLgCnIbhRs/edit?usp=sharing)<br>* [GitLab Support Handbook](/handbook/support/)<br>* [GitLab Hosted](/gitlab-hosted/)<br> |


## Sales Tools

Tools that you will use in your day to day workflow, to see a complete list &/or for Contact/Admin details please see [full tech stack table on the Business OPS page](/handbook/business-ops/tech-stack-applications/#tech-stack-applications).  

- Chorus (Recorder in NORAM; Listener for ROW)
- Clari
- Conga Contracts
- DataFox
- DiscoverOrg (Commerical, provided to XDRs paired to SALs)
- GovWinIQ
- LeanData (not directly used by reps but handles routing)
- LinkedIn Sales Navigator
- Outreach.io
- Rollup Helper
- Salesforce
- Sertifi
- Slack
- Visual Compliance
- Xactly
- Webex (limited use case tt OPS if access needed)
- Zendesk
- Zoom


## Team Structure & Roles

See the GitLab [org chart](/company/team/org-chart).

Reporting into the Chief Revenue Officer (CRO) are the:

* VP Enterprise Sales
* VP [Commercial Sales](/handbook/sales/commercial/)
* VP WW Field Operations
* VP Customer Success
* VP WW Channels (TBH)

#### Initial Account owner - based on [segment](/handbook/business-ops/resources/#segmentation)

The people working with each segment and their quota are:

1. Large: Strategic Account Leader (SAL)
2. Mid-Market: Mid-Market Account Executive (MM AE)
3. SMB: [SMB Customer Advocate](/handbook/sales/commercial/#smb-customer-advocates) can close SMB deals directly by referring them to our self-serve web store or assisting them with a direct purchase.


#### Holdover Accounts when a SAL is promoted to Area Sales Manager
In advance of a SAL being promoted to an ASM, the SAL may choose as holdovers for 3 months from their promotion effective date opportunities which are greater than or equal to all of the following Salesforce opportunity characteristics: $100k IACV, Stage 3, 50% Probability, provided that the opportunity attributes are established in good faith. The holdover accounts must first be approved by the VP, Enterprise Sales and then documented with Sales Operations no later than the promotion effective date. The new ASM is expected to spend less than 25% of their time as an ASM managing the holdover accounts and is required to proactively incorporate the receiving SAL into the account so the transition is minimally disruptive to the account and maximizes GitLab’s opportunity to best serve the customer. Each individual holdover account will be fully transitioned to the receiving SAL on the opportunity close date (won or lost), 3 months following the promotion effective date, or upon the new ASM’s discretion, whichever comes first. Any opportunity which meets the listed qualifications and is closed won during the holdover period will result in commission credit to the ASM equal to the commission they would have received under their most recent SAL compensation plan. There will be no additional stacked ASM payout, although it will retire ASM quota. The receiving SAL will receive no quota or commission credit.

## Operating Rhythm - Sales Meetings, Presentations, Forecasting and Definitions

### Bi-Weekly Monday Sales Call

* When: Every other Monday 9:00am to 9:50am Pacific time. The call is cancelled on observed US holidays that fall on Mondays.
* What: The agenda can be found by searching in Google Drive for Sales Agenda". Remember that everyone is free to add topics to the agenda. Please start with your name and be sure to link to any relevant issue, merge request, or commit.

* General Guidelines

1. We start on time and do not wait for people.
1. If you are unable to attend, just add your name to the list of "Not Attending".
1. When someone passes the call to you, no need to ask "Can you hear me?" Just begin talking and if you can't be heard, someone will let you know.

* The call is recorded automatically, and all calls are transferred every hour to a Google Drive folder called "GitLab Videos". There is a subfolder called "Sales Team Meeting", which is accessible to all users with a GitLab.com e-mail account.
* The general order of the call is outlined below. Once the presenter has shared all of his/her items, he/she should then ask for questions. If there are no questions or all the questions have been answered, the presenter is to handoff to the next presenter on the agenda.
    1. The CRO or VP Sales will start the call with performance to-date for the month/quarter, then share the remaining forecast for the month/quarter. The CRO will then discuss any number of  topics ranging from pipeline cleanup, process reminders, and other  messages to the group. If the CRO is not attending, the Director of Sales Operations will share this update.
    2. The VP of Go To Market will provide a team update.
    3. Account Executives will share details of key deals (won, lost, pushed).
    1. The Director of Sales Operations will provide an update to process changes, new fields, and other updates related to sales process and group productivity.
    1. Customer Success is next. The Director of Customer Success may provide an update to an existing process or an introduction to a new process. In some cases, Solutions Architects may provide an update as well.
    1. Marketing will provide an update to Sales. The Sales and Business Development Manager will provide an update on SDR and BDR performance to plan: pipeline created, meetings scheduled, and other SDR/BDR metrics. Other members of marketing may provide updates - new collateral, events, and other marketing-related activities.
    1. Next, any topics from the group for discussion are encouraged. This can include questions on products, processes, or reminders to the rest of the group. In some cases, a summary of an event (client meetings, meetups, trade shows) will be shared.
    1. The call will conclude with a general 'scrum', which is designed for Account Executives to share anything new they have learned over the last week. This may include some competitive knowledge, new-found strategies or tactics that they have found helpful.
* If you cannot join the call, consider reviewing the recorded call or at minimum read through the sales team agenda and the links from there.

### Weekly Thursday Sales Enablement Call led by Product Marketing

* When: Happens every Thursday at 9am Pacific time

### Sales Group Conversation

* When: Happens once every 4 to 5 weeks at 8am Pacific time
* Where: Zoom link varies - please be sure to check the calendar invitation
* What: The CRO or VP Sales and Director of Sales Operations will provide the company with an update on sales team performance, notable wins, accomplishments, and strategies planned. You can view previous updates in the "Group Conversation Sales" (previously "Functional Group Update Sales") folder on the Google Drive.

### Weekly Forecasting

The Regional Director/Vice President of each team will establish weekly due date/time for your forecasts submissions. The RD/VP will be responsible for using your data for forecast the following:

* Net IACV Commit: this is your Gross IACV Commit minus your Renewal Loss Commit.
* Net IACV Best Case: this is your Gross IACV Best Case minus your Renewal Loss Best Case.
* Net IACV 50/50: this value serves as what you believe will be the actual forecast for your team. If you consider the Commit as the floor and Best Case as the ceiling, then the 50-50 is what is the most likely outcome.

Please watch the following videos on how to submit your [IACV forecast](https://drive.google.com/open?id=1b42doQGZ1H-3Yi_8boGF5wmk-7Fb7SL-) and your [Renewal ACV forecast](https://drive.google.com/open?id=1X8LRZVmC4Q4FQ__GiCGsG1Xq6SwZagqc). The following sections provide you with additional information on our forecasting methodologies and practices.

#### Opportunity Management Guidelines

* **All opportunities in stage 2-Scoping or later** must complete **ALL 5 Opportunity Overview** fields in Salesforce (or Clari):
   1. **Why Now?** - *What is the compelling event? Why does the customer need to make the purchase now including both qualitative and quantitative implications?*
   1. **Primary Value Driver** - *What is the primary business problem that the customer is trying to solve (even if GitLab didn't exist) that is causing the economic buyer to allocate discretionary funds to solve?*
   1. **Use Case(s)** - *What is the use case(s) that the customer is pursuing to achieve the above value driver(s)?*
   1. **Why GitLab?** - *Why is the customer choosing GitLab over the competition? What are GitLab's key differentiators from the customer's perspective?*
   1. **Why Do Anything At All?** - *What are the negative consequences if the customer does nothing? What happens if the customer doesn’t do this deal on the forecasted timeline?*
* In addition, all such opportunities that are at or above defined thresholds (**$50K+ for Enterprise**, **$20K+ for Mid-Market**, and **$10K+ for SMB**) must also complete the following fields:
   - **8 MEDDPICC** fields - see details [here](https://about.gitlab.com/handbook/sales/#capturing-meddpicc-questions-for-deeper-qualification)
   - **3 Close Plan Details** fields
      - **Close Plan** - *What is the full beginning to end close plan with dates (day by day or week by week)?*
      - **Risks** - *What is the biggest risk with this opportunity? What would be the possible reasons for why the deal doesn't come in on time? What are we doing to proactively address key risks?*
      - **Help** - *How can the extended GitLab team help (i.e. Product Management/Engineering needs, Legal requirements, Deal Desk, Finance, Execs)?*

#### Forecast Category and Renewal Forecast Category Fields

* **Forecast Category** will be used when forecasting any opportunity with Incremental IACV. For example, if you are submitting a New Business, Add On Business, or Renewal opportunity to your forecast, the IACV portion of the opportunity will be included in your number.

* **Renewal Forecast Category** will be used when forecasting any renewal opportunities. In these cases, the Renewal ACV portion of the deal will be included into your forecast number.

#### Default Salesforce Stage to Forecast Category and Renewal Forecast Category Mapping

| **Opportunity Stage** | **Default Forecast Category** | **Renewal Forecast Category**
| -------- | -------- | -------- |
| 00-Pre Opportunity   | Omitted   |  NULL   |
| 0-Pending Acceptance   | Omitted   |  Commit   |
| 1-Discovery   | Pipeline   |  Commit   |
| 2-Scoping   | Pipeline   |  Commit   |
| 3-Technical Evaluation   | Pipeline   |  Commit   |
| 4-Proposal   | Best Case   |  Commit   |
| 5-Negotiating   | Commit   |  Commit   |
| 6-Awaiting Signature   | Commit   |  Commit   |
| Closed Won   | Closed   |  Closed   |
| 8-Closed Lost   | Omitted   | Omitted   |
| 9-Unqualified  | Omitted   | NULL   |
| 10-Duplicate   | Omitted   | NULL   |

#### Forecast Categories Definitions
There are two types of categories we review when your forecasts are pushed to Clari:

* **Closed**: Closed includes only those opportunities that are "Closed Won" only. In forecasting, Closed includes Closed Won.
* **Commit**: Commit includes opportunities that you feel have a 95-100% probability to close in the given period as represented by the Close Date. If you do not believe it will close in that period, it should not be a Commit.  For forecasting purposes, Commit will include "Closed + Commit Deals".
* **Best Case**: Best Case includes opportunities that have a 50% and above probability to close in a period. These deals have a realistic possibility to close; however, only if certain events occur. The path to close should not be based on some miraculous event, as there should a credible plan for this deal to close this period. For forecasting purposes, Best Case includes Closed + Commit + Best Case.
* **Pipeline**: Pipeline consists of deals that are not going to close in the quarter, but should be expected to close in the next few quarters. You should not have deals that are in the Pipeline Forecast Stage with a close date in the current period.  For forecasting, Pipeline includes Closed + Commit + Best Case + Pipeline.
* **Omitted**: These are not included in the forecast and should reflect very early stage deals in 0-Pending Acceptance stage or those in a Lost, Unqualified, or Duplicate stage.

#### Overriding Forecast Categories
Early in a quarter, you may not have many opportunities in Stage 4 and beyond (Best Case and up). With that in mind, you will have the ability to override the Forecast Category and Renewal Forecast Category for your opportunities in both Salesforce and Clari.

In Salesforce, go to the Opportunities Home tab:
  1. Select either the *My Forecast* opportunity list view OR the *My Forecast* report.
  2. Click on the Opportunity Name of the record you want to update.
  3. Change the **Forecast Category** and/or **Renewal Forecast Category** fields.
  4. Click Save.

In Clari, you will update records individually via Opportunity tab:
  1. Select the *CQ: Open Deals*, *CQ: Commit Deals*, *CQ: Best Case*  or *CQ: Open Renewals* view
  2. You can update the opportunities directly within the opportunity grid. Just to to the field you want to update and double click.
  2. Click on the row of the opportunity you want to edit. On the right, you should see additional details (if you don’t, click on the *Toggle Details* button).
  2. In the Details tab, scroll to Forecast Category and double-click the field to edit. Select the desired category.
  3. Click Save.

##### Advantages to Updating Opps in Clari
In Clari, we have provided a simplified layout meaning that we have designed the **Details** tab to include the most important fields that an RD/VP will review when determining whether they will include your opportunity in their regional forecast. **Purchasing Plan, MEDDPICC, Next Steps** are atop this simplified layout. You won’t have to navigate through various sections of Salesforce to enter the most important details. What's more is that sales leadership will use this exact same view when reviewing your opportunities. So while you might be more familiar with updating opportunity records in Salesforce, over time, you should find updating in Clari will prove much more convenient.

#### Forecasting and Sales Leaders
If you are a Sales Leader, will have additional access to the **Forecasting Tab** in Clari. You will see four tabs:

* **Net IACV**
  * You will enter your Gross IACV Commit/Best Case and Renewal Loss Commit/Best Case in this tab.
  * Gross IACV will be your bookings, a positive value; Renewal Loss will be for lost renewal ACV, represented by a negative value.
  * The result will be your Net IACV, which will be your official value submitted.

* **Gross IACV**
  * This will be a read only tab. You will see your rep’s quota, Won IACV, and pipeline coverage.

* **Renewal ACV**
  * This will be a read only tab. You will see your rep’s Closed Won Renewal ACV, Commit, Best Case, Renewal Loss Commit, and Renewal Loss Best Case.

* **TCV**
  * This will be a read only tab. You will see your rep’s Closed Won TCV, Commit, Best Case, and Pipeline.

##### Reviewing Forecasts

On the right of each tab, you will see a Toggle Details button. You can click on this to expand the list of opportunities in the selected forecast category.You will have three options:
* Total:  shows the combined deals for Closed + Open in that category (Closed, Commit, Best Case, or Pipeline)
* Closed: shows deals Closed Deals
* Open: shows open deals in that category

Click on the radio button for the type of opportunities you want to see (normally for forecasting calls with AEs, you will select “Open”). Then click “View Deals” to go to a full list of opportunities.

To overwrite your regional forecast:
1. In Clari, go to the Forecasting tab.
1. Go the tab of the metric you want to forecast (Gross IACV or Renewal ACV).
1. Go to Commit and override the value.
1. Add a note on why the adjustment made.
2. Repeat this process for Best Case.
1. Click Save.

#### Forecast Terminology
Please use these terms correctly and don't introduce other words. Apart from the above the company uses two other terms:

* **Plan**: Our yearly operating plan that is our promise to the board. The IACV number has a 50% of being too low and 50% chance of being too high but we should always hit the TCV - Opex number.
* **Forecast**: Our latest estimate that has a 50% of being too low and 50% chance of being too high.

## Definitions
### Customers
We define customers in the following categorical level of detail:
1. Subscription: A unique subscription contract with GitLab for which the term has not ended. As customers become more sophisticated users of GitLab the number of subscriptions may decline over time as Accounts and Parents consolidate subscriptions to gain more productivity.
1. Account: An organization that controls multiple subscriptions that have been purchased under a group with  common leadership. In the case of the U.S. government, we count U.S. government departments and major agencies as a unique account.
1. Parent: An accumulation of Accounts under an organization with common ownership. In the case of the U.S. government, we count U.S. government major agencies as a unique parent account. (In Salesforce this is the `Ultimate Parent Account` field)

Because "customer" can have three different meanings whenever customer is used in presenting data it must be qualified by the type of customer. The default description is parent. When the default is used no further description is required. When account or subscription is being reported then the title or field description on the chart must be added to call out the basis for reporting. Metrics that are based on customer data should also carry a clarifying description.

#### Customer Segmentation
Customer segmentation follows the segmentation as laid out in the [Business Operations Handbook](/handbook/business-ops/resources/#segmentation) at the [Parent Account level](#customers).

#### Customer Counts
1. Subscriptions: Given that subscriptions can consolidate, fan out, be renewed, and experience other kinds of transformations over time, counting subscriptions are less straightforward than counting accounts. The core principle is: if a subscription was active at any point in time in the proposed timeframe, it is counted as active.

1. Accounts and Parents: If an account was active at any point in time during the proposed timeframe it is counted as active. For example, an account that is active from March 2019 to May 2019 but is inactive from June 2019-on is counted for CY2019, FY2020 (which runs from February 2019-January 2020), FY20-Q1, and FY20-Q2; it is not counted in FY20-Q3 or FY20-Q4.

<details>
<summary>Specific Examples of Subscription Counts (Click to expand)</summary>

<ul>

<li> Non-renewal: A subscription that is active from March 2019 to May 2019 but is inactive from June 2019-on is counted for CY19, FY20 (which runs from February 2019-January 2020), FY20-Q1 (Feb-April 2019), and FY20-Q3 (May-July 2019); it is not counted in FY20-Q3 or FY20-Q4. </li>
<li> Standard renewal:  A subscription that is active from March 2019 to May 2019 and is renewed in June 2019 with a single subscription will have a total number of 1 subscriptions at all points in which it is counted. </li>
<li> Consolidation: Two subscriptions are active under one account from March 2019 to May 2019. In June 2019, they are consolidated into one subscription. (The use of "consolidation" does not imply a smaller subscription, just that there are now fewer subscriptions.) In April 2019, the count of active subscriptions for that month will be 2 subscriptions. In July 2019, the count of active subscriptions for that month will be 1 subscription; at the same time, in July 2019, the count of active subscriptions for the month of April 2019 will be updated to reflect 1 given the consolidation. Once subscriptions are consolidated, they will count as 1. The historical count of subscriptions will go down as subscriptions are consolidated. </li>
<li> Fan out: One subscription is active under one account from March 2019 to May 2019. In June 2019, these are cancelled and renewed to two new subscriptions. In April 2019, the count of active subscriptions for that month will be 1 subscription. In July 2019, the count of active subscriptions for that month will also be 1 subscription. For all periods of time, these subscriptions will count as one. </li>
</ul>

This method of counting subscriptions may understate the number of active subscriptions active at any given point in time. This approach to counting reduces complexity and scale, makes clear we are never overstating subscriptions, and makes the counting process straightforward.

</details>

#### Annual Recurring Revenue (ARR)
MRR times 12

#### ARR by Annual Cohort
{: #arr-cohort}
ARR can be sliced many different ways for analysis. In the ARR by Cohort analyses, we look at ARR (as defined above) by the Fiscal Year Cohort. That analysis can be found on the [Finance Dashboard](https://app.periscopedata.com/app/gitlab/483606/Finance-KPIs).

#### Monthly Recurring Revenue (MRR)
Monthly recurring revenue from subscriptions that are active on the last day of the month plus (true-ups/12).

Subscription data from Zuora is the sole source of tracked MRR. The MRR value for a given month is based on the rate plan charge that is active on the last day of the month. True-up revenue is divided by twelve and added to the subscription MRR for the month it was charged.

Note that MRR values can change on a regular basis. The primary causes are customers updating, renewing, or canceling their subscriptions in a month different from when the original subscription ended. Updates increase and decrease the MRR values for all previous months of a subscription. Renewals increase MRR for all months since the start of the subscription. Cancellations decrease MRR for all months the subscription was active.

#### Rep Productivity
The average amount of annualized IACV a native quota carrying sales rep produces in a given month
formula: (IACV / # of native quota carrying reps adjusted for ramp time) * 12 months. See [Quota Ramp](/#quota-ramp)

##### Measuring Sales Rep Productivity

The primary metric when measuring rep productivity for only for quota attainment but also for compensation is the Gross Incremental Annual Contract Value (Gross IACV). Is is important to remember that while renewals are not a part of comp or quota attainment, renewing customers is still very important aspect of our business.

Rep Productivity is defined as IACV divided by type of Rep (i.e. SAL, MM AEs, SMB Customer Advocates).

Another measured KPI is Rep Productivity (as defined above) divided by On Target Income.

### Average Sales Price (ASP)
IACV per won deal. This metric can be reported against various dimensions (e.g. ASP by customer segment, cohort, sales channel, territory, etc.)


#### Annual Contract Value (ACV)
Value of **all** bookings from new and existing customers that will result in recurring revenue over the next 12 months less any credits, lost renewals, downgrades or any other decrease to annual recurring revenue. Excluded from ACV are bookings that are non-recurring such as professional services, training and non-recurring engineering fees (PCV).

#### Incremental Annual Contract Value (IACV)
Value of new bookings from new and existing customers that will result in recurring revenue over the next 12 months less any credits, lost renewals, downgrades or any other decrease to annual recurring revenue. Excluded from IACV are bookings that are non-recurring such as professional services, training and non-recurring engineering fees (PCV). Also equals ACV less renewals. However, bookings related to true-up licenses, although non-recurring, are included in IACV because the source of the true-ups are additional users which will result in recurring revenue.
IACV may relate to future periods (within twelve months).

Beg ARR + IACV may not equal ending ARR due to the following reasons:
1. Timing difference due to IACV that will not start until a later period.
1. ARR will be reduced by subscriptions that have expired but which may be recorded as a reduction to IACV in a different period (either earlier or later).

##### How Incremental ACV is Calculated

Incremental IACV can be calculated for **new and add on business** in the following ways:

| **Type** | **Opportunity Term**  | **Amount/TCV** | **Incremental ACV** | **Explanation** |
| ------ | ------ | ------ | ------ | ------ |
| New Business | 12 months | $120,000 | $120,000 | New customer signs for less than or equal to 12 months is IACV. |
| New Business | 24 months |  $240,000 | $120,000 | New customer signs up for 24 months, so while Amount/TCV is $240,000, only the first 12 months counts towards IACV.  |
| Add On Business | 5 months | $40,000 | $40,000 | Existing customer signs for less than or equal to 12 months is IACV. |
| Add On Business | 16 months | $16,000 | $12,000 | Existing customer signs for more than 12 months, so while Amount/TCV is for the entire 16 months at $16,000, only the first 12 months counts towards IACV.   |


When calculating Incremental ACV against **renewal** opportunities, we consider the Renewal ACV (please see "[How Renewal ACV is Calculated](/handbook/sales/#renewals-acv)" for more details). In the following scenario, we will look at how IACV is calculated:

| **Type** | **Opportunity Term** | **ACV** | **Renewal ACV** | **Incremental ACV** | **Explanation** |
| ------ | ------ | ------ | ------ | ------ | ------ |
| New Business | 12 months | $120,000 | $0 | $120,000  | New customer signs for less than or equal to 12 months is IACV. |
| Add On Business | 5 months |  $50,000 | $0 | $50,000   | Customer add on at less than or equal to 12 months is IACV.  |
| Renewal | 12 months | $240,000 | $170,000 | $70,000  | The IACV from the new and add on business from the previous year total $170,000, while the ACV of the renewal opp is $240,000. The delta is IACV of $70,000.  |

Note that in the above scenario, while you are only paid for the first 5 months for the upsell, you are paid for the remaining 7 months on the renewal, meaning you are paid the entire 12 month upsell ($120,000 IACV) over two separate opportunities.


**Contractual Ramps:** this happens when a customer wants to increase the number of users over time on a **single** contract.
* A customer purchased 1,000 Starter seats at $48,000 for year 1 of a **multi-year** subscription.
* Year 2, they have agreed to purchase 2,000 seats at $96,000.
* Previously, we would take the average of the two years as the IACV, which would have been $72,000.
* Due to stricter requirements regarding revenue recognition, effective **July 1, 2019**, the calculation is no longer the average, but the actual year first year IACV. This ensures alignment between our bookings and revenue recognition.
* In this case, the IACV now would be $48,000.



**Contract Resets/Restarts:** this happens when a customer wants to restart their subscription in the middle of term.
* A customer purchased a subscription for $12,000 in January. In April, they decide to upgrade to a $24,000 annual package, but also restart their term to end 12 months from April, meaning the end date would now be March 31 and not December 31.
* We would cancel the remaining subscription as of April 1 through December 31. This will result in a credit of $9,000, which will be applied to the new subscription.
* By definition, the reset should be tagged as a Renewal, since we are extending the customer's subscription, albeit early. Any increase in subscription fees would result in IACV.

| **Type** | **Opportunity Term** | **Dates** | **ACV** | **Renewal ACV** | **Incremental ACV** | **Explanation** |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| New Business | 12 months | Jan 1 - Dec 31 | $12,000 | $0 | $12,000 | New customer signs for 12 month plan at $12,000 IACV |
| New Business | 9 months |  Apr 1 - Dec 31 | ($9,000) | $0 | ($9,000) | Credit opp: Customer wants to upgrade to $24,000, but also wants to reset their term starting Apr 1  |
| Renewal | 12 months | Apr 1 - Mar 30 | $24,000 |  $3,000 |  $21,000 | Contract reset is closed. Renewal ACV is only $3,000 ($12,000 - $9,000). |

**Consolidation of Subscriptions:** this happens when a customer wants to combine multiple subscriptions into one. In this case, we would total all current Renewal ACV into a single renewal opportunity.
* A customer purchased a subscription for $12,000 in January for 12 months.
* In July, they purchase a second subscription, this time for $24,000 for 12 months.
* They want to co-term both subscriptions to end in December.


| **Type** | **Opportunity Term** | **Dates** | **ACV** | **Renewal ACV** | **Incremental ACV** | **Explanation** |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| New Business (Sub 1) | 12 months | Jan 1 - Dec 31 | $12,000 | $0 | $12,000 | New customer signs for 12 month plan at $12,000 IACV  |
| New Business (Sub 2) | 12 months | Jul 1 - Jun 30 | $12,000 | $0 | $12,000 | New customer signs for 12 month plan at $12,000 IACV  |
| New Business (Sub 2) | 6 months |  Jan 1 - Jun 30 | ($6,000) | $0 | ($6,000) | Credit opp: Customer will cancel this subscription for the remainder of term to coterminate their subscriptions  |
| Renewal (Sub 1 + Sub 2) | 12 months | Jan 1 - Dec 31 | $24,000 |  $18,000 |  $6,000 | Renewal consoildation will take the full 12 months for Sub 1 and the remaining credit for Sub 2 ($12,000 - $6,000). |

**Splitting Subscriptions:** this happens when a customer wants to split a single subscription into multiple subscriptions. In this case, we would split the renewal ACV from the single renewal opp into multiple opportunities:
* A customer purchased a subscription for $12,000 in January for 12 months.
* In July, they purchase a second subscription, this time for $24,000 for 12 months.
* They want to co-term both subscriptions to end in December.


| **Type** | **Opportunity Term** | **Dates** | **ACV** | **Renewal ACV** | **Incremental ACV** | **Explanation** |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| New Business         | 12 months | Jan 1 - Dec 31 | $12,000 | $0 | $12,000 | New customer signs for 12 month plan at $12,000 IACV  |
| Renewal Business (Sub 1) | 12 months | Jan 1 - Dec 31 | $3,000 | $3,000 | $0 | Customer is taking 1/4 of users and creating new subscription. $3,000 is the renewal ACV from the original $12,000.  |
| Renewal Business (Sub 2) | 6 months |  Jan 1 - Jun Dec 31 | $9,000 | $9,000 | $0 | Customer is taking 3/4 of users and creating new subscription. $9,000 is the renewal ACV from the original $12,000.  |

In the above scenario, the total Renewal ACV of two new renewal opps should total the IACV of the original opportunity.

The final IACV value comes from the Net_IACV field in the Salesforce opportunities table.

#### Gross Incremental Annual Contract Value (Gross IACV)
Value of new bookings from new and existing customers that will result in recurring revenue over the next 12 months. Gross IACV includes true-ups and refunds.

#### Growth Incremental Annual Contract Value (Growth IACV)
Contract value that increases at the time of subscription renewal

#### New Incremental Annual Contract Value (New IACV)
Contract value from a new subscription customer

#### ProServe Contract Value (PCV)
{: #pcv}
Contract value that is not considered a subscription and the work is performed by the Professional Services team

#### Total Contract Value (TCV)
Value of **all** bookings from new and existing customers that will result in revenue less any credits, lost renewals, downgrades or any other decrease
 (i.e. within 90 days from close of the deal).
### Credit
Lost or lowered contract value that occurs before a subscription renewal or subscription cancellation.

### Customer Acquisition Cost (CAC)
Total Sales & Marketing Expense/Number of New Customers Acquired

### Customer Acquisition Cost (CAC) Ratio
{: #cac-ratio}
Total Sales & Marketing Expense/ACV from new customers (excludes growth from existing).  [Industry guidance](http://www.forentrepreneurs.com/2017-saas-survey-part-1/) reports that median performance is 1.15 with anything less than 1.0 is considered very good.
All bookings in period (including multiyear); bookings are equal to billings with standard payment terms.

### Downgrade
Contract value that results in a lower value than the previous contract value. Downgrade examples include seat reductions, product downgrades, discounts, and customers switching to Reseller at time of renewal.

### Field Efficiency Ratio
Field efficiency for a given month is calculated as the ratio of [IACV](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) from closed won opportunities for the current month and the 2 preceding months vs the Sales Operating Expenses (IACV / Sales Operating Expenses) for the same time period. Sales Operating Expenses are the sum of debits minus the sum of credits within the "Sales" Parent Department within Netsuite from accounts between 6000 and 6999. GitLab's target is greater than 2.

### Licensed Users
{: #licensed-users}
The number of contracted users on active paid subscriptions. Excludes OSS, Education, Core and other non-paid users. The data source is Zuora.

### Life-Time Value (LTV)
{: #ltv}
Customer Life-Time Value = Average Revenue per Year x Gross Margin% x 1/(1-K) + GxK/(1-K)^2; K = (1-Net Churn) x (1-Discount Rate).  GitLab assumes a 10% cost of capital based on current cash usage and borrowing costs.

### Life-Time Value to Customer Acquisition Cost Ratio (LTV:CAC)
{: #ltv-to-cac-ratio}
The customer Life-Time Value to Customer Acquisition Cost ratio (LTV:CAC) measures the relationship between the lifetime value of a customer and the cost of acquiring that customer. [A good LTV to CAC ratio is considered to be > 3.0.](https://www.klipfolio.com/resources/kpi-examples/saas-metrics/customer-lifetime-value-to-customer-acquisition-ratio)

### Lost Renewal
Contract value that is lost at the time of subscription renewals. Lost Renewals examples include cancellations at or before the subscription renewal date. If you have a customer who is not renewing, you must mark the Stage as `8-Closed Lost`.

### Magic Number
IACV for trailing three months / Sales and marketing Spend over trailing months -6 to months -4 (one quarter lag) (see the details of this spend, as defined in the Sales Efficiency Ratio). [Industry guidance](http://www.thesaascfo.com/calculate-saas-magic-number/) suggests a good Magic Number is > 1.0. GitLab's target is to be at 1.1. This analysis can be found on the [Finance Dashboard](https://app.periscopedata.com/app/gitlab/483606/Finance-KPIs).

### Win Rate
Measurement of closed won, sales assisted opportunities vs the total number of closed, sales assisted opportunities (lost and won) in a given period.  GitLab's target is over 30%.

### New ACV / New Customers
Net IACV that come from New Customers divided by the number of net closed deals in the current month.

### New ACV / New Customers by Sales Assisted
Net IACV that come from New Customers and sold by the field sales team divided by the number of net closed deals in the current month.

### Revenue per Licensed User (also known as ARPU)
ARR divided by number of [Licensed Users](#licensed-users)

### Self-Serve Sales Ratio
Ratio of total IACV from closed won, Web Direct opportunities (i.e. customers who purchase via the self-service portal) divided by the total IACV of all closed won opportunities. GitLab's target is greater than 30%. The default measurement is IACV but this can also be calculated and reported for ACV.

### Sales Efficiency Ratio
Sales efficiency for a given month is calculated as the ratio of [IACV](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) from closed won opportunities for the current month and the 2 preceding months vs the Sales & Marketing Operating Expenses (IACV / Sales & Marketing Operating Expenses) for the same time period. Sales & Marketing Operating Expenses are the sum of debits minus the sum of credits within the "Sales" and "Marketing" Parent Departments within Netsuite from accounts between 6000 and 6999. [Industry guidance](http://tomtunguz.com/magic-numbers/) suggests that average performance is 0.8 with anything greater than 1.0 is considered very good. GitLab's target is greater than 1. This analysis can be found on the [Finance Dashboard](https://app.periscopedata.com/app/gitlab/483606/Finance-KPIs).

### Sales Qualified Lead (SQL)
[Sales Qualified Lead](/handbook/business-ops/resources/#customer-lifecycle)

### Late Stage Pipeline
The IACV of all open opportunities currently in the stages of 4-Proposal, 5-Negotiating, and 6-Awaiting Signature.

### Total Pipeline
The IACV of all open opportunities.

### Pipeline Coverage Ratio
Pipeline with close dates in a given period (quarter) divided by IACV target. Pipeline coverage should be 2X for current quarter, 1X for next quarter, and .5 for 2 QTRs out.

### Pipeline Generation
We measure pipeline generated on a monthly basis for net new customers and existing customers.  For KPI measurement pipeline creation vs plan should exceed 1.0.

### Renewals (ACV)
The value of previously closed Won ACV that is up for renewal. Renewal ACV should not include ACV from Professional Services or True-Ups.

##### How Renewal ACV is Calculated

Renewal IACV can be calculated in the following ways:

| **Type** | **Opportunity Term**  | **Amount/TCV** | **Incremental ACV** | **Explanation** |
| ------ | ------ | ------ | ------ | ------ |
| New Business | 12 months | $120,000 | $120,000 | New customer signs for less than or equal to 12 months is IACV. |
| Add On Business | 5 months | $40,000 | $40,000 | Existing customer signs for less than or equal to 12 months is IACV. |

In the above case, the renewal ACV will be $160,000. Any delta from this number will result in a positive or negative impact to IACV.

**True Ups:**  Remember that true-ups are NOT included in the Renewal ACV of a renewing customer, even though they are classified as IACV. Let's look at the scenario below:

| **Type** | **Opportunity Term**  | **Amount/TCV** | **Incremental ACV** | **Explanation** |
| ------ | ------ | ------ | ------ | ------ |
| New Business | 12 months | $48,000 | $48,000 | New customer purchases 1,000 Starter Seats |
| First Renewal | 12 months | $96,000 | $48,000 | Customer renews the 1,000 Seats, but also has a true up for 500 users. They also need to purchase 500 seats for the following term, so the IACV is $48,000  |
| Second Renewal | 12 months | $48,000 | $0 | On the second renewal, there is no growth from the 1,500 users, and since we are not including true ups in the Renewal ACV, the IACV is $0 (and not -$48,000 if we included true ups)  |

### Renewals + Existing Growth
Renewal ACV plus Growth IACV minus (Lost Renewals + Credits + Downgrades)

### Upsells/Cross sells and Extensions (IACV)
The value of the first twelve (12) months of any mid-term upgrade.

### Closed Deal - Won
A unique deal that is set to `Closed Won` in SalesForce.


## How Sales work with other GitLab teams

### Sales Development Representative (SDR)

#### 1:1 pairing with SDR

* Align on accounts and prioritize prospecting targets (both on the account and title levels).
* SDRs are to set 'at bat' meetings utilizing sales development best practices which are documented in the [SDR Handbook](/handbook/marketing/revenue-marketing/xdr/#sdr-expectations)
* Drive brand awareness within target accounts

### Customer Success

See our [customer success page in the handbook for more details](/handbook/customer-success/).

### Support - Escalation to Support

During the sales cycle potential customers that have questions that are not within the scope of sales can have their queries escalated in different ways depending on the account size:

1. For Strategic/Large accounts that will have a dedicated Solutions Architect, [engage the SA](/handbook/customer-success/solutions-architects#when-and-how-to-engage-a-solutions-architect) so that they can triage and develop the request.
1. For questions that you think technical staff can answer in less than 10 minutes, see the [internal support](/handbook/support/#internal-support-for-gitlab-team-members) section of the support handbook.
1. If a potential customer has already asked you a question, forward a customer question via email to the **support-trials** email address. - It's important the email is **forwarded** and not CC'd to avoid additional changes required once the support request is lodged.

#### How Support Level in Salesforce is Established

Once a prospect becomes a customer, depending on the product purchased, they will be assigned a Support Level (Account) in Salesforce:

##### GitLab Self-managed
1. Premium: Ultimate customers; Premium customers; any Starter customer who has purchased the Premium Support add on
1. Basic: any Starter customer without the Premium Support add on
1. Custom: any customer on Standard or Plus legacy package

##### GitLab.com
1. Gold: Gitlab.com Gold Plan
1. Silver: Gitlab.com Silver Plan
1. Bronze: Gitlab.com Bronze Plan

If a customer does not renew their plan, their account will be transitioned to an "Expired" status.

## Product

### Who to talk to for what

If a client has a question or suggestion about a particular part of the product, find out with whom on the Product team you need to speak by looking under [DevOps Stages](/handbook/product/categories/#devops-stages) on the Product stages, groups, and categories page.

### Contributing to Direction

Being in a customer facing role, salespeople are expected to contribute to [GitLab Direction](/direction/). Each day we talk to customers and prospects we have the opportunity to hear of a feature they find valuable or to give feedback (positive and constructive) to an feature that there is an issue for.
When you hear of feedback or you personally have feedback, we encourage you to comment within the issue, if one exists, or create your own issue on our [Issue Tracker](https://gitlab.com/gitlab-org/gitlab-ee/issues). Checking the [GitLab Direction](/direction/) page and the [Issue Tracker](https://gitlab.com/gitlab-org/gitlab-ee/issues) should happen throughout the week.

When you have an organization that is interested in a feature and you have commented in the issue and added a link to the account in salesforce, please follow the process outlined on the [product handbook](/handbook/product/#example-a-customer-has-a-feature-request) to arrange a call with the product manager and account to further discuss need of feature request.

## Requesting Assistance/Introductions Into Prospects from our Investors

We have great investors who can and want to help us penetrate accounts.  Each month we send out an investor update and within this update there will be a section called "Asks".  Within this section, we can ask investors for any introductions into strategic accounts. To make a request, within the account object in Salesforce, use the chatter function and type in "I'd like to ask our investors for help within this account".  Please cc the CRO and your manager within the chatter message.  All requests should be made before the 1st of the month so they are included in the upcoming investor update.

If an investor can help, they will contact the CRO, and the CRO will introduce the salesperson and the investor. The salesperson shall ask how the investor wants to be updated on progress and follow up accordingly.

### Email Intro

_This introduction email is progressive. You can choose to use just the first paragraph or the first two, three, or four depending on the detail you feel is appropriate for your audience._

GitLab is the end-to-end platform used by 10,000+ enterprises and millions of developers.  As the world is massively transforming the way software is developed and delivered, 10,000+ enterprises and millions of developers have done so using GitLab.

GitLab is the only single application built from the ground up for all stages of the DevOps lifecycle.  Companies including VMWare, Goldman Sachs, NVidia, Samsung, and Ticketmaster have all transformed and modernized their development process using GitLab.  In the process, they have made their software lifecycle 200% faster, reduced QA tasks from one hour to 30 seconds, all while also improving security and making developers massively more productive.

With GitLab, Product, Development, QA, Security, and Operations teams are finally able to work concurrently on the same project. GitLab enables teams to collaborate and work from a single conversation, instead of managing multiple threads across disparate tools. GitLab provides teams a single data store, one user interface, and one permission model across the DevOps lifecycle allowing teams to collaborate, significantly reducing cycle time and focus exclusively on building great software quickly.

From project planning and source code management to CI/CD and monitoring, GitLab is a [complete DevOps platform](/stages-devops-lifecycle/), delivered as a single application. Only GitLab enables [Concurrent DevOps](/concurrent-devops) to make the software lifecycle 200% faster.  Some additional detail is linked below:

* [Gitlab ranked number 4 software company (44th overall) on Inc. 5000 list of 2018's Fastest Growing Companies](/is-it-any-good/#gitlab-ranked-number-4-software-company-44th-overall-on-inc-5000-list-of-2018s-fastest-growing-companies)
* [GitLab has 2/3 market share in the self-managed Git market](/is-it-any-good/#gitlab-has-23-market-share-in-the-self-managed-git-market)
* [GitLab CI is the fastest growing CI/CD solution](/is-it-any-good/#gitlab-has-23-market-share-in-the-self-managed-git-market)
* [GitLab CI is a leader in the The Forrester Wave™](/is-it-any-good/#gitlab-ci-is-a-leader-in-the-the-forrester-wave)
* [GitLab is a top 3 innovator in IDC's list of Agile Code Development Technologies for 2018](/is-it-any-good/#gitlab-is-a-top-3-innovator-in-idcs-list-of-agile-code-development-technologies-for-2018)
* [GitLab is a strong performer in the new Forrester Value Stream Management Tools 2018 Wave Report](/is-it-any-good/#gitlab-is-a-strong-performer-in-the-new-forrester-value-stream-management-tools-2018-wave-report)
* [GitLab is one of the top 30 open source projects](/is-it-any-good/#gitlab-is-one-of-the-top-30-open-source-projects)
* [GitLab has more than 2,000 contributors](/is-it-any-good/#gitlab-has-more-than-2000-contributors)
* [GitLab has been voted as G2 Crowd Leader in 2018](/is-it-any-good/#gitlab-has-been-voted-as-g2-crowd-leader-in-2018)


## [GitLab Sales Process](/handbook/business-ops/resources/#opportunity-stages)

### Capturing "MEDDPICC" Questions for Deeper Qualification

MEDDPICC (view the [MEDDPICC training slides](https://docs.google.com/presentation/d/1A2zw8jUNBlgZhtcpzQk7wdKZskkGdlF6YHL-gyzRPrI/)) is a proven sales methodology used for strategic opportunity management and complex sales process orchestration for enterprise organizations. These questions should be answered in the "1-Discovery" and "2-Scoping" stages of the sales process. These questions will appear in the MEDDPICC section of the Command Plan in Salesforce (part of the opportunity layout) and will be required for all new business opportunities that are greater than the [Opportunity Management Guidelines](https://about.gitlab.com/handbook/sales/#opportunity-management-guidelines).

* **(M) Metrics**: *the economic impact of the solution*
  * What is the business case? 
  * Have you quantified the challenge(s) the customer is having and the impact to the organization? 
  * What quantifiable metrics/measurements have been identified? 
     - Top-line metrics include quicker time to market, higher quality (improvements towards sales and revenue)
     - Bottom line metrics include reduction in operating costs
  * How will you prove the business benefits of the solution?
* **(E) Economic Buyer**: *the individual within the customer’s organization who is required for the final “yes”*
  * Have you identified and met with the Economic Buyer (EB) (the individual who has the power to spend and/or has profit/loss responsibility for this solution)?
  * How do you know they’re actually the EB (versus someone higher in the organization)?
* **(D) Decision Process**: *the process and timeline by which the customer will evaluate, select, and purchase a solution*
  * What will the decision-making process look like? 
  * What key stakeholders are involved in the Decision Process and what do we know about them? 
  * How will the decision ultimately be made (e.g. one person's influence, decision by committee)? 
  * How have you influenced the Decision Process in favor of GitLab?
* **(D) Decision Criteria**: *the formal solution requirements against which each participant in the decision process will evaluate (includes the technical and financial lens with which the customer views GitLab)* 
  * What is the customer's criteria for selection?
     - What is the Business Decision Criteria (BDC)?
     - What is the Technical Decision Criteria (TDC)?
  * How have you influenced the Decision Criteria in favor of GitLab? 
  * How does our technology align with their technical wish list? 
  * How does our subscription model align with how they usually buy software? 
  * What other criteria have they told us about?
* **(P) Purchasing Process**: *the process by which the customer will document and execute the process of purchasing (including the MSA, order form, PO, etc.)*
  * What does the purchasing process entail?
  * What paperwork will be required to make the purchase happen? 
  * Do you know how each step gets approved? 
  * How has this process and timeline expectations for each step been documented and communicated? 
  * Is there a legal team involved in contract negotiations?
  * What does a purchase of this size look like?
  * Has a purchase of this size happened before?
  * Do you know who the actual signer is? 
  * Do you know if the PO gets automatically generated once the business approves the order form or if the PO needs to also get approved by someone in purchasing/procurement?
* **(I) Identify Pain**: *the problems created by the customer's current state (nobody buys until there is pain!)* 
  * What are the customer's primary objectives with this project or initiative?
  * What are the biggest business and technical pains? 
  * Are those pains resulting in lost productivity, reduced revenues, increased costs, etc.? 
  * How severe is the pain? 
  * Does the customer perceive that the pain of change is less than the pain of doing nothing and sticking with the status quo? 
  * Is there a compelling event that adds to the pain? 
* **(C) Champion**: *the person(s) with power and influence inside the customer’s organization who are actively selling on your behalf and willing to give you inside information to help you win the deal* 
  * Who is your Champion? 
  * Does this person know he/she is your Champion?
  * Is your Champion strong enough to help you connect the dots, give you access to people, and tell you if you're on track or not? 
  * How have you tested him/her to ensure he/she is a true Champion?
* **(C) Competition**: *competitive strengths, weaknesses, and differentiators, including competitor’s champions* 
  * Who is our top competition in this opportunity? 
  * Where are we strong (Differentiators) and where are we weak relative to the competition *as perceived by the customer* (perception is reality!)? 
  * Have you discussed this with your client?
  * Who are your competitors' Champions?
  * What is the competition's relative power and influence in the account?
  * What is your plan to counter your competition's sales strategy?

### Studying your customer and prospects accounts to bring maximum value 

Every GitLab customer is unique. 
For this we believe we must spend time understanding which of their Problems can GitLab solve in order to show them how and if we can help. 

It starts by understanding the bigger picture your customer is working towards, this means taking a step back from the technical requirements, to understand why those are important to the business. 

[Build a Value Deck](https://docs.google.com/presentation/d/1ocQwS3IO320hV9rXnXuwZTW5JTIdmuT3aVX4nvtmK8w/edit#slide=id.g5e9f5a6cd8_0_480)

A Value Deck is dedicated to your focus accounts on which you will be spending more time. 
It is a deck that contains high level information on the Corporate Objectives, Business Strategies, Key initiatives and Challenges. 

We then use this information to formulate a Problem Statement that sums up your customer’s Challenges and what could happen if the Business Strategies and thus Corporate Objectives are not met.

Spending time on this will help you understand your customers better to work smart together, it will also help you formulate the business value at the Commercial Validation stage. 

You can introduce the Value Deck at any stage but it is always best to do it before you start prospecting as it can help you build customised emails to your different accounts.

Make it collaborative: work on it with your customer (or print a blank version and offer them to fill it), and have them review your findings to see if there is anything to add. 
Your champions will appreciate that, and can share more details about their Challenges and Key Initiatives than Uncle Google. 


### Standard Implementation Sequence

The GitLab product is incredibly well matched to customer needs. In fact, we almost always earn a customer and grow quickly if they simply 1) give it a real try and 2) understand it. That means our best sales motion is to ensure we accomplish both at the same time. We've learned that a hands-on workshop is the best way to help the customer understand and make good decisions from the first conversation to the first expansion decision to the move to a higher tier and to the renewals down the road. At every step, a workshop is the most efficient and effective way to serve the customer.

Goal for every conversation with a customer:
* **Agree to a workshop**
  * A workshop is a great way to _give value_ to our prospect and have a conversation in context about GitLab.  Even if the prospect does not move forward with GitLab, they should always be much better off for having done the workshop. This can be a workshop of any kind as long as a) the key people at the account are learning about the full capability of GitLab in their context and b) they are learning hands-on with the product (not just in slides).  When the customer has the right people in one place and working hands-on to see what's possible, GitLab almost always moves to the next stage of the adoption or growth process with the customer.  The product is an incredible fit, they simply need to see it live and in their context.  A workshop is much more effective than a "trial" opportunity as we can greatly accelerate both the time to value for the customer and the time to understanding GitLab in their own unique environment.

Sales Motion and Sequence:
1. **Initial conversations:** In an early conversation when a customer is still trying to learn about GitLab, _**agree to a workshop**_ with decision maker and involved team members to learn about end-to-end devops in a hands-on workshop to see what would be possible in their environment and exit with a working example to evaluate and better understand DevOps at 5x speed and better security and quality at the same time.
   1. DELIVER: DevOps Best Practices workshop
   1. Give customer a complete understanding of how their own company could be successful with end-to-end DevOps and GitLab in their own environment
   1. Hands-on setup of GitLab in their own environment => This is now the PoC deployment in place and running
   1. Begin PoC and with framework to measure success and report back on their real-world experience
   * On completion, move to an initial deployment and opportunity to become a customer.
1. **First Purchase:** When a successful PoC is completed: _**agree to another workshop**
   1. DELIVER: Demo of how to use GitLab in their phase 1 deployment.  Show the desired end-state to the customer.
   1. Train customer on GitLab best practices
   1. Set customer up with Shared Compute (attached [Runners](https://docs.gitlab.com/ee/ci/runners/) and [Kubernetes clusters](https://docs.gitlab.com/ee/user/project/clusters/))
   1. Leave demo with agreed deployment plan, timeline and target results
   * On completion: customer purchases subscription
1. **CI Expansion conversations:** If not in the first deployment, as soon as possible _**agree to a CI workshop**_
   1. DELIVER: CI best practices workshop, training and hands-on training for customer
   2. Set up shared runners for the customer during the workshop every time.  Ensure this is in place before completing the workshop
   3. Schedule CI training for the broader team at the customer
   * On completion: Expand subscription user base as customer gets broader value from use of CI without having to move up in tier
1. **Department expansion conversations:** When a customer has great success with GitLab in part of the company but other departments/organizations have not yet adopted: _**agree to a workshop**_ with the new organization to save them time.  In a 4 hour workshop, that new organization can have developers, devops managers, integration teams, etc. all emerge with:
   1. A complete understanding of how their own company has been successful with end-to-end DevOps and GitLab in their own environment
   1. A working example to evaluate real development in their own environment
   1. A framework to measure success and report back on their real-world experience
   * On completion: deploy production users on already implemented instance and ensure all are fully trained and successful.
1. **Up Tier conversations:** When a customer is interested in capabilities in a higher tier (security in Ultimate, for example) _**agree to a workshop**_ so they can see the benefit of end-to-end DevOps with security from the very first commit. These are 1 day workshops and a big investment in our customers' success:
   1. DevSecOps Workshop: Dev, Ops, and Security team members fully understanding DevSecOps and how to implement it. Security automation, auto DevOps, etc. Implement Ultimate in workshop to allow hands-on experience and results.
   1. Kubernetes Workshop: Cloud-native best practices workshop for fully-modern development with fast cycle time and high security and quality.  Support our customers to be expert in Kubernetes.
   1. End-to-end DevOps: Using all stages of DevOps.  Use remaining stages of GitLab for fastest possible cycle time with highest possible security and quality.
   * In both workshops, deliver a framework to measure success and proof points in agreed cases that are important in their specific context.
   * On success, renew customer on higher tier (Ultimate, in this case) and ensure all users are trained and fully utilizing the new capabilities.


### Tracking Proof of Concepts (POCs)

In Stage 3-Technical Evaluation, a prospect or customer may engage in a proof of concept as part of the evaluation. If they decide to engage, please fill in the following information, as we will use this to track existing opportunities currently engaged in a POC, as well as win rates for customers who have engaged in a POC vs. those that do not.

1. In the opportunity, go to the 3-Technical Evaluation Section.
1. In the POC Status field, you can enter the status of the POC. The options are Not Started, Planning, In Progress, Completed, or Cancelled.
1. Enter the POC Start and End Dates.
1. Enter any POC Notes.
1. Enter the POC Success Criteria, ie how does the prospect/customer plan to determine whether the POC was successful or not.

### Capturing Purchasing Plans

Once you have reached Stage 4-Proposal, you should agree to a purchasing plan with your prospect/contact. This will include a clear understanding of purchase/contract review process and a close plan. This should include actions to be taken, named of people to complete actions and dates for each action. The information captured in this field will be used by sales management to see if the deal is progressing as planned. Delays in the purchasing plan may result in a request to push the opportunity into the following period.

## How Sales Segments Are Determined

Sales Segmentation information can be found in the [Business Operations - Database Management](/handbook/business-ops/resources/#segmentation) section.

## Export Control Classification, and Countries We Do Not Do Business In

GitLab's Export Control Classification (or ECCN) is 5D992.c. As a consequence of this classification, we currently do not do business in: Iran, Sudan (excluding South Sudan), Syria, North Korea, and Cuba.

# Seeing how our customers use GitLab

## Adoption - GitLab Usage Statistics

Using [GitLab Version Check](/handbook/sales/process/version-check/), GitLab usage data is pushed into Salesforce for both CE, EE and EE trial users. Once in Salesforce application, you will see a tab called "Usage Statistics". Using the drop down view, you can select CE, EE trails or EE to see all usage data sent to Gitlab.
Since version check is pulling the host name, the lead will be recorded as the host name.  Once you have identified the correct company name, please update the company name. Example: change gitlab.boeing.com to Boeing as the company name.

To view the usage, click the hyperlink under the column called "Usage Statistics".  The link will consist of several numbers and letters like, a3p61012001a002.
You can also access usage statistics on the account object level within Salesforce.  Within the account object, there is a section called "Usage Ping Data", which will contain the same hyperlink as well as a summary of version they are using, active users, historical users, license user count, license utilization % and date data was sent to us.

A few example of how to use Usage Statistics to pull insight and drive action?
* If prospecting into CE users, you can sort by active users to target large CE instances. You can see what they are using within GitLab, like issues, CI build, deploys, merge requests, boards, etc.  You can then target your communications to learn how they are using GitLab internally and educate them on what is possible with EE.
* For current EE users who are below their license utilization, you can engage the customer to understand their plan to rollout GitLab internally and how/where we can help them with adoption.
* For current EE users who are above their license utilization, you can leverage the data to engage the customer.  Celebrate the adoption of GitLab within their organization.  Ask why the adoption took off?  How they are using it (use cases)? Engage them in amending their contract right now for the add-on? Update the renewal opportunity to reflect the increase in usage for the true-up and new renewal amount.
* For current EE users who are not using a EE feature, i.e. CI or issues, you can engage the customer to understand why they are not using it.  Do they know we offer it? Are they using a competitive tool?  Have the integrated their current tool into GitLab? Are they open to learning more about what we offer to replace their current tool?
* For EE trials.  What EE features are they using and not using? If using a EE feature, what are they trying to solve and evaluate? If not using, why and are they open to evaluating that feature?

Take a look at a Training Video to explain in greater detail by searching for the "Sales Training" folder on the Google Drive.

## Open Source users - GitLab CE Instances and CE Active Users on SFDC Accounts

In an effort to make the [Usage/Version ping data](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html) simpler to use in SFDC, there are 2 fields directly on the account layout - "CE Instances" and "Active CE Users" under the "GitLab/Tech Stack Information" section on the Salesforce account record.

The source of these fields is to use the latest version ping record for and usage record for each host seen in the last 60 days and combine them into a single record. The hosts are separated into domain-based hosts and IP-based hosts. For IP based hosts, we run the IP through a [reverse-DNS lookup](https://en.wikipedia.org/wiki/Reverse_DNS_lookup) to attempt to translate to a domain-based host. If no reverse-DNS lookup (PTR record) is found, we run the IP through a [whois lookup](https://en.wikipedia.org/wiki/WHOIS) and try to extract a company name, for instance, if the IP is part of a company's delegated IP space. We discard all hosts that we are unable to identify because they have a reserved IP address (internal IP space) or are hosted in a cloud platform like GCP, Alibaba Cloud, or AWS as there is no way to identify those organizations through this data. For domain-based hosts, we use the Datafox &/or DiscoverOrg domain lookup API to identify the company and firmographic data associated with the organization and finally the [Google Geocoding API](https://developers.google.com/maps/documentation/geocoding/start) to parse, clean, and standardize the address data as much as possible.

These stats will be aggregated up and only appear on the Ultimate Parent account in SFDC when there are children (since we don't really know which host goes to which child).

To see a list of accounts by # of CE users or instances, you can use the [CE Users list view](https://na34.salesforce.com/001?fcf=00B61000004XccM) in SFDC. To filter for just your accounts, hit the "edit" button and Filter for "My Accounts". Make sure to save it under a different name so you don't wipe out the original. These fields are also available in SFDC reporting.

A few caveats about this data:
* The hosts are mapped based on organization name, domain, and contact email domains to relate the instances to SFDC accounts as accurately as possible, but we may occasionally create false associations. Let the Data & Analytics team know when you find errors or anomalies.
* Both fields can be blank, but the organization can still be a significant CE user. Both fields being blank just means that there are no instances sending us version or usage pings, not necessarily that there are no active instances.
* Users can be the same person in multiple instances, so if a user exists in several instances at an organization with the usage ping on, they are counted each time they appear in an instance. Users may also be external to an organization, so the number of users may not represent employees and can thus be higher than the number of employees.
* These numbers represent the minimum amount that GitLab is likely being used within the organization, since some instances may have these pings turned on and some off. They also may have just the version ping on, in which case we won't see the number of users. This should not be interpreted as having instances but no users.

For the process of working these accounts, appending contacts from third-party tools, and reaching out, please refer to the [business operations](/handbook/business-ops/) section of the handbook.

# How we use our systems

## GitLab Tech Stack

For information regarding the tech stack at GitLab, please visit the [Tech Stack Applications page](/handbook/business-ops/tech-stack-applications/#tech-stack-applications) of the Business Operations handbook where we maintain a comprehensive table of the tools used across the company, in addition to a 'cheat-sheet' for quick reference of who should have access and whom to contact with questions.

## Collaboration Applications used by Sales

Please [use the handbook](/handbook/handbook-usage/#why-handbook-first) as much as possible instead of traditional office applications.
When you have to use office applications always use [Gsuite applications](https://gsuite.google.com/together/) instead of Microsoft Office; for example, Google Sheets instead of Excel, Google Docs instead of Word, and Google Slides instead of Keynote or Powerpoint. Give the entire company editing rights whenever possible so everyone can contribute.

## Parent and Child Accounts

* A Parent Account is the business/organization which owns another business/organization.  Example: The Walt Disney Company is the parent account of Disney-ABC Television Group and Disney.com.
* A Child Account is the organization you may have an opportunity with but is owned by the Parent Account. A Child Account can be a business unit, subsidiary, or a satellite office of the Parent Account.
* You may have a opportunity with the Parent account and a Child Account.  Example: Disney and ESPN may both be customers and have opportunities. However, the very first deal with a Parent Account, whether it is with the Parent Account or Child Account, should be marked as "New Business". All other deals under the Parent Account will fall under Add-On Business, Existing Account - Cross-Sell, or Renewal Business (see Opportunity Types section).
* If the Parent and Child accounts have the same company name, either add the division, department, business unit, or location to the end of the account name. For example, Disney would be the name of the Parent Account, but the Child Account would be called The Walt Disney Company Latin America or The Walt Disney Company, Ltd Japan.
* When selling into a new division (which has their own budget, different mailing address, and decision process) create a new account.  This is the Child Account.  For every child account, you must select the parent account by using the parent account field on the account page. If done properly, the Parent/Child relationship will be displayed in the Account Hierarchy section of the account page.
* Remember that a child account can also be a parent account for another account. For example, Disney-ABC Television Group is the child for The Walt Disney Company, but is the parent for ABC Entertainment Group.
* We want to do this as we can keep each opportunity with each child account separate and easily find all accounts and opportunities tied to a parent account, as well as roll-up all Closed Won business against a Parent Account.

## When to Create an Opportunity with Software License Fees

Before a LEAD is converted or an OPPORTUNITY is created the following must occur:

1. Authority
  * What role does this person play in the decision process? (i.e. decision maker, influencer, etc.) OR is this someone that has a pain and a clear path to the decision maker
1. Need
  * Identified problem GitLab can solve
1. Required Information
  * Number of potential paid users
  * Current technologies and when the contract is up
  * Current business initiatives
1. Handoff
  * The prospect is willing to schedule another meeting with the salesperson to uncover more information and uncover technical requirements and decision criteria
1. Interest by GitLab salesperson to pursue the opportunity.  Are you interested in investing time in pursuing this opportunity.

* When creating any OPPORTUNITY be sure to review the criteria needed for each [Opportunity Stage](/handbook/business-ops/resources/#opportunity-stages) and be aware that all opportunities must enter the "O-Pending Acceptance" stage or you will encounter a validation rule error.

### Creating an Opportunity

Opportunities can only be created *from* a CONTACT record or when *converting* a LEAD. See the ['How to create an Opportunity'](/handbook/business-ops/resources/#how-to-create-an-opportunity) section of the Business OPS handbook.


## True up

***What is it?***

A true up is a back payment for a customer going over their license count during the year.

***Why do we use it?***

We use the true up model so that the license never becomes a barrier to adoption for the client.

***Let's take an example.***

A customer takes out a subscription for 100 users.  During the year they grow the usage of the server and 200 additional users come on board.  When it comes time to renew, they are now up to 300 active users despite the license only being for 100 users.

At this point the customer needs to do two things: they need to renew the license and to add the true up users.  The number of true up users in this case is 200.  It is the difference between the `Maximum Users` and the `Users in License`.  This can be found by the customer in their GitLab server by going to the Admin > License area.  It will look like [this](https://docs.gitlab.com/ee/user/admin_area/img/license_details.png)

There is more information below on the steps you need to take, but in this example you would need to update the number of users for the renewal to 300 and add the `True Up` product to the renewal quote for 200 users.  This will create a one time only charge to the customer and in a year's time, they will have a renewal for 300 users.

[***See here more information on the steps to take for the true up***](#renew-existing-subscription-with-a-true-up-for-the-billing-account)

***Note that we make provision for true ups in our standard [Subscription Terms](/terms/#subscription) Section 5 - Payment of Fees.***

## Deal Sizes

Deal Size is a dimension by which we will measure stage to stage conversions and stage cycle times of opportunities. Values are [IACV](/handbook/finance/operating-metrics/#bookings-incremental-annual-contract-value-iacv) in USD.

1. Jumbo - USD 100,000 and up
1. Big - USD 25,000 to 99,999.99
1. Medium - USD 5,000 to 24,999.99
1. Small - below USD 5,000

Note that we use the same amounts to group customers by ARR.

## Associating Emails to Opportunities

Associating emails to Salesforce is a best practice that should be followed by all users. Attaching emails to a Lead, Account, Contact, or Opportunity record are vital for transparency, as any number of team members can review the correspondence between GitLab and the customer/prospect account. It doesn't do anyone any good when all of the correspondence is kept in a user's inbox.

With that said, there are multiple ways that an email can be associated to an opportunity. But before you do that, you must first make sure that the contact record is associated to an opportunity via the Contact Roles object.

### First, Contact Roles

To add a Contact Role to an opportunity:
1. Go to the Opportunity.
1. Go to the Contact Role related list.
1. Click 'New'.
1. Add the Contact.
1. If this person is your primary contact, click on the 'Primary' radio button.
1. Add the Role this person plays in the opportunity (Economic Buyer, Technical Buyer, etc).
1. Repeat the steps, although you can only have one Primary Contact per opportunity.
1. For more information, you can visit the [Salesforce Knowledge Center](https://help.salesforce.com/articleView?id=contactroles_add_cex.htm&type=5).

### Email to Salesforce

Email to Salesforce allows you to associate emails to Opportunities using an email long email string that associates the email to the contact. To set this up:

1. Click on your 'Name' on the top right of any page in Salesforce.
1. Select 'My Settings'
1. Go to the left sidebar and click 'Email'.
1. Then click 'My Email to Salesforce'.
1. Copy the email string.
1. Go to My Acceptable Email Addresses and make sure your gitlab email address is there.
1. In 'Email Associations', make sure "Automatically assign them to Salesforce records" is checked.
1. Check Opportunities, Leads, and Contacts.
1. In the Leads and Contacts section, make sure to select "All Records".
1. It is up to you if you want to save all attachments, as well as if you want to receive an email confirmation of an association (my recommendation is yes for the first few weeks to make sure it's working, then you can turn it off anytime).
1. Click Save.
1. Go to Gmail and save this email address in a Contact record. A good practice is name it 'BCC SFDC' for first and last name.
1. When you send any emails from Gmail, you will BCC the "BCC SFDC" contact, which will send the email to long string you copied in Step 5.

### Outreach

If you want to associate emails to Opportunities using Outreach, follow these steps:

1. Go to your photo on the bottom left.
1. Click Settings
1. Select 'Plugins' on the left menu.
1. Select the SFDC (Salesforce) plugin.
1. Click on 'Contact'.
1. On the bottom right, enable 'Automatically associate activity with Opportunity'
1. Click the 'Save' button on the top right.

### Salesforce Lightning for Gmail

If you want to associate emails to Opportunities or other records using the Salesforce Lightning for Gmail plug in, follow these steps:

1. Visit the Chrome Store to download the [Salesforce Lightning for Gmail](https://chrome.google.com/webstore/detail/salesforce-lightning-for/jjghhkepijgakdammjldcbnjehfkfmha) plug in.
1. Click `Add to Chrome`
2. Click `Add Extension`
2. Go to Gmail and open the right sidebar.
3. When you open an email that may contain email addresses from existing leads or contacts in Salesforce, all related records associated to that email (Lead, Contact, Account, Opportunity, and Cases) will appear, and you can select any or all records to related the email to.
3. For each record you'd like to associate the email to, click on the upload icon for each record.
4. If you wish, you can include some or all of the attachments to the record as well by clicking the checkbox next to each attachment.

If you have any issues with the setup, please contact the Director of Sales Operations via Slack, email, or SFDC Chatter.

## Sales Order Processing

See the [order processing](/handbook/business-ops/order-processing/) page for information and training related to using Salesforce to create accounts, contacts, opportunities, etc.; opportunity reassignments; returning customer creation; and more.

## Elevator pitch

Please see [our value proposition](/handbook/marketing/product-marketing/messaging/#gitlab-value-proposition).

# Sales Note Taking

Gitlab is an organisation that is built on contribution and collaboration. These values are not only critical for building a great product, they are critical for building a world class commercial team. Each member of the sales team is expected to create insider notes in Salesforce on accounts and conversations they are involved with. With Gitlab being a remote company, it weighs even more importance on creating and maintaining a culture of great note taking.

Read: [How we take notes at Gitlab](https://docs.google.com/document/d/1l5mfHMck914I1aJMOCX4lU2J2sImrloUpSm3Nb5Pa9U/edit)

# SMB Commercial Pitch Deck (short)

SMB Customer Advocates may use this deck as a guide for sales meetings.  Speaking tips and best practices are included in the Comments section of each slide.  

Deck: https://docs.google.com/presentation/d/1ypo8PNB5TFK1PboMUyU_D5QjRkRR92uzYpS3pYyQgbw/edit?usp=sharing