---
layout: markdown_page
title: "Sales Discovery and Qualification Questions"
---

An [effective questioning strategy](https://docs.google.com/presentation/d/1caRLqSGNhFAQ9KuRVBaxI9cExOvBQBC4hd36uVfWFvs/edit?usp=sharing) is critical to engaging customers in dialogue that will help you truly understand their needs, build credibility and trust, and qualify the opportunity. Furthermore, effective discovery and needs analysis will be instrumental to your developing a tailored, compelling, customer-centric solution that will increase your chances to win the business and advance the buying decision. 

Target buyer personas include CIO, CTO, VP of Application Development, Product Owner, and App Developer/DevOps Engineer.

## **CUSTOMER STRATEGY** 

### Goal
*  So that we may best understand your organization’s needs and how we can help, what are your long-term goals for modernizing your application development practices to deliver increased value to the business?
*  What are you trying to achieve and why are these goals important?
*  What is  your strategy for increasing the frequency of app deployments? If you were able to do this, what would the impact be to the business?
*  How will you measure success for each of your goals?

### Objectives
*  With an understanding of your long-term goals, what are your key objectives for the next 6-12 months?
Challenges
*  What challenges are you experiencing with modernizing your application development practices? 
*  If you could address or improve one or a just a few things in your current DevOps stack implementation and execution, what would that be and why?
*  What has prevented the move to <x> so far?
*  What are the implications if these problems were to continue to persist?
*  As you look into the future, what other challenges or obstacles do you anticipate encountering? What are you doing today to prepare for those challenges?
*  What else is holding your team back from being even more successful?

### Initiatives
*  On what prioritized initiatives are you and your team currently focused?
*  What initiatives are currently being planned or under consideration? 
*  Where is this interest/initiative coming from and what is the scope? (note: higher priority initiatives are often more likely to close and close more quickly)
    - Is it a company-wide or business unit-wide initiative? Or other? 
*  Do you have cloud-native initiatives? If so, what solutions are you exploring (i.e., Kubernetes)?

## **CUSTOMER NEEDS**

### Desired Outcomes
*  What are the requirements you have for this project? What does success look like?
*  How will success be measured?

### Current Situation
*  What can you tell me about your current situation? 
*  What led you to look at GitLab? (if applicable)
*  What is your role and what do you do? (ideal roles: Infrastructure/Cloud/Enterprise Architects, Dev Ops, System Admin/Engineer, IT Managers/Directors, Release Engineer, VP Engineering)
*  How would you describe what your group does?
*  How large is your group? (goal: find out who will use GitLab and how large the opportunity could be)
*  How would you describe the projects on which your dev teams are currently working?
*  If you were to rate how modern your organization’s application development practices are on a scale of 1 to 5 (1 being not very modern at all, 5 being best-in-class modern), what would your rating be and why? What would it take to move the rating up?
*  What tools are you currently using in your DevOps stack (or application development toolchain)?
    - What are you using for issue & bug tracking?
    - What are you using for CI?
    - What are you using for code review?
    - What tool(s) do you leverage for application deployment?
    - How would you describe your organization’s current and future plans for adoption of the following:
        - Containers
        - VMs
        - Microservices
        - Kubernetes
        - What tools do you use for project management?
*  What platforms/clouds are you deploying to?  (AWS/GCP/Azure/VMware/Openshift/other)
    - If AWS are you deploying to EC2, ECS, EKS, Fargate, or Lamda
    - If GCP are you deploying to GCE, GKE, App Engine, or Cloud Run
    - If Azure are you deploying to VMs, AKS, Functions
*  How would you describe your maturity level/progress with continuous integration and continuous delivery Is this something your group and company want to improve upon?
*  How are you using GitLab CE right now? (note: if applicable--check https://version.gitlab.com/ to identify if their company is on CE)
*  Are other groups using Git? If no, why not and what Version Control System(s) are they using? If yes, what are they using?
*  How is security testing currently done in your organization?
*  What is the current process for finding vulnerabilities around Static Application Security Testing, Dependency Scanning, and/or License Compliance?
*  What is the financial impact on your organization/teams budget with other test and review tools?

### Level of Satisfaction
*  How is your current application development & delivery tool stack working overall?
*  What is working well?
*  If you could change anything about it, what would that be and why? 
*  What elements are presenting the most challenges today? Why are those challenges proving to be so pesky? 
*  What are the underlying root causes for the issues you and the team are experiencing?
*  What else do you need to be successful?

### Personal Needs
*  What’s most important to *you* in accomplishing this objective(s)?
*  What makes you personally so committed to the success of the project?
*  What will it mean to you personally when the project is successful?
*  What are the implications to you personally if it is not successful?
*  What concerns you most?
*  What is most important to you as it relates to your relationship with your supplier/partner?
*  How do you prefer to work with your suppliers?
*  What have you heard about and what are your perceptions of GitLab? 
*  Do you have any prior experience with GitLab? How did you find out about us?

## **CUSTOMER DECISION**

### Decision Process
*  How would you describe the decision-making process at your organization?
*  How many individuals or teams are involved in the decision making process?
*  What executive or senior level sponsorship is required?
*  Does each team/group purchase their own solutions or is there a department who does this? If a centralized department, what is that group’s name?
*  How does high availability rank within your needs and what you are trying to accomplish?

### Decision Criteria
*  What criteria will be used in making a decision?

### Budget and Timeline
*  When are you looking to implement? What is driving that timeline and is there any flexibility in the timeline?
*  To what extent is there a compelling event (or series of events) that are influencing/impacting the timeline? What else can you tell me about that?
*  What budget and resources have been allocated to this project?

### Competitors
*  Are you considering other Git-based offerings? If so, who?
*  What other suppliers/vendors/solutions are you considering and/or evaluating? 
*  How is GitLab viewed in comparison to the other suppliers?

## **ADDITIONAL QUESTIONS IF…**

### Prospect Signed Up For a Free Trial (goal: ensure a successful trial)
*  How would you and your organization describe a successful trial?
*  What are the most important criteria in your evaluation?
*  How many users are you interested in deploying on GitLab?
*  Are you evaluating any other tools?
*  What is your timeframe for making a decision?
*  Are you looking to bring one division of your company on to EE?
*  If only a group, what is preventing you from bringing on the entire company at this time?

### Competing/Selling Against GitHub
*  Review the battle card and discovery questions [here](https://docs.google.com/presentation/d/1AgPA-2WiHsHdVJABsnYWbBpMHtsi1nVigde1fv9igNU/edit?usp=sharing)

### Competing/Selling against Jenkins
*  Review the battle card and discovery questions [here](https://docs.google.com/presentation/d/1ILMuOkg0QlFa8zBptQPANqPWh3aceQE4x4yWT_Y5md0/edit?usp=sharing)

### Customer is Using Jira
*  How would you describe the current implementation of Jira within your organization?
*  What are some of the challenges you have with Jira (if any)?

### Post Sale Account Development (goal: understand why they bought and use nuggets to further expand the sale and help with other sales)
*  How is the adoption of GitLab going?
*  What are you liking about GitLab EE?
*  What are you not liking or have questions about?
*  How has GitLab helped your group? What are their use cases?
    - Improve code quality?
    - Speed up code releases?
*  How many projects do they have?
*  Is there anything you or your team would like to see within GitLab?
*  Are you seeing any challenges with not having other groups using GitLab as well?
*  Are there other groups that you think would benefit using GitLab?
*  If yes, how would you suggest getting in contact with them?
*  How many of the provisioned licenses are being used?
*  If low adoption, Is there anything I can do to help you?
*  If high adoption of licenses, Ask what their growth plans are and when they feel they will need to increase their seats?
*  Would you be willing to be a reference customer for GitLab? If so, how has GitLab helped you and your organization?
