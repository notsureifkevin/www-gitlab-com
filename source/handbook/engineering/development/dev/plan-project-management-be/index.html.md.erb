---
layout: markdown_page
title: Plan:Project Management Backend Team
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Plan:Project Management backend team

The Plan:Project Management backend team works on the backend part of
GitLab's [Project Management category] in the [Plan stage].

For more details about the vision for this area of the product, see the
[Plan stage] page.

From an engineering perspective, we are also responsible for the [code
backing our GraphQL API][graphql]. This does not mean we own everything
about the API - each team is responsible for implementing its own
resources in GraphQL - but we are responsible for the overall
stewardship of this API.

[Project Management category]: /handbook/product/categories/#project-management-group
[Plan stage]: /direction/plan/
[graphql]: https://gitlab.com/groups/gitlab-org/-/epics/711

### Team members

<%= direct_team(manager_role: 'Engineering Manager, Plan:Project Management') %>

### Stable counterparts

<%= stable_counterparts(role_regexp: /[,&] (Plan(?!:)|Plan:Project Management)/, direct_manager_role: 'Engineering Manager, Plan:Project Management') %>

### Hiring chart

This chart shows the progress we're making on hiring. Check out our
[jobs page](/jobs/) for current openings.

<%= hiring_chart(department: 'Plan:Project Management BE Team') %>

## Work

You can see how we work as a stage at the [Plan stage page].

For the backend team specifically, we use the standard GitLab
[engineering workflow]. To get in touch with the Plan:Project Management
backend team, it's best to [create an issue] in the relevant project
(typically [GitLab CE]) and add the ~"group::project management" label, along
with any other appropriate labels. Then, feel free to ping the relevant
Product Manager and/or Engineering Manager as listed above.

For more urgent items, feel free to use [#g_plan] on Slack.

[Plan stage page]: /handbook/product/categories/plan/
[engineering workflow]: /handbook/engineering/workflow/
[create an issue]: /handbook/communication/#everything-starts-with-an-issue
[GitLab CE]: https://gitlab.com/gitlab-org/gitlab-ce

### Capacity planning

We use a lightweight system of issue weighting to help with capacity planning,
with the knowledge that [things take longer than you think]. These weights are
used for capacity planning and the main focus is on making sure the overall sum
of the weights is reasonable.

It's OK if an issue takes longer than the weight indicates. The weights are
intended to be used in aggregate, and what takes one person a day might take
another person a week, depending on their level of background knowledge about
the issue. That's explicitly OK and expected.

These weights we use are:

| Weight | Meaning |
| --- | --- |
| 1 | Trivial, does not need any testing |
| 2 | Small, needs some testing but nothing involved |
| 3 | Medium, will take some time and collaboration |
| 5 | Large, will take a major portion of the milestone to finish |

Anything larger than 5 should be broken down if possible.

We look at recent releases and upcoming availability to determine the
weight available for a release.

[things take longer than you think]: https://erikbern.com/2019/04/15/why-software-projects-take-longer-than-you-think-a-statistical-model.html

#### Planning rotation

To assign weights to issues in a future milestone, we ask two team members to
take the lead each month. They can still ask questions - of each other, of the
rest of the team, of the stable counterparts, or anyone else - but they are the
initial. To weight issues, they should:

1. Look through the issues on the milestone filtered by Weight:None.
2. For those they understand, they add a weight. If possible, they also add a
   short comment explaining why they added that weight, what parts of the code
   they think this would involve, and any risks or edge cases we'll need to
   consider.
3. Timebox the issue weighting overall, and for each issue. The process is
   intended to be lightweight. If something isn't clear what weight it is, they
   should ask for clarification on the scope of the issue.
4. If two people disagree on the weight of an issue, even after explaining their
   perceptions of the scope, we use the higher weight.
5. Start adding weights around a week before the weights for a milestone
   are due. Finishing earlier is better than finishing later.

The rotation for upcoming releases is:

| Release | Weights due | Engineer       | Engineer          |
| ---     | ---         | ---            | ---               |
| 12.3    | 2019-08-07  | Felipe Cardozo | Heinrich Lee Yu   |
| 12.4    | 2019-09-07  | Charlie Ablett | Mario de la Ossa  |
| 12.5    | 2019-10-07  | Brett Walker   | Alexandru Croitor |

### Picking something to work on

The [Plan backend board] always shows work in the current release, with
the left column being items that are:

1. In priority order, with priorities set from the Product Manager.
2. Not assigned to anyone in the Plan backend team. (They may be
   assigned to people from other teams; for instance, if an issue needs
   backend, frontend, and UX work, then the frontend and UX parts may
   already be assigned.)

It's OK to not take the top item if you are not confident you can solve
it, but please post in [#g_plan] if that's the case, as this probably
means the issue should be better specified.

### Working on unscheduled issues

Everyone at GitLab has the freedom to manage their work as they see fit,
because [we measure results, not hours][results]. Part of this is the
opportunity to work on items that aren't scheduled as part of the
regular monthly release. This is mostly a reiteration of items elsewhere
in the handbook, and it is here to make those explicit:

1. We expect people to be [managers of one][efficiency], and we [use
   GitLab ourselves][collaboration]. If you see something that you think
   is important, you can [request for it to be scheduled], or you can
   [work on a proposal yourself][iteration], as long as you keep your
   other tasks in mind.
2. From time to time, there are events that GitLab team-members can participate
   in, like the [issue bash] and [content hack days]. Anyone is welcome
   to participate in these.
3. If you feel like you want to have some specific time set aside, but
   aren't interested in the topics of an existing event, feel free to
   label issues with "For Scheduling" and copy your manager for visibility.

When you pick something to work on, please:

1. Follow the standard workflow and assign it to yourself.
2. Share it in [#g_plan] - if not even more widely (like in #development
   or #backend).

[collaboration]: /handbook/values/#collaboration
[results]: /handbook/values/#results
[efficiency]: /handbook/values/#efficiency
[iteration]: /handbook/values/#iteration

[request for it to be scheduled]: /handbook/engineering/workflow/#requesting-something-to-be-scheduled
[issue bash]: /community/issue-bash/
[content hack days]: /handbook/marketing/corporate-marketing/content/content-hack-day/

## Useful links

* [Plan backend board] - this shows work in the current release
* [#g_plan] in Slack
* [Recorded meetings][youtube]
* [Retrospectives][retros]
* [Group Conversations] (archive; group conversations now happen at a the
  [section level])

[Plan backend board]: https://gitlab.com/groups/gitlab-org/-/boards/631316

[#g_plan]: https://gitlab.slack.com/archives/g_plan
[youtube]: https://www.youtube.com/playlist?list=PL05JrBw4t0KoceqcTneOVmAzhEp6NinY0
[retros]: https://gitlab.com/gl-retrospectives/plan/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=retrospective
[Group Conversations]: http://gitlab-org.gitlab.io/group-conversations/plan/
[section level]: /company/team/structure/#organizational-structure
