---
layout: markdown_page
title: "GitLab Security Compliance Controls"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# GitLab's Security Controls

Security controls are a way to state our company's position on a variety of security topics. It's not enough to simply say "We encrypt data" since our customers and teams will naturally want to know "what data do we encrypt?" and "how do we encrypt that data?". When all of our established security controls are operating effectively this creates a security program greater than the sum of its parts that will demonstrate to our stakeholders that GitLab has a mature and comprehensive security program that will provide assurance that data within GitLab is reasonably protected.

## GitLab Control Framework (GCF)

We have tried to take a comprehensive approach to our immediate and future security compliance needs. Older and larger companies tend to treat each security compliance requirement individually which results in independent security compliance teams going out to internal teams with multiple overlapping requests. For example, at such a company you might have one database engineer that is asked to provide evidence of how a particular database is encrypted based on SOC2 requirements, then again for ISO requirements, then again for PCI requirements. This approach can be visualized as follows:

```mermaid
graph TD;
    SOC2_Requirement1-->Team1;
    SOC2_Requirement1-->Team2;
    SOC2_Requirement2-->Team1;
    SOC2_Requirement2-->Team2;
    PCI_Requirement1-->Team1;
    ISO_Requirement1-->Team2;
```

Given our [efficiency value](https://about.gitlab.com/handbook/values/#efficiency) here at GitLab we wanted to create a set of security controls that would address multiple underlying requirements with a single security control which would allow us to make fewer requests of our internal teams and efficiently collect all evidence we would need for a variety of audits at once. This approach can be visualized as follows:

```mermaid
graph TD;
    SOC2_Requirement1-->GCF;
    SOC2_Requirement2-->GCF;
    PCI_Requirement1-->GCF;
    ISO_Requirement1-->GCF;
    GCF-->Team1;
    GCF-->Team2;
```

Adobe's [open source compliance framework](https://blogs.adobe.com/security/2017/05/open-source-ccf.html) served as the starting point for this efficient method of collecting security control evidence. It has been adapted and expanded as needed and the result is the below list of controls grouped by families and sub-families. 

Clicking a control below that has a link will take you to a page with a variety of information about that control.

## Control Prioritization

There are a number of controls without links due to the way that GitLab has prioritized the documentation of all of these controls. A control without a link should not be taken to mean that GitLab is not in compliance with a specific control’s requirements, but rather that we are not yet tracking evidence of the operation of that control. Supporting documentation for all controls will be built out as we continue to iterate on our compliance program and pursue additional compliance certifications. 

## Control Ownership

For an overview of how GitLab team are responsible, accountable, consulted, and informed about security controls, please refer to the [controls RACI chart](./compliance-raci.html).

Control Owner - Ensures that the design of the control and the control activities operate effectively and is responsible for remediation of any control activities that are required to bring that control into a state of audit-readiness.

Process Owner - Supports the operation of the control and carries out the process designed by the control owner. The process owner is most likely to be interviewed by an auditor to determine whether or not the process is operating as intended.

## Data Classification Policy

For GitLab's data classification policy, please refer to the [data classification page](./data-classification-policy.html).

## Security Controls Feedback

If you have any feedback on any of the security controls or related documentation, please add it as a comment in [this issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/218).

## Security Control Changes

The GitLab compliance team is responsible for ensuring the consistency of the documentation of the security controls listed below. While normally we welcome any GitLab team-member to make edits to handbook pages, please be aware that even small changes to the wording of any of these controls impacts how they satisfy the requirements for the security frameworks they map to. Because of this, we ask any changes that need to be made to this page and the underlying guidance pages to start with a comment in [this issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/219). The compliance team will then engage with you and make any appropriate changes to these handbook pages.

# List of controls by family:

## Asset Management
* Device and Media Inventory
    * [Inventory Management](./guidance/AM.1.01_inventory_management.html)
    * Inventory Management: Payment Card Systems
    * Inventory Labels
* Device and Media Transportation
    * Asset Transportation Authorization
    * Asset Transportation Documentation
* Component Installation and Maintenance
    * Maintenance of Assets
    * Tampering of Payment Card Capture Devices

## Backup Management
* Backup
    * [Backup Configuration](./guidance/BU.1.01_backup_configuration.html)
    * [Resilience Testing](./guidance/BU.1.02_resilience_testing.html)
    * Alternate Storage

## Business Continuity
* Business Continuity Planning
    * [Business Continuity Plan](./guidance/BC.1.01_business_continuity_plan.html)
    * [Continuity Testing](./guidance/BC.1.03_continuity_testing.html)
    * [Business Impact Analysis](./guidance/BC.1.04_business_impact_analysis.html)

## Change Management
* Change Management
    * [Change Management Workflow](./guidance/CM.1.01_change_management_workflow.html)
    * [Change Approval](./guidance/CM.1.02_change_approval.html)
* Segregation of Duties
    * Segregation of Duties

## Configuration Management
* Baseline Configurations
    * [Baseline Configuration Standard](./guidance/CFG.1.01_baseline_configuration_standard.html)
    * Default "Deny- all" Settings
    * [Configuration Checks](./guidance/CFG.1.03_configuration_checks.html)
    * [Configuration Check Reconciliation: CMDB](./guidance/CFG.1.04_configuration_check_reconciliation.html)
    * Time Clock Synchronization
    * Time Clock Configuration Access
    * [Default Device Passwords](./guidance/CFG.1.07_default_device_passwords.html)
    * Process Isolation

## Data Management
* Data Classification
    * Data Classification Criteria
* Choice and Consent
    * [Terms of Service](./guidance/DM.2.01_terms_of_service.html)
* Data Handling
    * External Privacy Inquiries
    * Test Data Sanitization
* Data Encryption
    * [Encryption of Data in Transit](./guidance/DM.4.01_encryption_of_data_in_transit.html)
    * [Encryption of Data at Rest](./guidance/DM.4.02_encryption_of_data_at_rest.html)
    * Approved Cryptographic Technology
* Data Storage
    * Credit Card Data Restrictions
    * Personal Account Number Data Restrictions
* Data Integrity
    * Changes to Data at Rest
* Data Removal
    * [Secure Disposal of Media](./guidance/DM.7.01_secure_disposal_of_media.html)
    * Customer Data Retention and Deletion
* Social Media
    * Social Media

## Identity and Access Management
* Logical Access Account Lifecycle
    * [Logical Access Provisioning](./guidance/IAM.1.01_logical_access_provisioning.html)
    * [Logical Access De-provisioning](./guidance/IAM.1.02_logical_access_deprovisioning.html)
    * [Logical Access Review](./guidance/IAM.1.04_logical_access_review.html)
    * Role Change: Access De- provisioning
    * [Shared Logical Accounts](./guidance/IAM.1.06_shared_logical_accounts.html)
    * [Shared Account Restrictions](./guidance/IAM.1.07_shared_account_restrictions.html)
* Authentication
    * [Unique Identifiers](./guidance/IAM.2.01_unique_identifiers.html)
    * [Password Authentication](./guidance/IAM.2.02_password_authentication.html)
    * Multifactor Authentication
    * Authentication Credential Maintenance
    * Session Timeout
    * Account Lockout: Cardholder Data Environments
    * Full Disk Encryption
* Role-Based Logical Access
    * Logical Access Role Permission Authorization
    * [Source Code Security](./guidance/IAM.3.02_source_code_security.html)
    * Service Account Restrictions
    * PCI Account Restrictions
* Remote Access
    * [Remote Connections](./guidance/IAM.4.01_remote_connections.html)
    * Ability to Disable Remote Sessions
    * Remote Maintenance: Authentication Sessions
    * Remote Maintenance: Unique Authentication Credentials for each Customer
* End-user Authentication
    * End-user Environment Segmentation
* Key Management
    * [Key Repository Access](./guidance/IAM.6.01_key_repository_access.html)
    * Data Encryption Keys
    * Key Maintenance
    * Clear Text Key Management
* Key Storage and Distribution
    * Key Store Review
    * Storage of Data Encryption Keys
    * Clear Text Distribution

## Incident Response
* Incident Response
    * [Incident Response Plan](./guidance/IR.1.01_incident_response_plan.html)
    * Incident Response Testing
    * [Incident Response](./guidance/IR.1.03_incident_response.html)
* Incident Communication
    * [External Communication of Incidents](./guidance/IR.2.01_external_communication_of_incidents.html)
    * [Incident Reporting Contact Information](./guidance/IR.2.02_incident_reporting_contact_information.html)
    * [Incident External Communication](./guidance/IR.2.03_incident_external_communication.html)

## Mobile Device Management
* Mobile Device Security
    * Configuration Management: Mobile Devices

## Network Operations
* Perimeter Security
    * [Network Policy Enforcement Points](./guidance/NO.1.01_network_policy_enforcement_points.html)
    * Inbound and Outbound Network Traffic: DMZ Requirements
    * Ingress and Egress Points
    * Non-disclosure of Routing Information
    * Dynamic Packet Filtering
    * Firewall Rule Set Review
* Network Segmentation
    * Network Segmentation
    * Card Processing Environment Segmentation
* Wireless Security
    * Disable Rogue Wireless Access Points
    * Wireless Access Points
    * Rogue Wireless Access Point Mapping
    * Authentication: Wireless Access Points

## People Resources
* On-boarding
    * [Background Checks](./guidance/PR.1.01_background_checks.html)
    * [Performance Management](./guidance/PR.1.02_performance_management.html)
* Off-boarding
    * GitLab Property Collection
* Compliance
    * Disciplinary Process

## Risk Management
* Risk Assessment
    * [Risk Assessment](./guidance/RM.1.01_risk_assessment.html)
    * [Continuous Monitoring](./guidance/RM.1.02_continuous_monitoring.html)
    * Self- Assessments
    * [Service Risk Rating Assignment](./guidance/RM.1.04_service_risk_rating_assignment.html)
* Internal and External Audit
    * [Internal Audits](./guidance/RM.2.01_internal_audits.html)
* Controls Implementation
    * [Remediation Tracking](./guidance/RM.3.01_remediation_tracking.html)
    * Statement of Applicability

## Security Governance
* Policy Governance
    * [Policy and Standard Review](./guidance/SG.1.01_policy_and_standard_review.html)
    * Exception Management
* Security Documentation
    * [Information Security Program Content](./guidance/SG.2.01_information_security_program_content.html)
* Privacy Program
    * Privacy Readiness Review
* Workforce Agreements
    * Proprietary Rights Agreement
    * Review of Confidentiality Agreements
    * Key Custodians Agreement
* Information Security Management System
    * Information Security Management System Scope
    * [Security Roles and Responsibilities](./guidance/SG.5.03_security_roles_and_responsibilities.html)
    * Security Roles and Responsibilities: PCI Compliance
    * Information Security Resources

## Service Lifecycle
* Release Management
    * [Service Lifecycle Workflow](./guidance/SLC.1.01_service_lifecycle_workflow.html)
* Source Code Management
    * Source Code Management

## Site Operations
* Physical Security
    * [Secured Facility](./guidance/SO.1.01_secured_facility.html)
    * Physical Protection and Positioning of Cabling
* Physical Access Account Lifecycle
    * [Provisioning Physical Access](./guidance/SO.2.01_provisioning_physical_access.html)
    * [De-provisioning Physical Access](./guidance/SO.2.02_de-provisioning_physical_access.html)
    * [Periodic Review of Physical Access](./guidance/SO.2.03_periodic_review_of_physical_access.html)
    * [Physical Access Role Permission Authorization](./guidance/SO.2.04_physical_access_role_permission_authorization.html)
    * Monitoring Physical Access
    * Surveillance Feed Retention
    * Visitor Access
* Environmental Security
    * Temperature and Humidity Control
    * Fire Suppression Systems
    * Power Failure Protection

## Systems Design Documentation
* Internal System Documentation
    * [System Documentation](./guidance/SDM.1.01_system_documentation.html)
    * System Documentation: Cardholder Environment

## Systems Monitoring
* Logging
    * [Audit Logging](./guidance/SYS.1.01_audit_logging.html)
    * Secure Audit Logging
    * Audit Logging: Cardholder Data Environment Activity
    * Audit Logging: Cardholder Data Environment Event Information
    * Audit Logging: Service Provider Logging Requirements
    * [Log Reconciliation: CMDB](./guidance/SYS.1.06_log_reconciliation_cmdb.html)
    * Audit Log Capacity and Retention
    * Enterprise Antivirus Logging
* Security Monitoring
    * [Security Monitoring Alert Criteria](./guidance/SYS.2.01_security_monitoring_alert_criteria.html)
    * Log-tampering Detection
    * Security Monitoring Alert Criteria: Failed Logins
    * Security Monitoring Alert Criteria: Privileged Functions
    * Security Monitoring Alert Criteria: Audit Log Integrity
    * Security Monitoring Alert Criteria: Cardholder System Components
    * [System Security Monitoring](./guidance/SYS.2.07_system_security_monitoring.html)
    * Intrusion Detection Systems
* Availability Monitoring
    * [Availability Monitoring Alert Criteria](./guidance/SYS.3.01_availability_monitoring_alert_criteria.html)
    * [System Availability Monitoring](./guidance/SYS.3.02_system_availability_monitoring.html)

## Third Party Management
* Vendor Assessments
    * [Third Party Assurance Review](./guidance/TPM.1.01_third_party_assurance_review.html)
    * [Vendor Risk Management](./guidance/TPM.1.02_vendor_risk_management.html)
    * Forensic Investigations
    * [Vendor Compliance Monitoring](./guidance/TPM.1.04_vendor_compliance_monitoring.html)
* Vendor Agreements
    * Network Access Agreement: Vendors
    * [Vendor Non- disclosure Agreements](./guidance/TPM.2.02_vendor_non-disclosure_agreements.html)
    * Cardholder Data Security Agreement
    * Network Service Level Agreements (SLA)
* Vendor Procurement
    * [Approved Service Provider Listing](./guidance/TPM.3.01_approved_service_provider_listing.html)

## Training and Awareness
* General Awareness Training
    * [General Security Awareness Training](./guidance/TRN.1.01_general_security_awareness_training.html)
    * [Code of Conduct Training](./guidance/TRN.1.02_code_of_conduct_training.html)
* Role-Based Training
    * Developer Security Training
    * Payment Card Processing Security Awareness Training

## Vulnerability Management
* Production Scanning
    * [Vulnerability Scans](./guidance/VUL.1.01_vulnerability_scans.html)
    * Vulnerability Assessment: Cardholder Data Environment
    * Approved Scanning Vendor
* Penetration Testing
    * [Application Penetration Testing](./guidance/VUL.2.01_application_penetration_testing.html)
    * Penetration Testing: Cardholder Data Environment
* Patch Management
    * [Infrastructure Patch Management](./guidance/VUL.3.01_infrastructure_patch_management.html)
* Malware Protection
    * [Enterprise Antivirus](./guidance/VUL.4.01_enterprise_antivirus.html)
    * Enterprise Antivirus Tampering
* Code Security
    * [Code Security Check](./guidance/VUL.5.01_code_security_check.html)
    * Code Security Check: Cardholder Data Environment
* External Advisories and Inquiries
    * [External Information Security inquiries](./guidance/VUL.6.01_external_information_security_inquiries.html)
    * External Alerts and Advisories
* Program Management
    * [Vulnerability Remediation](./guidance/VUL.7.01_vulnerability_remediation.html)
