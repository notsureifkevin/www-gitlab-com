---
layout: markdown_page
title: "BC.1.04 - Business Impact Analysis Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# BC.1.04 - Business Impact Analysis

## Control Statement
GitLab identifies the business impact of relevant threats to assets, infrastructure, and resources that support critical business functions. Recovery objectives are established for critical business functions.

## Context
* Business impact analysis (BIA) is a component of both business continuity planning and risk management.
* In the context of business continuity, BIAs help establish a priority for teams and services.
* Additionally, BIAs help document the risks associated with those teams and functions.
* BIAs help document the role of a team or service within the organization. They are not meant to be comprehensive or perfectly reflect the impact that the team or service has on GitLab. They are simply a way to illustrate the threats and the related impact to the business operations.

## Scope
This control is a subset of the Business Continuity control. Business Impact Analysis should exist for all services and teams that have a business continuity plan. 

## Ownership
* Business Operations owns this control.
* Infrastructure will provide implementation support for .com.

## Guidance
* Meet with senior management
* Identify the scope of the Business Impact Analysis (BIA), and the subject matter experts who will be involved.
* Determine the operating parameters of the BIA.
* Gather all required data before conducting the BIA interviews (pre-work).
* Schedule & Conduct the BIA interviews.
* Aggregate the data and analyze it.
* Send participants the completed BIA.
* Create and send the report to senior management.
* Work on the agreed upon recovery strategies.

## Additional control information and project tracking
Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Business Impact Analysis control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/777).

## Framework Mapping
* ISO
  * A.17.1.1
  * A.17.1.2
* SOC2 CC
  * CC7.5
