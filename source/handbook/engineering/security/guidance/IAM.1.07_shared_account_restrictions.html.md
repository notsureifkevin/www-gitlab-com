---
layout: markdown_page
title: "IAM.1.07 - Shared Account Restrictions Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IAM.1.07 - Shared Account Restrictions

## Control Statement

Where applicable, the use of generic and shared accounts to administer systems or perform critical functions is prohibited; generic user IDs are disabled or removed.

## Context

Use of shared or generic accounts limits the ability to ensure authenticity and integrity.  Someone outside the organization could exploit this and their actions could not be easily traced.

## Scope

TBD

## Ownership

* Control Owner: `Security`
* Process owner(s): 
    * Security: `50%`
    * Business Ops: `25%`
    * Security Compliance: `25%`

## Guidance

Review and document required accounts for a given system and disable all unnecessary accounts. Use of shared accounts should not used.  If unavoidable, compensating controls should be utilized to add accountability.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Shared Account Restrictions control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/811).

## Framework Mapping

* PCI
  * 8.5
