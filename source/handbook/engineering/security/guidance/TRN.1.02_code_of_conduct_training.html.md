---
layout: markdown_page
title: "TRN.1.02 - Code of Conduct Training Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# TRN.1.02 - Code of Conduct Training

## Control Statement

All GitLab team-members complete a code of business conduct training.

## Context

The aim of this control is help ensure that all GitLab team-members are aligned on the values of the organization. The purpose of this alignment is to demonstrate to any external auditors that we hold all GitLab team-members to this same standard of conduct.

## Scope

This control applies to all GitLab team-members and contractors.

## Ownership

Control owner: 
* People Operations team

Process owner: 
* People Operations team

## Guidance

Pending additional input, we might be able to use the onboarding issues, which require new Gitlabbers to sign off on having read the GitLab handbook as evidence of this code of conduct training since there is plenty of information about our values and how Gitlabbers are expected to behave.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Code of Conduct Training control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/932).

## Framework Mapping

* ISO
  * A.11.2.8
  * A.7.1.2
  * A.7.2.1
  * A.8.1.3
* SOC2 CC
  * CC1.1
  * CC1.4
  * CC1.5
* PCI
  * 12.3
  * 12.3.5
