---
layout: markdown_page
title: "IAM.6.01 - Key Repository Access Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IAM.6.01 - Key Repository Access

## Control Statement

Access to the cryptographic keystores is limited to authorized personnel.

## Context

One of the fundamental and most important security considerations of encryption is protecting cryptographic keys, particularly private keys. This controls aims to protect the confidentiality and integrity of data for customers, GitLab team-members, and partners by limiting the number of people with access to view, modify, and create new cryptographic keys. A malicious actor with access to GitLab's cryptographic keys could decrypt all sensitive data stored and transmitted by GitLab, rending the encryption useless.

## Scope

This control applies to any and all cryptographic keystores.

## Ownership

TBD

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Key Repository Access control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/832).

## Framework Mapping

* ISO
  * A.10.1.2
  * A.18.1.5
* SOC2 CC
  * CC6.1
  * CC6.3
* PCI
  * 3.5
  * 3.5.2
  * 3.6
  * 3.6.2
  * 3.6.3
  * 3.6.7
