---
layout: markdown_page
title: "CFG.1.07 - Default Device Passwords Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# CFG.1.07 - Default Device Passwords

## Control Statement

Vendor-supplied default passwords are changed according to GitLab standards prior to device installation on the GitLab network or immediately after software or operating system installation.

## Context

Changing the default password will strengthen the baseline configuration and reduce the ability for the system/device to become compromised.

## Scope

This control applies to all hosted systems (e.g. VM's and GCP compute services) as well as end user workstations (e.g. GitLab team-members' MacBooks) and all third-party applications utilized by GitLab.

## Ownership

TBD

## Guidance

Tip - add task to runbook(s) to implement password change and/or validate default password has been changed upon review. This may be achievable by a Chef cookbook entry.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Default Device Passwords control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/790).

## Framework Mapping

* PCI
  * 2.1
  * 2.1.1
