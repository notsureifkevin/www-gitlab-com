---
layout: markdown_page
title: "BU.1.01 - Backup Configuration Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# BU.1.01 - Backup Configuration

## Control Statement

GitLab configures redundant systems and performs data backups routinely as specificied by management to resume system operations in the event of a system failure.

## Context

We need to demonstrate that GitLab can be restored to a prior point in time in the event the data is corrupted, service interruption or other event that disrupts normal service. This helps ensure we have redundancy/backups enabled.

## Scope

Regular Backups help ensure secure and clean data to keep the business running in case of data loss, or a natural disaster. Scope is to make sure, at Gitlab there is a regual backup process in place which is efficeint and operational. 

* At GitLab, two components of infrastructure are backed up regularly: git repository data and the database. 
* The cadence of backup cycle at GitLab: Disk snapshots are taken every 24 hours. In addition to snapshots, the database is continually streaming transaction data to a fleet of standby replicas.
* All data is stored on the Google Cloud Platform on disk snapshots and in storage buckets and remains available indefinitely.
* Log of snapshots/backups are kept up to 180 days. There is a lot of detailed information within the logs.

## Ownership

* GitLab Infra team owns the backup configuration to 100%. They are responsible to run both app snapshots and database backups.  
* The ultimate responsibility to ensure backups take place per cadence, falls on Senior management

## Guidance

* Assess and apply a clear backup and restore standard for all GitLab systems
* Prioritize systems according to data sensitivity
* Define the backup and recovery standards per data prioritization
* Prevent loss of data in case of an accidental deletion or corruption of data, system failure, or disaster
* Permit timely restoration of information and business processes, should such events occur.
* Manage and secure backup and restoration processes and the media employed in the process
* Set retention periods of data, contained within system level backups designed for recoverability
* Provide a point-in-time snapshot of information as it was, during the time period defined by GitLab backup policies
* Backup retention periods and the data retention periods are defined by legal and/or business requirements.

Backup configuration Documentation should include:

* The configuration of redundant systems showing how a failover occurs
* The configuration of backup settings of each system
   * If centrally managed, the runbook that controls backup configurations.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Backup Configuration control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/778).

## Framework Mapping

* ISO
  * A.18.1.3
* SOC
  * A1.2
* PCI
  * 12.10.1
