---
layout: markdown_page
title: "SYS.3.02 - System Availability Monitoring Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SYS.3.02 - System Availability Monitoring

## Control Statement

Critical systems are monitored in accordance to predefined availability criteria and alerts are sent to authorized personnel.

## Context

This control is related to GitLab control # SYS.3.01 (Availability Monitoring Alert Criteria). The purpose of this control is to ensure that there is monitoring and alerting based on that availability criteria. This control is meant to create actionable information from the uptime/availability thresholds we have established for ourselves. The idea is to clearly state what our availability requirements are and then hold ourselves accountable to those requirements.

## Scope

This control applies to all GitLab production systems.

## Ownership

TBD

## Guidance

Tools like Splunk are perfect for automating these types of controls.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [System Availability Monitoring control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/921).

## Framework Mapping

* ISO
  * A.12.1.3
  * A.17.2.1
* SOC2 CC
  * CC7.2
* SOC2 Availability
  * A1.1
