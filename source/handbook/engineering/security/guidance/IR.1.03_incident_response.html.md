---
layout: markdown_page
title: "IR.1.03 - Incident response Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IR.1.03 - Incident response

## Control Statement

Confirmed incidents are assigned a priority level and managed to resolution. If applicable, GitLab coordinates the incident response with business contingency activities.

## Context

It's important for every issue to be assigned an appropriate severity so that time, effort, and resources can be most effectively allocated. And by having a mechanism to track whether every incident is seen to resolution, every incident is eventually resolved.

## Scope

This control applies to all systems within our production environment. The production environment includes all endpoints and cloud assets used in hosting GitLab.com and its subdomains. This may include third-party systems that support the business of GitLab.com.

## Ownership

Control owner: 
* Security Operations team

Process owner: 
* Security Operations team

## Guidance

Security incidents should have a defined process and support the ability to be tracked and managed for resolution.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Incident response control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/841).

## Framework Mapping

* ISO
  * A.16.1.1
  * A.16.1.2
  * A.16.1.4
  * A.16.1.5
  * A.16.1.6
  * A.16.1.7
* SOC2 CC
  * CC4.2
  * CC5.1
  * CC5.2
  * CC7.4
  * CC7.5
* PCI
  * 10.6.3
  * 10.8.1
  * 12.10.3
