---
layout: markdown_page
title: "SYS.3.01 - Availability Monitoring Alert Criteria Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SYS.3.01 - Availability Monitoring Alert Criteria

## Control Statement

GitLab defines availability monitoring alert criteria, how alert criteria will be flagged, and identifies authorized personnel for flagged system alerts.

## Context

In order for alerts to be configured for availability, we first have to establish what criteria we use for altering. This control simply formalizes the need for GitLab to explicitly define what criteria we use for availability alerting.

## Scope

This control applies to all production systems related to the GitLab SaaS product.

## Ownership

TBD

## Guidance

This control is not meant to dictate what criteria we use for our availability monitoring; this control only requires us to formally define what our alerting criteria is.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Availability Monitoring Alert Criteria control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/920).

## Framework Mapping

* ISO
  * A.12.1.3
  * A.17.2.1
* SOC2 CC
  * CC7.2
* SOC2 Availability
  * A1.1
