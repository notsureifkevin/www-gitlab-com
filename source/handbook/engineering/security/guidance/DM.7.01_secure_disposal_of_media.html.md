---
layout: markdown_page
title: "DM.7.01 - Secure Disposal of Media Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# DM.7.01 - Secure Disposal of Media

## Control Statement

GitLab securely erases media containing decommissioned red and orange data and obtains a certificate or log of erasure; media pending erasure are stored within a secured facility.

## Context

Securely disposing of both electronic and physical media adds a layer of protection from the data being disposed being recovered by unauthorized persons. There are several effective, publicly available tools and techniques to recover data from electronic and physical media, including hard drives and shredded paper. This control aims to reduce the risk of data being recovered by unauthorized persons and shows customers, GitLab team-members, and partners we take measures to protect their data even after it's done being used.

## Scope

This control applies to both electronic and physical (for example, paper printouts) media.

## Ownership

* Control Owner: `IT Ops`
* Process owner(s):
    * IT Ops: `100%`

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Secure Disposal of Media control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/802).

## Framework Mapping

* ISO
  * A.11.2.7
  * A.8.3.2
* SOC2 CC
  * CC6.5
* PCI
  * 9.8
  * 9.8.1
  * 9.8.2
