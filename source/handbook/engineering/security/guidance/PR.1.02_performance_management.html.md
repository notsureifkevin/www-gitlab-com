---
layout: markdown_page
title: "PR.1.02 - Performance Management Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# PR.1.02 - Performance Management

## Control Statement

GitLab has established a check-in performance management process for on-going dialogue between managers and their reports. quarterly reminders are sent to managers to perform their regular check-in conversation.

## Context

The purpose of this control is to ensure managers and their direct reports are in ongoing, open conversation with one another to stay current with projects, tasks, roadblocks, and so on. This benefits both parties - particularly with GitLab being all-remote and asynchronous - by facilitating regular feedback, timely issue escalation, decision making, and work prioritization.

## Scope

This control applies to GitLab management and leadership.

## Ownership

Control Owner: 
* People Operations team

Process Owner: 
* People Operations team

## Guidance

A process to evaluate the performance of team-members.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Performance Management control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/861).

## Framework Mapping

* SOC2 CC
  * CC1.3
