---
layout: markdown_page
title: "RM.1.02 - Continuous Monitoring Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# RM.1.02 - Continuous Monitoring

## Control Statement

The design and operating effectiveness of internal controls are continuously evaluated against the established controls framework by GitLab. Corrective actions related to identified deficiencies are tracked to resolution.

## Context

GitLab's controls aim to protect the confidentiality, integrity, and availability of customer, GitLab team-member, and partner data and the service provided to them. To ensure the controls remain current and relevant, and they're both being used in the way they were intended and have the expected impact, they should be regularly evaluated and improved when necessary.

## Scope

This control applies to all controls in the GitLab Control Framework (GCF).

## Ownership

* Control Owner: `Security Compliance`
* Process owner(s):
    * Security Compliance: `100%` 

## Guidance

The design and operating effectiveness of GCF controls can be evaluated as part of a gap analysis. A gap analysis can identify areas where the design of a control is insufficient to satisfy the control objectives and any findings can be preliminarily validated through control testing. [Further control testing](https://about.gitlab.com/handbook/engineering/security/guidance/RM.2.01_internal_audits.html) may then be performed by the Internal Audit team.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Continuous Monitoring control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/867).

Examples of evidence an auditor might request to satisfy this control:

* A documented process to test the design and operating effectiveness of internal controls
* An established cadence for such testing and the ability to show the cadence is followed
* A document, report, or other such documentation showing the testing of the design and operating effectiveness of internal controls

## Framework Mapping

* ISO
  * A.12.7.1
  * A.18.2.2
  * A.18.2.3
* SOC2 CC
  * CC1.2
  * CC3.2
  * CC3.4
  * CC4.1
  * CC4.2
  * CC5.1
  * CC5.2
