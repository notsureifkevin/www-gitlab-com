---
layout: markdown_page
title: "Preferred Companies"
---

## On this page
{:.no_toc}

- TOC
{:toc}

If you work, or have worked, at one of the following companies we encourage you to apply. Of course, it is not necessary to have this experience to work at GitLab, so we encourage everyone else to apply as well.

We try to avoid mentioning [partners](https://about.gitlab.com/partners/) on this page because it might hurt the relationship.

## DevOps Stages

GitLab is a single application for the complete DevOps Lifecycle. As such, we're highly interested in hiring individuals with experience from companies who create products related to specific parts of this lifecycle.

### Common

* AirBnB
* Alcide
* Amazon
* Atlassian
* Capital One
* Chef
* Datera
* Dell
* DigitalOcean
* Docker
* f5
* Facebook
* Google
* Heroku
* Joyent
* Lyft
* Mesosphere
* Microsoft
* Netflix
* New Relic
* NPM
* Oracle
* Palo Alto Networks
* Pivotal
* Puppet
* Qualcomm
* Rackspace
* RedHat
* Shopify
* Sonarcube
* Stripe
* Sumologic
* Twilio
* Twitch
* Uber
* VMware
* Workday
* WP Engine

### Manage

* GitPrime

### Plan

* Aha
* Trello
* Upland

### Create

* Eclipse
* GitHub

### Verify

* Cloudbees
* CircleCI
* Travis CI

### Release

* Codeship
* Netlify
* Zeit

### Package

* codefresh
* goHarbour
* JFrog
* NPM
* Quay
* Sonatype

### Configure

* Serverless
* Cloudreach

### Monitor

* Datadog
* Dynatrace
* Elastic
* Influxdata
* Lightstep
* Netsil
* New Relic
* OpsGenie
* SignalFx
* Splunk/VictorOps

### Secure

* Anchore
* Appscan
* Blackduck
* Checkmarx
* Coverity
* Kata Containers
* Micro Focus
* Microsoft
* Snyk
* Sonarqube
* Synopsys
* Twistlock
* Veracode
* Whitesource


### Defend

* Avira
* Carbon Black
* Crowdstrike
* Cylance
* F5
* FireEye
* McAfee
* Microsoft
* Palo Alto Networks
* Sophos
* Symantec


### Enablement

* Look under [common](#common)

### Growth

Companies with a strong reputation for running effective Growth teams and experiments.

* AirBnb
* Facebook
* HubSpot
* Netflix
* Slack
* Spotify
* Twitter
* Uber

## Company Experience Based on Functional Area

Some companies demonstrate excellence in specific function. So we value experience from there regardless of how similar their product is to ours.

### Engineering Division

#### Development Department

Companies with strong reputation in product development reputation and/or SaaS solutions.

* Look under [DevOps Stages](#devops-stages)
* Adobe
* Salesforce

#### Infrastructure Department

* Facebook
* Google
* Amazon
* Twitter
* Spotify

#### Quality Department

Companies with a strong reputation in test engineering, and engineering productivity.

* Walmart Labs
* BlackBoard
* Expedia
* Houzz
* Credit Karma
* GitHub
* Circle CI
* Victor Ops
* SauceLabs
* Splunk
* Puppet
* Slack
* SalesForce
* Rackspace
* Workday
* Amazon
* Google
* Facebook
* Microsoft
* NetFlix
* VMware
* ThoughtWorks

#### Security Department

We strive to hire globally at GitLab, and the Security Department is very much focused on hiring globally. Our Security top-level objectives span an extremely broad expanse of domain knowledge. We've found that hiring from companies that have established security teams works best for bringing on the best people. The companies most likely to have established security teams are Global 2000 companies. 

* [Global 2000 Companies](https://www.forbes.com/global2000/)

#### Support Department

* Rackspace
* AWS
* GoodData
* Circle CI
* New Relic
* Zendesk
* Digital Ocean
* DreamHost
* NexGen Technologies
* Optiva
* WP Engine

#### UX Department

* AppDynamics
* Atlassian
* AWS
* Chartbeat
* Chartio
* DigitalOcean
* Fivetran
* HubSpot
* IBM
* Looker
* Palintir
* Sumo Logic
* Segment
* Sentry.io
* Stack Overflow
* Wavefront by VMware
* WeWork
