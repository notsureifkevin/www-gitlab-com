---
layout: markdown_page
title: "Merge Request Coach Lifecycle"
---

## Applying to become a Merge Request Coach

1. Create an MR to add "Merge Request coach" to your team page entry.
1. Explain your motivation in the MR body:
   * Why you want to become a Merge Request Coach?
   * How much time you are planning to spend on it?
   * Which duties you are focusing on (e.g. triage, finish stale MRs)?
1. Mention `@gitlab-org/coaches` and assign one of them.

## Stepping down gracefully

If you are no longer able to serve as a Merge Request Coach, you should identify another GitLab team member to take your place so that the capacity of the remaining coaches remains the same. When you are ready to step down, you need to: 

1.  Make an announcement on the `#mr-coaching` Slack channel. 
1.  Update the `team.yml` file to remove "Merge Request Coach". 
1.  Remove yourself from the `@gitlab-org/coaches` group.  

## References/resources

1.  [Merge Request Coach responsibilities](https://about.gitlab.com/job-families/expert/merge-request-coach/)
1.  [Gitter room for contributors](https://gitter.im/gitlabhq/contributors): please join if you're not part of it already!
