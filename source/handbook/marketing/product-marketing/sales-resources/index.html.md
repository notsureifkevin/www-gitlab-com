---
layout: markdown_page
title: "Most commonly used sales resources"
---

## On this page
{:.no_toc}

- TOC
{:toc}

The purpose of this page is to be a *quick reference* to help the sales team navigate and find resources to help them be more efficient and effective.  This is NOT every possible resource, but a curated list of what we believe is **most valuable**. If you find a broken link - either **update it** or **contact the PMM team**.  Ultimately, these resources should ALL be available on our main [resources page](/resources/). This is a work in progress resource. **Sales Team**: if you have comments, suggestions, or feedback about this page, this [issue- #366](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/366) is open for ongoing feedback.

## Force Management: Command of the Message
Below are the key GitLab-tailored artifacts that align to and support the Force Management Command of the Message methodology.

* **KEY ASSET**: [**GitLab Value Framework**](https://drive.google.com/open?id=1GV1WGyJIRuor0jxG-9ABu9ZSIBUFtPq1pqAxV9yJOvQ) 
    - Tip: Use Google's "Document Outline" feature to easily navigate to various sections within the document
* [GitLab Value Framework Summary (1-page quick reference guide)](https://docs.google.com/document/d/1BawkSEbejPKx2EVgOqxtrhzHlCTGEFNzvoIF4hW3bT4/edit?usp=sharing)
* [GitLab CXO Digital Transformation Discovery Guide](https://drive.google.com/open?id=1R6is7t4Ph3-p4tGJbDq0RhezT4j-3P0rluMV3H9-Rho)
* [Additional Job Aids](https://drive.google.com/drive/u/0/folders/1RMAiyvPEHu7b3Tsmlf1wqxzONu1DjE4y): 
   - [GitLab CoM & MEDDPICC Training Slide Deck](https://drive.google.com/file/d/1aHPGHUW4oZHwPU9_Qo4qTy1pkBNBZCZU/view)
   - [Pre-Call Plan](https://docs.google.com/document/d/15i-Vq1W6amWS8g4rSGXcLYT1f3lFaBtWeWR5TVNaskg/copy)
   - [Customer Call Notes Template](https://docs.google.com/document/d/1wGSgkm2gOS-gue-1eZnyIGlR0e_MvGpH0lmUtEjMluo/copy) 
   - [Opportunity Qualifier](https://docs.google.com/document/d/1ntAIY9Dr7HisosoGjNGRH2-_kNCl0rZqH-iegj4ahXA/copy)
   - [Opportunity Coaching Guide](https://docs.google.com/document/d/1JoDraPlny2NKqqox9Z5xEMjb9ePLIJ_Gc32zXT9-Ktk/copy)
   - [MEDDPICC template](https://docs.google.com/document/d/14snDK0Gd328_2H24ZBFXUHsTJT2olwideFZF0xl38Ik/copy)
   - [GitLab CoM & MEDDPICC Participant Guide](https://drive.google.com/file/d/1rFTcWNxUKLQBTGyZKhs8pCMLzml5hAfn/view)
   - [GitLab CoM & MEDDPICC Fast Start Program Manager Playbook](https://drive.google.com/file/d/1VmwkucH_JsO204Amh7SwxvZgDbfs6IG8/view)
   - [GitLab Manager Coaching Slide Deck](https://drive.google.com/file/d/10RAEZ2dXQ15twAduYseUNuzivfOv4gr8/view)

GitLab sales professionals and Sales Development Reps (SDRs) may access additional information in the [Force Management Command Center](https://gitlab.lyearn.com) (password protected since resources contain Force Management intellectual property). In particular, the [Channels](https://gitlab.lyearn.com/#/learner/channels) section of the Force Management Command Center contains supplemental instructional videos, podcasts, and blogs).

## Pricing and tiers

* [Why Ultimate Page](/pricing/ultimate/), [Why Ultiamte Deck](https://docs.google.com/presentation/d/1TP5cXH5Nr0VkH7mE6M_-DFXT_Jnq7o5LPxuMUz2paI4/edit)
* [Why Premium](/pricing/premium/)
* [Self-managed pricing](/pricing/#self-managed), [Self-managed feature comparison](/pricing/self-managed/feature-comparison/)
* [GitLab.com pricing](/pricing/#gitlab-com), [GitLab.com feature comparison](/pricing/gitlab-com/feature-comparison/)
* [License FAQ](/pricing/licensing-faq/)
* [Pricing handbook](/handbook/ceo/pricing/)
* [Past releases sorted by tier](/releases/), [Upcoming releases sorted by stage](/upcoming-releases/), [upcoming features sorted by tier](/direction/#paid-tiers) 
* [Product maturity](/direction/maturity/)

## GitLab Use Cases

Here is a list of GitLab use cases. Visit the [Use case handbook page](/handbook/use-cases/) for the full list.

* Source Code Management (SCM)
* Continuous Integration (CI) - [WIP buyer's journey collatoral map](https://docs.google.com/spreadsheets/d/1-VmrzB7T1b-UXBoP54j1pYMbYE78TGeKflFom3KNfU8/edit#gid=711048571)
* CI/CD
* DevSecOps
* Project Management
* Simply DevOps
* Cloud Native Developement 
* GitOps

## Presentations and Messaging 

* [Elevator Pitch and Messaging](/handbook/marketing/product-marketing/messaging/) - Overall GitLab value proposition and messaging
* [Pitch Deck:](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/edit?usp=sharing) - The primary GitLab overview presentation.
* [Product Vision, Strategy, and FY20 Plan](https://docs.google.com/presentation/d/19o720CqP9S-xRQoT9y8FF7DgtRxuJhQedaaQQQygYx8/edit#slide=id.g57cef2563b_0_0)
* [Professional Services Deck](https://docs.google.com/presentation/d/1CFR8_ZyE9r4Dk_mjoWGe4ZkhtBimSdN0pylIPu-NAeU/edit?usp=sharing)
* [Security Overview](https://docs.google.com/presentation/d/1WHTyUDOMuSVK9uK7hhSIQ_JbeUbo7k5AW3D6WwBReOg/edit?usp=sharing)
* [Agile Overview Slides](https://docs.google.com/presentation/d/1MmPPao_382wBYYbQ_DLiBGuxsYNRzodiZ0MydXeeYU8/edit#slide=id.g5d6196cc9d_2_113)
* [Accelerating Delivery Short Talk](https://drive.google.com/open?id=15PgY9Dm0emVlWZdBjJX15nVxDR47l1MwhOF-tGbzY5Y)
* [Accelerating Deliver + Digital Transformation](https://drive.google.com/open?id=1_fCQW8-K_QaUAtTMfvrTSmjRRYRdpaZp78ljndEsk0c) and [recording](https://about.gitlab.com/handbook/sales/training/#gitlab-sales-learning-framework)
* [Kubernetes 101](https://docs.google.com/presentation/d/1fWjiVgSNMKTHyC6_nWDY5rDhvdm-zEQMQytZysIXAzk/edit)
* [Digital & Cloud Native Transformation (GitLab Benefits) Deck](https://docs.google.com/presentation/d/1mcg5E4dztQZQTN4WvJ4sdqnRGZ6Wz7IRDa8_o8-p4vg/edit#slide=id.g51da45aefc_0_5)
* [Cloud Native Transformation (vendor agnostic speaker deck)](https://docs.google.com/presentation/d/15FN3tebw8LZMYkOMtqaPmHV17NDVZy0K6Tou1-TS-Ac/edit#)
* [Multi-cloud Serverless](https://docs.google.com/presentation/d/16TgspGl7jxVRMStfNHytZsDFQK1Mr5QWn5UP06N3JnA/edit)
* [Why Ultimate](https://docs.google.com/presentation/d/1TP5cXH5Nr0VkH7mE6M_-DFXT_Jnq7o5LPxuMUz2paI4/edit)

## Help with RFPs
* [RFP Archive](https://drive.google.com/drive/u/0/folders/0B0JubjRNovkvNVJTVmNJdXJNZEk)

## GitLab by the Stages

Key resources by stage.

### Manage

* [Geo and DR](/solutions/geo/)
* [High Availability](/solutions/high-availability/)
* [Value Stream Management](/solutions/value-stream-management/)

### Plan

* [Agile Delivery Overview](/solutions/agile-delivery/)
* [Agile Teams Video](https://youtu.be/VR2r1TJCDew)
* [Gitlab and Scaled Agile Framework Video](https://www.youtube.com/watch?v=PmFFlTH2DQk&feature=youtu.be)
* [Agile Overview Slides](https://docs.google.com/presentation/d/1MmPPao_382wBYYbQ_DLiBGuxsYNRzodiZ0MydXeeYU8/edit#slide=id.g5d6196cc9d_2_113) 

### Create

* [SCM Overview](/product/source-code-management/)

### Secure
* [Dev Sec Ops Overview](/solutions/dev-sec-ops/)
* [Security Demo Video](/resources/video-gitlab-security-demo/)
* [Whitepaper GitLab App Sec Workflow](/resources/downloads/gitlab-seismic-shift-in-application-security-whitepaper.pdf)

### Verify, Package, Release, Configure

Customers aren't going to think about or ask for different "stages". They are going to say things like
- We want to adopt DevOps
- We want to adopt CI/CD
- We want to adopt Kubernetes (same thing as saying "We want to go cloud native")

#### CI/CD Resources

* [CI/CD overview](/product/continuous-integration/)
* [Scalable app deployment with Google Cloud and Gitlab video](https://www.youtube.com/watch?v=uWC2QKv15mk) (Great Kuberentes 101 content for customers that are unfamilar. Also Demos Auto DevOps.)
* [Automating Kubernetes Deployments video](https://www.youtube.com/watch?v=wEDRfAz6_Uw) (For folks familiar with Kubernetes or Cloud Native. This is a webinar hosted by the CNCF. Goes into both business value and more technical detail on GitLab CI/CD)
* [GitLab + Kubernetes](/solutions/kubernetes/)
* [What is Cloud Native?](/cloud-native/)
* [What are microservices?](/topics/microservices/), [Business value of microservices](/topics/microservices/#business-value-of-microservices)
* [What is serverless?](/topics/serverless/), [Business value of serverless](/topics/serverless/#business-value-of-serverless)

### Monitor

* tbd

### Defend

* tbd

## [Analyst Reports](/analysts/)

- [Overview: Forrester Continuous Integration Tools](/analysts/forrester-ci/) and [Report: GitLab and The Forrester Wave™: Continuous Integration Tools, Q3 2017](/resources/forrester-wave-ci-2017/)
- [Overview: Forrester Value Stream Management](/analysts/forrester-vsm/) and [Report: GitLab and The Forrester New Wave™: Value Stream Management Tools, Q3 2018](/resources/forrester-new-wave-vsm-2018/)
- [Overview: Gartner Application Release Orchestration](/analysts/gartner-aro/) and [Report: GitLab and the Gartner Magic Quadrant for Application Release Orchestration 2018](/resources/forrester-wave-ci-2017/)
- [Overview: IDC Innovators Agile Code Development Technologies, 2018](/analysts/idc-innovators/) and [Report: IDC Innovators Agile Code Development Technologies, 2018](https://page.gitlab.com/rs/194-VVC-221/images/IDC-Innovators-Agile-Code-Development-Technologies-2018.pdf)

- [Analyst Program Overvew Page](/handbook/marketing/product-marketing/analyst-relations/)


## White Papers

Key Whitepapers:

- [A seismic shift in application security](/resources/downloads/gitlab-seismic-shift-in-application-security-whitepaper.pdf)
- [Bridging the divide between developers and management ](/resources/downloads/201806_WP_Bridging_developers_and_management.pdf)
- [What is Concurrent DevOps](/resources/downloads/gitlab-concurrent-devops-whitepaper.pdf)
- [*Scaled CI and CD*](https://page.gitlab.com/rs/194-VVC-221/images/gitlab-scaled-ci-cd-whitepaper.pdf)
- [*Moving to Git: a guide*](https://page.gitlab.com/rs/194-VVC-221/images/gitlab-moving-to-git-whitepaper.pdf)
- [How GiLab is Enterprise Class](/solutions/enterprise-class/)
- [2018 Developer Report](/developer-survey/2018/)
- [Reduce cycle time whitepaper](/resources/downloads/201906-whitepaper-reduce-cycle-time.pdf)
- [Speed to mission whitepaper](/resources/downloads/201906-whitepaper-speed-to-mission.pdf)
- [Forrester-Manage your toolchain before it manages you](https://about.gitlab.com/resources/whitepaper-forrester-manage-your-toolchain/)


## Datasheets

- [GitLab Capabilities](/images/press/gitlab-capabilities-statement.pdf)
- [GitLab Data Sheet](/images/press/gitlab-data-sheet.pdf)
- [GitLab on AWS Concurrent DevOps Solution Brief](/resources/downloads/GitLab_AWS_Solution_Brief.pdf)

## [Customer References](/customers/)

- [Customer proof points (GitLab Internal-only)](https://docs.google.com/document/d/1gwLqlJKiiDxqsIaJ5H_NLsRgDD32cKO1-4wS9ivBijs/edit)

- [Goldman Sachs improves from two daily builds to over a thousand per day](/customers/goldman-sachs/)
- [How Wag! cut their release process from 40 minutes to just 6 minutes](https://about.gitlab.com/2019/01/16/wag-labs-blog-post/)
- [Axway realizes a 26x faster release cycle by switching from Subversion to GitLab](/customers/axway/)
- [Paessler AG switches from Jenkins and ramps up to 4x more releases](/customers/paessler/)
- [Trek10 provides radical visibility to clients](/customers/trek10/)
- [Particle physics laboratory uses GitLab to connect researchers from across the globe](/customers/cern/)
- [iFarm plants the seeds for operational efficiency](/customers/ifarm/)
- [Connecting the cosmos with Earth - How the European Space Agency uses GitLab to focus on space missions](/customers/european-space-agency/)
- [How GitLab CI supported Ticketmaster’s ramp up to weekly mobile releases](https://about.gitlab.com/2017/06/07/continous-integration-ticketmaster/)
- [The Cloud Native Computing Foundation eliminates complexity with a unified CI/CD system](/customers/cncf/)
- [Worldline hosts 14,500 projects and has 3,000 active users on their GitLab platform](/customers/worldline/)
- [Equinix increases the agility of their DevOps teams with self-serviceability and automation](/customers/equinix/)
- [The Paul G. Allen Center for Computer Science & Engineering gains control and flexibility to easily manage 10,000+ projects](/customers/uw/)
- **[ALL Customer Case Studies](/customers/)**

- [Overall Customer Reference Program](/handbook/marketing/product-marketing/customer-reference-program/)


## [Comparisons](/devops-tools/)

Here are links to a few **KEY comparisons** and then a link to main comparison page where over 50 comparisons are available.
- [GitHub](/devops-tools/github-vs-gitlab.html)
- [BitBucket](/devops-tools/bitbucket-vs-gitlab.html)
- [AWS Codestar](/devops-tools/codestar-vs-gitlab.html)
- [Jenkins](/devops-tools/jenkins-vs-gitlab.html)
- [Azure](/devops-tools/azure-devops-vs-gitlab.html)
- [Jira](/devops-tools/jira-vs-gitlab.html)
- [ALL Comparisons](/devops-tools/)

## Demos
- [Demo Videos](/handbook/marketing/product-marketing/demo/#videos)
- [Demo Click Throughs](/handbook/marketing/product-marketing/demo/#click-throughs)
- [Demo Instructions](/handbook/marketing/product-marketing/demo/#live-instructions)


## Getting Started with GitLab  (Services)
- [Professional Services](/services/)
- [Quick Start Implementation Package](/services/implementation/quickstart)
- [Dedicated Implementation Planning](/services/implementation/enterprise)
- [GitLab Training](/services/education)

## ROI

The ROI calculator is a work in progress - welcome contributions.
- [Overall ROI page](/roi/)  
- [GitLab replacing other tools](/roi/replace/)

## Social Selling Basics
- [Social Selling Basics presentation](https://docs.google.com/presentation/d/1UCRF6PC6al8XxT8E_4rDKkQjkW6WGPA6gybWeuRIg7A/edit?usp=sharing)  
- [Social Selling Basics video](https://youtu.be/w-C4jts-zUw) (20 minutes)
   - [Social Selling_Sales Enablement_2019-07-11](https://www.youtube.com/watch?v=Ir7od3stk70) (28 minutes)
- [LinkedIn Sales Navigator resources](https://docs.google.com/document/d/1UF69ieck4AdHadzgPmZ5X1GBs3085JhlYaMowLj0AOg/edit?usp=sharing)

## Enablement

- [GitLab Sales Training](https://about.gitlab.com/handbook/sales/training/) 
- [Sales Enablement Sessions](https://about.gitlab.com/handbook/sales/training/sales-enablement-sessions/)
- [GitLab YouTube Sales Enablement playlist](https://www.youtube.com/playlist?list=PLFGfElNsQthYe-_LZdge1SVc1XEM1bQfG)
- [GitLab Unfiltered YouTube Sales Enablement playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrirMKe3CyWl4ZBCKna5rJX)
