---
layout: markdown_page
title: "Marketing Career Development"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Position Levels

| Individual Contributor Track | SDR IC Track     | Management Track |
|------------------------------|------------------|------------------|
| Coordinator                  | SDR Admin        |                  |
| Associate                    | SDR              |                  |
| Manager                      | Senior SDR       |                  |
| Senior Manager               | Lead SDR         |                  |   
| Staff                        |                  | Manager,         |
| Distinguished                |                  | Director,        |
| GitLab Fellow                |                  | Senior Director, |
|                              |                  | VP               |
|                              |                  | CXO              |


Note: `Manager` denotes a management of a certain specialty, whereas `Manager,` denotes a manager of people.

### Career Path

#### Sales Development Career Path

| Starting Role                        | Promotion                                 | Lateral Options                   |
|--------------------------------------|-------------------------------------------|-----------------------------------|
| BDR - Inbound                        | Senior BDR - Inbound                      | SDR - Outbound                    |
| SDR - Outbound                       | Senior SDR - Outbound                     | BDR - Inbound                     |
| Senior BDR - Inbound                 | Manager, Inbound Sales Development        | Customer Success Associate        |
| Senior BDR - Inbound                 | Manager, Inbound Sales Development        | Marketing Programs Associate      |
| Senior BDR - Inbound                 | Manager, Inbound Sales Development        | Marketing Operations Associate    |
| Senior SDR - Outbound                | Manager, Outbound Sales Development       | Recruiting Associate              |
| Senior SDR - Outbound                | Manager, Outbound Sales Development       | Associate Field Marketing Manager |
| Manager, Inbound Sales Development   | Senior Manager, Sales Development Manager |                                   |
| Outbound Sales Development           | Senior Manager, Sales Development         |                                   |
| Senior Manager, Sales Development    | Director, Sales Development               |                                   |

#### Marketing Career Path

| Starting Role                        | Promotion                                 | Lateral Options                   |
|--------------------------------------|-------------------------------------------|-----------------------------------|
| Marketing Associate                  | Field Marketing Manager                   |                                   |
| Marketing Coordinator	               | Content Marketing Associate               |                                   |            
| Marketing Coordinator	               | Marketing Programs Associate              |                                   |
| Marketing Coordinator	               | Associate Customer Advocate               |                                   |
| Marketing Coordinator	               | Marketing Operations Associate            |                                   |
| Marketing Coordinator	               | Online Marketing Associate                |                                   |
| Marketing Coordinator	               | Associate Product Marketing Manager       |                                   |
| Content Marketing Associate	         | Content Marketing Manager	               | Associate Customer Advocate       |
| Content Marketing Associate	         | Content Marketing Manager	               | Marketing Programs Associate      |
| Content Marketing Manager	           | Manager, Content Marketing	               | Customer Advocate                 |
| Content Marketing Manager	           | Manager, Content Marketing                | Marketing Programs Manager        |
| Manager, Content Marketing	         | Senior Manager, Content Marketing         |                                   |
| Manager, Content Marketing	         | Manager, Corporate Marketing              |                                   |
| Senior Manager, Content Marketing	   | Director, Corporate Marketing             |                                   |
| Associate Field Marketing Manager	   | Field Marketing Manager	                 | Marketing Programs Associate      |
| Field Marketing Manager	             | Manager, Field Marketing                  |                                   |
| Manager, Field Marketing	           | Senior Manager, Field Marketing           |                                   |
| Senior Manager, Field Marketing	     | Director, Field Marketing                 |                                   |
| Marketing Operations Associate	     | Marketing Operations Manager              |                                   |
| Marketing Operations Manager	       | Manager, Marketing Operations             |                                   |
| Marketing Operations Manager	       | Senior Marketing Operations Manager       |                                   |
| Manager, Marketing Operations	       | Senior Manager, Marketing Operations      |                                   |
| Senior Manager, Marketing Operations | Director, Marketing Operations            |                                   |
| Online Marketing Associate	         | Online Marketing Manager                  |                                   |
| Online Marketing Manager	           | Manager, Online Marketing                 |                                   |
| Online Marketing Manager	           | Senior Online Marketing Manager           |                                   |
| Manager, Online Marketing	           | Senior Manager, Online Marketing          |                                   |
| Senior Manager, Online Marketing	   | Director, Online Marketing                |                                   |
| Associate Designer                   | Designer                                  |                                   |
| Designer	                           | Senior Designer                           |                                   |
| Associate Community Advocate         | Community Advocate                        |                                   |
| Community Advocate	                 | Senior Community Advocate                 |                                   |
| Senior Community Advocate	           | Manager, Developer Relations	             |               |
| Manager, Developer Relations	       | Director, Developer Relations             |                                   |
| Associate Technical Evangelist         | Technical Evangelist                        |                                   |
| Technical Evangelist	                 | Senior Technical Evangelist                 |                                   |
| Senior Technical Evangelist	           | Manager, Technical Evangelism	             | Senior Technical Evangelism Program Manager            |
| Senior Technical Evangelism Program Manager	               | Manager, Technical Evangelism              |                                   |
| Manager, Technical Evangelism	       | Director, Technical Evangelism             |                                   |
| Associate Content Editor                | Intermediate Content Editor                  |                                 |
| Intermediate Content Editor               | Senior Content Editor                  |                                 |
| Senior Content Editor               | Managing Editor                  |                                 |
| Associate Analyst Relations Manager               |  Analyst Relations Manager                  |                                 |
| Analyst Relations Manager               | Senior Analyst Relations Manager                  |                                 |
| Senior Analyst Relations Manager               | Manager, Analyst Relations                  |                                 |
| Manager, Analyst Relations               | Director, Analyst Relations                  |                                 |
| Associate Reference Program Manager              | Reference Program Manager                   |                                 |
| Reference Program Manager              | Senior Reference Program Manager                   |                                 |
| Senior Reference Program Manager              | Manager, Reference Program                   |                                 |
| Manager, Reference Program              | Director, Reference Program                   |                                 |
| Associate Market Research & Customer Insights Manager        | Market Research & Customer Insights Manager             |                      |
| Market Research & Customer Insights Manager             | Manager, Market Research & Customer Insights              |                      |
| Manager, Market Research & Customer Insights              | Director, Market Research & Customer Insights              |                      |
| Associate Competitive Intelligence Manager              | Competitive Intelligence Manager                   |                                 |
| Competitive Intelligence Manager              | Senior Competitive Intelligence Manager                   |                                 |
| Senior Competitive Intelligence Manager              | Manager, Competitive Intelligence Manager                   |  
| Manager, Competitive Intelligence Manager              | Director, Competitive Intelligence Manager                   |
| Associate Partner Marketing Manager	 | Partner Marketing Manager                  |                                   |   
| Partner Marketing Manager	 | Senior Partner Marketing                  |                                   |       
| Senior Partner Marketing Manager	 | Manager, Partner Marketing                  |                                   |  
| Manager, Partner Marketing	 | Director, Partner Marketing                  |                                   |                     
| Associate Product Marketing Manager	 | Product Marketing Manager                 |                                   |
| Product Marketing Manager	           | Senior Product Marketing Manager                |                                   |
| Senior Product Marketing Manager	           | Manager, Product Marketing          |                                   |
| Manager, Product Marketing	         | Director, Product Marketing         |                                   |
| Director, Product Marketing	   | Senior Director, Product Marketing               |                                   |
| Associate Technical Marketing Manager	         | Technical Marketing Manager        |                                   |
| Technical Marketing Manager	           | Manager, Technical Marketing                |                                   |
| Technical Marketing Manager	           | Senior Technical Marketing Manager          |                                   |
| Manager, Technical Marketing	         | Director, Technical Marketing         |                                   |
| Director, Technical Marketing	   | Senior Director, Technical Marketing               |                                   |
