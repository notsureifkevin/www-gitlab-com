---
layout: markdown_page
title: Learning and Development
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

Welcome to the Learning & Development (L&D) page at GitLab! L&D is an essential part of any organization's growth, success and overall business strategy. We want to support the growth of our GitLab team-members' competencies, skills and knowledge by providing them with the tools they need and also the opportunities to progress their own personal and professional development.  

## What is Career Development

It is the lifelong process of managing learning, work, leisure and transitions in order to move toward a personally determined and evolving preferred future.

### Roles and Responsibilities

Employee Owned
 - Take charge of your own development and career
 - Close the skill gap for current and future positions
 - Seize development and stretch opportunities
 - Remember there are no promises or guarantees of a promotion

Manager Facilitated
- Take time with team members to discuss their career aspirations
- Listen and provide feedback, ideas and contacts
- Make choices to support their development

GitLab Supported
- Communicate future direction and skills requirements
- Provide information and tools when applicable
- Communicate internal opportunities

70-20-10 Rule for Development
- 70% of your development should come from on-the-job and action learning.  This can include development experiences like managing a project, serving on a cross-functional team, taking on a new task, job shadowing, job rotation, etc.
- 20% of your development should come from interactions with others.  This includes having a mentor, being a mentor, coaching, participating in communities of practice, serving as a leader in your organization, etc.
- 10% of your development should come from training, including classes, seminars, webinars, podcasts, reading, conferences, etc.

Additional Questions to Think About
- Do you have any overused strengths or underdeveloped skills that might cause your career to stall or derail?
- Considering feedback from others, are you perceived to have the skills required for the business needs of the future? If not, how could you shape that perception in a favorable direction?
- How can you leverage your current skills and talents for your future aspirations?
- What skills or talents are missing to qualify you for your future aspirations?
- Does your feedback from others tell you anything about how feasible your aspirations are?
- Do you currently have the skills and talents needed for the future business needs?  If not what can you do now to get ready?

### External Resources some with no cost

- [Stanford's Centre for Professional Development](http://scpd.stanford.edu/home)
- [Yale Open Courseware](https://oyc.yale.edu/)
- [MIT Open Courseware](https://ocw.mit.edu/index.htm)
- [Notre Dame Open Courseware](https://www.edx.org/school/notredamex)

- [WorkLife with Adam Grant Podcast](https://www.ted.com/series/worklife_with_adam_grant)

- [Dose of Leadership with Richard Rierson - Authentic & Courageous Leadership Development](https://www.stitcher.com/podcast/dose-of-leadership-podcast)

### Internal Resources 

During our Career Development workshops at Contribute, May 2019 we took team members through tips to creating a clear growth (aka development) plan. Below are the resources from that session: 

- [Career Development Workshop, slides](https://docs.google.com/presentation/d/1yY0ofMGgzN07ylTAnRP5geFnWcgUYkiVlcIyR54tpD0/edit#slide=id.g29a70c6c35_0_68)
- [Individual Growth Plan template](https://docs.google.com/document/d/1ZjdIuK5mNpljiHnFMK4dvqfTOzV9iSJj66OtoYbniFM/edit)
- [Tips for Creating Effective Growth Plans](https://docs.google.com/document/d/1O45gRkQqUa3dEgjJXGwdBE7iZbBI22EPC7zrkS3T4dM/edit)

### Career Mapping and Development
{:#career-mapping-and-development}

We have started this process at GitLab by defining Junior, Senior and Staff advancement levels. Career Mapping helps GitLab team-members to understand and develop the skills they need to achieve their goals, giving them clear criteria.  
Mapping helps managers and leaders internally develop the skills and knowledge they need to achieve future business goals. The key to this is to identify the key skills, knowledge, and abilities needed to master each level. Another essential tool is a career development plan, here are some examples:

- [Career Plan](https://docs.google.com/document/d/1hJIzMnVhEz3X4k24oAwNnlgGhBeQ518Cps9kLVRRoWQ/edit)
- [Template  - Development Scorecard](https://docs.google.com/spreadsheets/d/1DBrukzzsV6InaCkZf8_ngLeTcLQ9uj6ynE93qLmHkQA/edit#gid=1677297587)
- [Career Plan Template](https://performancemanager.successfactors.com/doc/po/develop_employee/carsample.html)

Managers should discuss career development at least once a month at the [1:1](/handbook/leadership/1-1/) and then support their team members with creating a plan to help them achieve their career goals. If you would to know more about this please checkout the [career mapping course video](https://www.youtube.com/watch?v=YoZH5Hhygc4)

### The Relationship Between Learning and Development and Promotion

As is highlighted in our [Leadership](/handbook/leadership/1-1/#key-points) section, GitLab team members should not feel pressure to climb the proverbial ladder. We recognize that not everyone wants to advance or move to a new level, and that is supported. Developing one's skills and promotion at the company are not mutually exclusive.

It is perfectly acceptable to seek out learning and development opportunities — to sharpen one's understanding of a coding language to better understand a function, etc. — yet not strive for promotion beyond your current role. Some team members are happier and more productive without managing a team, for example. 

As detailed in GitLab's [Definition of Diversity & Inclusion](/company/culture/inclusion/), we recognize that unique characteristics and experiences form how we as individuals approach challenges and solve problems. They also shape how we view success in our individual careers and lives. Not everyone views promotion as a measure of success, and team members will not be thought less of or penalized for holding this view.

As part of GitLab's [Transparency](/handbook/values/#transparency) value, team members are encouraged to be open and honest with their manager. You are encouraged to learn and develop your skills without pressure to in turn seek promotion. If you feel you are not being supported in this way, please visit the [Need Help?](/handbook/people-operations/#reach-peopleops) portion of the People Group Handbook. 

## Language Courses

If you have any language courses you would like to recommend or links to websites please add them to this section.

 - [The 8 Best Interactive Websites for Adults to Learn English](https://www.fluentu.com/blog/english/best-websites-to-learn-english/)

There is also a way to practice foreign languages and ask for advice in several Slack channels, each dedicated to a specific language. You can find all these channels by searching for channels starting with #lang. If you're missing a channel for your target language, feel free to create one and mention it in #company-announcements so that fellow GitLab team-members can join too!

## New Manager Enablement Program

GitLab has a growing [resource](/handbook/people-operations/learning-and-development/manager-development/) to enable all team members transitioning to a manager role. It contains a link to a checklist, readings, and a form to help learning and development customize your development as a manager.


## Common Ground: Harassment Prevention Training

All team members will be sent an invitation link from the people team to complete this training using [Will Interactive's LMS](https://learning.willinteractive.com/). Once you receive the email please do the following:

1. Underneath the green **Sign In** box, click on the **Sign Up Now** link (also in green) which is right after *Don't have an account?*
1. Enter in your name and GitLab email address
1. Create a password
1. You may be sent a link to verify your account
1. Once you have logged in successfully you will be taken to your home screen
1. Once there you should see the course title **GitLab for Supervisors: Common Ground Business for Supervisors** or **GitLab for Employees:Common Ground Business for Employees**, if you live in California the course title will include CA within it.
1. Click on that to begin the training.
1. For managers and leaders, this is 2 hours long, but you can stop and come back to it. For all other GitLab team-members, this is 1 hour long.
1. You can also use the navigation bar at the top right-hand side of screen for volume and screen settings
1. To the left and right of the center screen you should see this symbol: > which you can click on to move forward and back through the training
1. Once completed, People Operations Specialists will upload a copy of the certificate in BambooHR in the *verification* folder
1. You may also keep a record of the certificate for your own files. To create the certificate, click on *view* in the course title
1. Scroll down to *users* then click on *completion certificates* to download the PDFs

Our [Anti-Harassment Policy](/handbook/anti-harassment/?private=1) outlines guidelines, reporting and disciplinary action.  

If you have any questions or need further help please ping people ops in the `#peopleops` channels in slack.
