---
layout: markdown_page
title: "The Internal Audit Function"
---


## On this page
{:.no_toc}

- TOC
{:toc}

# The Mission of Internal Audit

To enhance and protect organizational value by providing risk-based and objective assurance, advice and insight, while consistently building trust and strengthening the relationship with management, through the delivery of high quality and distinctive internal audit services.

# What is Internal Audit? 

Internal Audit is an independent, objective assurance and consulting activity designed to add value and improve organization’s operations. 

Internal Audit is responsible to assess the effectiveness of risk management, control and governance processes and to provide insight and recommendations that can enhance these processes, particularly relating to: 

* Effectiveness of operations;
* Reliability of financial management and reporting; and
* Compliance with laws and regulations.

Internal Audit may also involve conducting fraud investigations to identify fraudulent acts and conducting post investigation fraud audits to identify control breakdowns and establish financial loss.

# How Internal Audit Adds Value

* Internal Audit works closely with management to review systems and operations to identify how well risks are managed, whether the right processes are in place, and whether agreed procedures are being followed.This provides an indication of the integrity of the organization’s systems and processes, their capability to support the set goals and also helps identify areas for improvements. 

* Internal Audit works across all areas of an organization, review tangible (e.g. supply chain/ IT systems) and intangible (e.g. organization culture and ethics) aspects of operations. 

* Internal Audit looks beyond financial statements and financial risks, and consider wider issues, e.g. the organization’s reputation, growth, impact on the environment, and how employees are treated. Any process that has an impact on the effective operation of an organization may be included in internal audit’s scope.

Internal Audit reports to the CFO, Board of Directors through an Audit Committee and they provide an independent viewpoint on the Internal Controls and their effectiveness.

# Internal Audit Activity Charter (effective 2019-06-07)

 [Model Internal Audit Activity Charter](https://na.theiia.org/standards-guidance/Public%20Documents/Model%20Internal%20Audit%20Activity%20Charter.pdf) by the [Institute of Internal Auditors](https://na.theiia.org/standards-guidance/topics/pages/the-internal-audit-function.aspx)

**Introduction**

Internal Audit is an independent and objective assurance and consulting activity that is guided by a philosophy of adding value to improve the operations of the GitLab. It assists in accomplishing its objectives by bringing a systematic and disciplined approach to evaluate and improve the effectiveness of the organization's governance, risk management, internal control.

**Role**

Currently, the internal audit activity is established by the finance team reporting to the Chief Financial Officer. In the future, the reporting will be to the Audit Committee of the GitLab Board of Directors. The internal audit activity’s responsibilities will be defined by the Audit Committee as part of its oversight role.

**Professionalism**

The internal audit activity will govern itself by adherence to The Institute of Internal Auditors' mandatory guidance including the Definition of [Internal Auditing](https://global.theiia.org/standards-guidance/mandatory-guidance/Pages/Definition-of-Internal-Auditing.aspx), the [Code of Ethics](https://global.theiia.org/standards-guidance/mandatory-guidance/Pages/Code-of-Ethics.aspx), and the [International Standards for the Professional Practice of Internal Auditing](https://global.theiia.org/standards-guidance/mandatory-guidance/Pages/Standards.aspx). This mandatory guidance constitutes principles of the fundamental requirements for the professional practice of internal auditing and for evaluating the effectiveness of the internal audit activity’s performance. The Institute of Internal Auditors' [Practice Advisories](https://global.theiia.org/standards-guidance/recommended-guidance/Pages/Practice-Advisories.aspx), [Practice Guides](https://global.theiia.org/standards-guidance/recommended-guidance/practice-guides/Pages/Practice-Guides.aspx), and [Position Papers](https://global.theiia.org/about/about-internal-auditing/Pages/Position-Papers.aspx) will also be adhered to as applicable to guide operations. In addition, the internal audit activity will adhere to GitLab relevant policies and procedures and the internal audit activity's standard operating procedures manual.

**Authority**

The internal audit activity, with strict accountability for confidentiality and safeguarding records and information, is authorized full, free, and unrestricted access to any and all of GitLab records, physical properties, and personnel pertinent to carrying out any engagement. All employees are requested to assist the internal audit activity in fulfilling its roles and responsibilities.

**Organization**

The Senior Internal Audit Manager will report to the Chief Financial Officer. In the future, reporting will be functionally to the Board/ Audit Committee and administratively (i.e. day to day operations) to the Chief Financial Officer.

**Functions of the Audit Committee**

The Audit Committee will perform the following functions:
1. Approve the internal audit charter. 
2. Approve the risk-based internal audit plan.
3. Approve the internal audit budget and resource plan. 
4. Receive communications from the Senior Internal Audit Manager on the internal audit activity’s performance relative to its plan and other matters.
5. Approve decisions regarding the appointment and removal of the Senior Internal Audit Manager.
6. Approve the remuneration of the Senior Internal Audit Manager. 
7. Make appropriate inquiries of management and the Senior Internal Audit Manager to determine whether there is inappropriate scope or resource limitations. 

The Senior Internal Audit Manager will communicate and interact directly with the Audit Committee, including in executive sessions and between Audit Committee meetings as appropriate.

**Independence and Objectivity**

The internal audit activity will remain free from interference by any element in the organization, including matters of audit selection, scope, procedures, frequency, timing, or report content to permit maintenance of a necessary independent and objective mental attitude. 

Internal auditors will have no direct operational responsibility or authority over any of the activities audited. Accordingly, they will not implement internal controls, develop procedures, install systems, prepare records, or engage in any other activity that may impair internal auditor’s judgment.

Internal auditors will exhibit the highest level of professional objectivity in gathering, evaluating, and communicating information about the activity or process being examined. Internal auditors will make a balanced assessment of all the relevant circumstances and not be unduly influenced by their own interests or by others in forming judgments. 

The Senior Internal Audit Manager will confirm to the Audit Committee, at least annually, the organizational independence of the internal audit activity.

**Responsibility**

The scope of internal auditing encompasses, but is not limited to, the examination and evaluation of the adequacy and effectiveness of the organization's governance, risk management, and internal controls as well as the quality of performance in carrying out assigned responsibilities to achieve the organization’s stated goals and objectives. This includes:
* Evaluating risk exposure relating to achievement of the organization’s strategic objectives
* Evaluating the reliability and integrity of information and the means used to identify, measure, classify, and report such information.  
* Evaluating the systems established to ensure compliance with those policies, plans, procedures, laws, and regulations which could have a significant impact on the organization.
* Evaluating the means of safeguarding assets and, as appropriate, verifying the existence of such assets. 
* Evaluating the effectiveness and efficiency with which resources are employed.
* Evaluating operations or programs to ascertain whether results are consistent with established objectives and goals and whether the operations or programs are being carried out as planned.
* Monitoring and evaluating governance processes. Monitoring and evaluating the effectiveness of the organization's risk management processes.  
* Evaluating the quality of performance of external auditors and the degree of coordination with internal audit.
* Performing consulting and advisory services related to governance, risk management and control as appropriate for the organization.
* Reporting periodically on the internal audit activity’s purpose, authority, responsibility, and performance relative to its plan.
* Reporting significant risk exposures and control issues, including fraud risks, governance issues, and other matters needed or requested by the Audit Committee.
* Evaluating specific operations at the request of the Board or management, as appropriate.

**Internal Audit Plan**

At least annually, the Senior Internal Audit Manager will submit to senior management and the Audit Committee an internal audit plan for review and approval. The internal audit plan will consist of a work schedule as well as budget and resource requirements for the next fiscal/calendar year. The Senior Internal Audit Manager will communicate the impact of resource limitations and significant interim changes to senior management and the Audit Committee.

The internal audit plan will be developed based on a prioritization of the audit universe using a risk-based methodology, including input of senior management and the Audit Committee. The Senior Internal Audit Manager will review and adjust the plan, as necessary, in response to changes in the organization’s business, risks, operations, programs, systems, and controls. Any significant deviation from the approved internal audit plan will be communicated to senior management and the Audit Committee through periodic activity reports.

**Reporting and Monitoring**

A written report will be prepared and issued by the Senior Internal Audit Manager or designee following the conclusion of each internal audit engagement and will be distributed as appropriate. Internal audit results will also be communicated to the Audit Committee.

The internal audit report may include management’s response and corrective action taken or to be taken in regard to the specific findings and recommendations. Management's response, whether included within the original audit report or provided thereafter (i.e. within thirty days) by the management of the audited area should include a timetable for anticipated completion of action to be taken and an explanation for any corrective action that will not be implemented.

The internal audit activity will be responsible for appropriate follow-up on engagement findings and recommendations. All significant findings will remain in an open issues file until cleared.

The Senior Internal Audit Manager will periodically report to senior management and the Audit Committee on the internal audit activity’s purpose, authority, and responsibility, as well as performance relative to its plan. Reporting will also include significant risk exposures and control issues, including fraud risks, governance issues, and other matters needed or requested by senior management and the Audit Committee.

**Quality Assurance and Improvement Program**

The internal audit activity will maintain a quality assurance and improvement program that covers all aspects of the internal audit activity. The program will include an evaluation of the internal audit activity’s conformance with the Definition of [Internal Auditing](https://global.theiia.org/standards-guidance/mandatory-guidance/Pages/Definition-of-Internal-Auditing.aspx) and the [Standards](https://global.theiia.org/standards-guidance/mandatory-guidance/Pages/Standards.aspx) and an evaluation of whether internal auditors apply the [Code of Ethics](https://global.theiia.org/standards-guidance/mandatory-guidance/Pages/Code-of-Ethics.aspx). The program also assesses the efficiency and effectiveness of the internal audit activity and identifies opportunities for improvement.

The Senior Internal Audit Manager will communicate to management and Audit Committee on the internal audit activity’s quality assurance and improvement program, including results of ongoing internal assessments and external assessments conducted at least every five years.


## Internal Audit Performance Measures

To build and continually improve an effective internal audit function, GitLab estlablishes, the following key objectives for conducting internal audits, the outcome measures associated with each objective, and the activity measures that drive each outcome measure. A link connects each outcome measure with its corresponding formula and analysis of the formula.

**1. Build the Internal Audit (IA) department as an internal knowledge resource**

   *Outcome Measures:* Percentage of IA team members  devoted to orientation, work paper reviews and training.
   
   *Activity Measures:* Average number of hours to complete an audit, Number of audits performed per year per auditor
   
   *Link:* (Hours for training new auditors and performing work paper reviews divided by the total hours expended by IA staff) x 100. This formula measures the total amount of resources required to train and orient auditors to the audit department policies and methodologies, as well as the culture and business processes of the company.
   


**2. Position Internal Audit as a change agent to improve controls**  


*Outcome Measures:* Percentage of recommendations implemented

*Activity Measures:* Number of audit findings that are implemented

*Link:* (number of recommendations implemented divided by total recommendations) x 100

  



