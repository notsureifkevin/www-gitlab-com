---
layout: markdown_page
title: "Handbook Style Guide"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

GitLab's general communications practices are detailed at [GitLab Communication](/handbook/communication/), but beyond those we do have some channel-specific guidance available. This page is for Handbook-specific guidance that does not necessarily apply to the company overall or to other specific channels such as [GitLab Documentation](https://docs.gitlab.com/) or the [GitLab Blog](https://about.gitlab.com/blog/).

In the absence of Handbook-specific guidance, you can't go wrong by following GitLab's [Writing Style Guidelines](https://about.gitlab.com/handbook/communication/#sts=Writing%20Style%20Guidelines) and [Documentation Style Guide](https://docs.gitlab.com/ee/development/documentation/styleguide.html). The GitLab Documentation site also offers a list of [topic-specific style guides](https://docs.gitlab.com/ee/development/contributing/style_guides.html).

So what is style? Is it aesthetics? Information architecture? Interaction design? Technical implementation conventions? It can be all of these things, but we'll start this style guide with some information about common elements and tasks.

## Headings

Content headings should be descriptive enough to suggest their corresponding content when they or corresponding fragment identifiers are seen out of context.

Content headings should be second-level headings and below. First-level headings are reserved for page titles.

Fragment identifiers are automatically generated from most headings. Keep that in mind when choosing your headings.

## Letter case

Handbook content currently uses both `Title Case` and `Sentence case` for titles and headlines, but we should probably standardize them. The GitLab Blog [uses sentence case](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/editorial-team/#case), but title case conveys meaning that is useful in reference-heavy contexts, so the Handbook may or may not end up following the same convention.

See [URL guidelines](#url-guidelines) for information about letter case in URLs.

## Links

### Link guidelines

Links from the Handbook should conform to a few general guidelines:

* Linked text should describe the content to which the link leads. A page title or description is usually a good choice and preferable to something like "this" or "here".
* Link URLs should not result in unnecessary redirection. HTTP redirection (e.g., via [redirect.rb](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/lib/redirect.rb) and [redirects.yml](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/redirects.yml)) can be helpful, especially for handling external links and bookmarks, but it is better for link and bookmark reliability (and sometimes loading speed) to skip redirection whenever possible.
* Link URLs should not specify default file names or extensions unless excluding them would result in redirection. Overly specific link URLs are more likely to break in response to future changes.
* Same-site links should use root-relative URLs, not absolute URLs or document-relative URLs.
* Same-_page_ URLs should just be fragment identifiers (e.g., `#link-guidelines`).

See [Understanding absolute and relative URLs](#understanding-absolute-and-relative-urls) for more information about each type of URL.

## URLs

### URL guidelines

In general, Handbook URLs should describe their content and be as clean and easy to remember as feasible. Unless there is a good reason for formatting URLs otherwise (e.g., language conventions or technical limitations), they should be entirely lower case, any two words should be separated by a single hyphen, and ampersands should be replaced by "and", _not_ an additional hyphen.

Files and directories should _never_ be given names that differ from the names of other files or directories in the same location only by letter case. Case-sensitive file systems allow such naming, but our source code is shared with people using various different file systems, including case-_insensitive_ file systems, upon which the mere existence such similarly named files in a repository can cause problems. For example, new macOS systems use case-insensitive APFS by default and on those systems Git can end up alternately showing two differently-capitalized files as modified when no changes have actually been made to either.

### Understanding absolute and relative URLs

When you’re adding or editing links in the Handbook (or really, _anywhere_ on the marketing site) please keep in mind that root-relative URLs are preferable for our same-site links.

Absolute URLs are the ones that look like this:

```
https://about.gitlab.com/handbook/
```

With absolute URLs links always load the _live_ version of the site—even when run from a development, testing, or staging context. They're great for sharing URLs via Slack, issue and merge request comments, email, and social media, but _not_ for same-site links.

Document-relative URLs are the ones that look like this:
```
../product/technical-writing/
```

With document-relative URLs links can break when either a linking document or a linked document is moved separately from the other document. These kinds of URLs can be great for maintaining relationships between documents in different contexts, but links that use this kind of URL are best managed by automated systems that won't forget to update them.

Root-relative URLs, _the kind we prefer for use in the Handbook_, look like this:

```
/handbook/communication/
```

With root-relative URLs same-site links can work properly during local development and testing, during automated tests and staging, and on the live site. And unlike with links that use document-relative URLs, moving a document won't break its same-site links to other pages that do not move with it.

## Related resources

* [GitLab Communication](https://about.gitlab.com/handbook/communication/)
* [Blog style guide](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/editorial-team/#blog-style-guide)
* [GitLab Documentation guidelines](https://docs.gitlab.com/ee/development/documentation/)
  * [Documentation Style Guide](https://docs.gitlab.com/ee/development/documentation/styleguide.html)
* [Style guides](https://docs.gitlab.com/ee/development/contributing/style_guides.html)
* [Blog style guide](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/editorial-team/#blog-style-guide)
* [Pajamas Design System](https://design.gitlab.com/)
* [Markdown Guide](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/)