---
layout: markdown_page
title: "GitLab Onboarding Processes"
---

----

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Onboarding Processes

This page is a guide for People Operations Specialists when onboarding new team members. All onboarding tasks and guidelines for new team members are in the [People Ops Onboarding Issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md).  Each onboarding issue is structured with general tasks at the top and department or role-specific tasks below.  In some cases, specific task may link to a supplemental issues such as in-depth training. All new hires will receive all invitational access to their GitLab email, GitLab accounts, and any other work-related accounts on Day 1.

People Operations Specialist will create the on-boarding issue and start the [onboarding tasks](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/) no later than one week before the new team member joins. Should a contract not be signed prior to 4 working days from the start date, a new start date will be required.

To ensure we create a fluid onboarding experience, provide enough time to the IT Ops team for laptop orders and less delays on day 1, People Operations specialists require a minimum of 4 business days (with the new hire timezone as the basis) before the new hire's start date.
At 14:00 UTC every Wednesday, `PeopleOps Bot` slack bot will send an alert in the `#peopleops-alerts` channel which includes details missing from BambooHR account of team members joining the following week. People Operations Specialist team will fill these missing details as soon as possible since completeness and accuracy of BambooHR profile is necessary for all automation.

At 16:00 UTC every Thursday, the bot will send another alert in the `#peopleops-alerts` channel with a list of new team members joining the following week. This list includes names, email address, joining date and job title of the new hires along with link to a Periscope graph showing the hiring progress over time. After a final and quick round of verification of the details, People Operations Specialist team will then post it in the `#team-member-updates` channel welcoming all new team members.

### Onboarding Issue Tasks
The People Operations Specialist will create the GSuite account first. Thereafter, update BambooHR with the information documented [here](/handbook/general-onboarding/onboarding-processes/#adding-a-new-team-member-to-bamboohr).
The People Operations Specialist will then create an onboarding issue with a ChatOps command in Slack 4-5 business days prior to the new team member's start date:
   1. Log into BambooHR and verify the information is accurate: Name, Job title, Starting date, Department, Country, Entity (Inc, BV, Safeguard, CXC, Lyra etc. etc.), People Manager or not, Name of the Manager (to assign the onboarding issue to).
   1. Use command: /pops run onboarding +BambooHR ID number (not Employee ID #). This can be found in the URL when the employee's file is open and usually straight after the `=` sign. If BambooHR's API is down, this ChatOps command will fail and will need to be created manually.  
   1. Once this has been created, check the accuracy of reporting lines and job titles. If the manager is not automatically tagged, it could be because the employee's manager uses a name in GitLab that is very different from the one in BambooHR, and they don't use their `@gitlab.com` email id as the primary email id in GitLab. They will then manually need to be assigned.
   1. Once created, copy and paste the link into the Google sheet [GitLab Onboarding Tracker](https://docs.google.com/spreadsheets/d/1L1VFODUpfU249E6OWc7Bumg8ko3NXUDDeCPeNxpE6iE/edit?usp=sharing). Please note that this document is only viewable to PeopleOps and ITOps to protect personal information.
 

- **I-9 and E-Verify**<a name="I-9"></a>
For all GitLab Inc employees, GitLab complies with all USCIS requirements and laws, including the I-9 and E-Verify process.
    - New hires are sent an [email](https://docs.google.com/document/d/1uWbIfAZrEbf7JTM4zR0PSbG_3eq4jrt_jIIrV_CorfM/edit) with instructions on GitLab's I-9 process approximately one week before their start date.
    - GitLab uses an online I-9 portal where the process is completed and documents are saved electronically and securely. Only the two US-based People Operations Specialists who are US Citizens have administrative access to this portal.
    - New hires are instructed to complete Section 1, and to designate an agent for Section 2, as outlined in the email template.
    - The People Operations Sepcialist will monitor responses to the I-9 Designated Agent form daily. [here](https://docs.google.com/spreadsheets/d/16vUt2P6Rz1kXtmdWsmOolQzjDV8RmybqYSUykeDqSZU/edit#gid=841432954)
    - Once a response is received, the People Operations Specialist will enter and designate the agent's contact information (Name and email) into the online I-9 system. The People Operations Specialist sends the invitation to the designated agent to have access to their designated team member's I-9 form.
    - Once Section 2 is completed by the designated agent, all access is disabled to both the new hire and designated agent.
    - The People Operations Specialist verifies that the new hire and their agent uploaded correct, high-resolution, uncropped images or scans of their designated I-9 documents. If the documents are missing or unacceptable, the People Operations Specialist must communicate with the new hire so that the People Operations Specialist may upload the correct documents into the portal.
    - The People Operations Specialist must audit the I-9 form, to see if the entered information matches to the scans of the designated documents. If there are any discreptancies or incorrectly entered information, the People Operations Specialist must audit the I-9 and correct the error.
    - The People Operations Specialist will run the E-Verify process manually (if it did not already run automatically), by clicking the red "E" in the new hire's employee profile in the portal.
    - The People Operations Specialist verifies that the Photo Match is completed. If it has not automatically been completed, the People Operations Specialist must click on the "Photo Match" link in the EVerify Employment Verification section in the employee profile. 
    - Once the I-9 process is complete, save a screenshot of the Employment Authorized screen message and upload to the Verification Docs folder in their BambooHR profile. Also upload the scans of their documents in the same folder. Check the tasks listed in the onboarding issue related to this process and notify the People Operations Analyst no earlier than the new hire's Day 1 that the process has been completed.
- **Invite to swag store**<a name="swag-store"></a>
Send "New Hire Swag" email to new hire. It must state that they need to access the [GitLab Swag Store](https://shop.gitlab.com/), choose goods and use the new hire discount code provided in the email at checkout. New hires must to use their GitLab email at checkout.
- **Add team member to Beamy**<a name="add-beamy"></a>
Login in to access the settings for the [Beam](https://suitabletech.com/accounts/login/). In the top menu move your cursor over the blue login button. Go to "Manage your beams". Click on "manage" in the lower left corner. Enter the GitLab email and scroll down to find the newly added email. Check the box for "Auto connect".
- **Add team member to Expensify (only with employees)**<a name="add-expensify"></a>
Log in to [Expensify](https://www.expensify.com/signin) and go to "Settings" in the left sidebar. Select the right policy based upon the entity that employs the new team member. Select "People" in the left menu. Select "Invite" and add the GitLab email. Add the GitLab manager as "submits to".  For employees in the U.S. use the proper "approves to" option.  Sync with the other specialists if you are not sure what this is.  For employees outside of the US set "submit to" as `wilson@gitlab.com` Click "invite". Verify that the role is set properly, a majority of the team it should be set as "employee" If the team member should be added as an admin to be able to also add new team members, update them to a [domain admin](https://docs.expensify.com/advanced-admin-controls/domain-members-and-groups).
- **Add team member to our NexTravel platform**<a name="add-nextravel"></a>
New team member should join NexTravel via link in onboarding issue.  
- **Add to Moo**<a name="business-cards"></a>
We expect every team member to be an advocate for GitLab and we offer every team member the opportunity to have business cards, to make GitLab feel real to people around them despite not having an office. Every week, the People Operations DRI creates a Report in BambooHR containing all Active Employees in three columns: their First Name, their Last Name, their GitLab work email. People Ops saves this report as a csv file and sends it to our Moo Rep, who sends out email invitations to new team members from the Moo platform. Business cards should be ordered by the team member themselves using the Moo platform. Self-help instructions are [here](/handbook/people-operations/#business-cards).
- **Upgrade team member's Zoom account to Pro**<a name="make-zoom-pro"></a>
People Ops Specialists will be assigned this task through the [Zoom Pro Access Request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=zoom_pro) issue if a team member requests this upgrade. Follow the steps below to complete.
1. Log in to the People Ops [Zoom](https://gitlab.zoom.us) account using the credentials stored in 1Password.
1. Click "My Account" in the top right corner.
1. On the left sidebar, under "User Management", click "Users".
1. In the search bar, search for the team member's name (sometimes you may have to search for their email address instead). If the user does not appear yet in Zoom, you can double check with them that they created their Zoom account using their GitLab email address, or you can add them manually by clicking "Add Users" button at the top of the User Management tab.
1. Under "Type", confirm if they have a Pro account. If it says "Basic", click on "Edit" and choose "Pro" and save.
1. If you receive an error, this means we need to purchase more Zoom Pro licenses. This can be done by People Operations Specialists by sending an email to our account manager at Zoom, as listed in a shared note on 1Password. The turnaround time is typically 2-3 business days for the new licenses to become activated.
1. Click on their email address.
1. Where it says "Personal Link" click "Customize" and type `gitlab.firstnamelastname` and click "Save Changes". (Please keep in mind that Zoom only accepts the English alphabet.)
1. The Pro account and personal Zoom room is now active!

## Ordering Supplies

If a GitLab team-member is in need of supplies and is unable to purchase the items themselves, People Operations Specialists can place the order, per [Spending Company Money](/handbook/spending-company-money/).

Use the Amazon business account for all Amazon links. In order to see what is available to ship in each country use that country's Amazon website to sign in, place the order, and ship.

For Apple products sync with IT Ops to place the order.

See more at the [IT Ops policy for laptops](/handbook/business-ops/it-ops-team/#laptops)

After an order has been placed or a laptop has been shipped, update Finance and BambooHR via [asset tracking](/handbook/finance/accounting/#asset-tracking).

## Adding a New Team Member to BambooHR

As part of [onboarding](/handbook/general-onboarding/), People Operations Specialists will process new hires in BambooHR. Aside from the steps listed in the onboarding issue, this is a description of how to add the proper information into BambooHR.

Personal Tab

1. Verify the team member was given an Employee ID number.
1. Enter the appropriate Country.
1. Enter the locality ([geo area](/handbook/people-operations/global-compensation/#geographical-areas), country). This information should be visible in the offer details. If you have any questions, please reach out to the compensation team.
  * Please note the formatting must be entered exactly as outlined in the [location factor file](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/location_factors.yml) with "area, country" or the integration outside BambooHR will fail. 
1. Region: Should either be Americas, EMEA, or JAPAC.
1. Verify the work email is entered.

After verifying the work email is set up, go to top right corner, and set access level to "employee self service"

Jobs Tab

1. Hire Date - This will be blank. Make sure to enter in the correct date.
1. Role
   * Leader - if director or above
   * Manager - if has any direct reports
   * Individual Contributor - all others
1. Cost Center - Leave blank for now. This will become relevant as we scale.
1. Payroll Type
   * Employee - paid through Payroll
   * Contractor - IND - Independent Contractor agreement
   * Contractor - C2C - Contractor Company agreement
   * PEO - anyone employed via a Professional Employer Org
1. Exception to IP Agreement - If they answered Yes, on the IP agreement in the contract. Also, send the text for the exception in an email to the VP of Engineering for approval. File the approved email in BambooHR.
1. Compensation Table
   * Use the BambooHR Calculator in Google Drive to do the following steps. Reach out to an Analyst if you need access.
   * Effective Date - Hire Date
   * Pay Rate - Entered as if it were a payroll amount. For example, a US employee would be entered as their yearly amount divided by 24 payrolls in a year. A contractor would have their monthly contract amount listed.
   * Pay Per - Monthly for contractors and employees paid once per month, Pay Period for all other employees
   * Pay Type - Use either Salary or Hourly for employees, or Contract for contractors.
   * Pay Schedule - Select the pay period. Currently we have twice a month for the US, and monthly for all others.
   * Overtime - This is the FLSA Code - This will either be exempt or non-exempt depending on how the role is classified. If there are questions on the classification, please ask the People Ops Analyst.
   * Change Reason - Hire
   * Comment - Please add any comments that are relevant from the contract terms.
1. Pay Frequency (Note: Pay Frequency times pay rate should equal annual compensation)
   * 12.96 for GitLab B.V. employees in the Netherlands
   * 13.92 for GitLab B.V. employees in Belgium
   * 24 for GitLab Inc. employees in the United States
   * 12 for everyone else paid monthly
1. On Target Earnings
   * If the team member does not have any variable compensation, enter "No" in the Variable Pay box, then do not add any additional information in the table. If yes, continue.
   * Add the effective date, annual amount of the variable component in local and USD, and the OTE in local and USD.
   * Lastly, add the cadence to which the variable component is paid out.
1. Currency Conversion
   * The effective date is either January 1 or July 1, whichever is more recent. Every January and July, the People Ops Analyst will conduct a currency conversion for all team members.
   * Use [Oanda](https://www.oanda.com/currency/converter/) for the currency conversion. Always convert the currency from local currency into USD so that we remain consistent.
   * Enter the currency conversation factor from Oanda with all 5 decimal places.
   * Enter the Local Annual Salary with the appropriate currency code and the converted salary in USD.
1. Job information
   * Effective Date - Hire Date
   * Location - Which entity the new team member is contracted through.
   * Division - Enter the appropriate division from the dropdown.
   * Department - Enter the appropriate department from the dropdown.
   * Job Title - Choose the job title. The title should read as Level Benchmark. If the position is at the Manager or Director level it should read as Level Manager, Benchmark. If it does not exist already, scroll to the bottom, click "Add New" and save the new job title.
   * Reports To - Select their manager.
   * Job Title Speciality - If there is a specialty (this would be seen in the contract as titling reads Level Benchmark, Specialty) use the drop down to select the appropriate specilty. 
   * Enter whether the team member is part-time or full-time. Any comments? Add them to the compensation table.
1. Employment Status
   * Enter the hire date and set the status to active. Also leave a comment if there is anything of note in the contract.
   * New team members from GitLab LTD, Lyra, and Safeguard Spain all have a three month probation period. Safeguard Ireland has a 6 month probation period. More detailed information about GitLab's probation periods are found in the [contracts handbook page](https://about.gitlab.com/handbook/contracts/#probation-period). Please use these probation periods in BambooHR for team  members in these specific groups.
   * Team members from GitLab GmbH will have a six month probation period.
   Set the status to probation period. This sets up an alert for the manager and People Ops automatically, 2 weeks, 1 week, and a day before the probation period expires. Details of the probation period process can be found on the [contracts page](/handbook/contracts/#probation-period).
1. For employees of HRSavvy, LYRA, and CIIC email the Employee ID number to our contact to align our systems.

Benefits Tab

1. Stock Options
  * Enter the start date
  * Employee or Contractor (This should match Payroll Type on the Jobs Tab)
  * Enter the number of shares from the contract.

### Auditing the BambooHR Entry

1. Download a copy of the contract from the Documents Tab. Verify the information (start date, title, stock, etc) matches the entries in BambooHR as outlined in [Adding a New Team Member to BambooHR](/handbook/general-onboarding/onboarding-processes/#adding-a-new-team-member-to-bamboohr).
1. If all the information is correct, mark the new hire as complete on the "Payroll Changes Report" in BambooHR.

### Settings in BambooHR

Changing a Format (Example: Date)

1. Click on Settings
1. Choose Account
1. Select General Settings
1. Change the date format to match desired output

Add a New Division

1. Click on Settings
1. Select Employee Field
1. Select Division
1. Add new division

### Auditing System Changes

At the end of each week a People Operations Analyst will review all data entered into BambooHR through the Payroll Change Report for audit purposes. Once a month an audit should be conducted from all payroll providers to ensure the salary information matches BambooHR.

## Analyst Onboarding Tasks

1. Audit the BambooHR Entry
1. Add to the compensation calculator
  * Add the Employee ID, First Name, Last Name
  * All other items in Blue are to be entered. Columns in black are formulas that need to be carried down.
  * Ensure the Locality follows the criteria for [geo areas](/handbook/people-operations/global-compensation/#geographical-areas).
  * If the team member is over range for their compensation, the Metics column will read as false. Please copy the formula down to generate the adjusted location factor for metrics reporting. For Sales, the Benchmark will need to be manually inputted based on the Sales Comp spreadsheets.
  * Audit the locality in BambooHR under the personal tab to ensure it matches to the comp calc.
1. Create a new profile in Lumity (if a US team member)
  * Login to Lumity
  * Click your name and then switch to admin view
  * Under employees, select manage.
  * Click Hire Employee and enter information as prompted.
  * Benefits plans are dependent on the state the new hire lives in. OOS stands for Out of State and should be selected if the new team member does not reside in California, Hawaii, or Colorado.
1. Create a new profile in Betterment
  * Login to Betterment's Business Account (this is different than the personal account)
  * Click Employees
  * Click add an Employee
  * Enter all information as prompted
  * All new team members in the US are primary, not part of a union, able to access a computer, and eligible to participate (except interns).

## Hiring Manager Onboarding Tasks  

### Add blank entry to team page <a name="blank-entry"></a>
1. Go to the [GitLab.com / www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/) project.
1. Click on the `data` folder, then on the next page click on the `team.yml` file.
1. Click on the button labeled `Web IDE` between the `Edit` and `Replace` buttons.
1. Find the vacancy entry for the new team member's position, and replace with some of the new employee details:
  * **DO NOT** change slug, unless you are copying template then use `jobabbreviation-firstname-lastinitial` (ex. `mpm-agnes-o`)
  * **Type** = change `vacancy` to `person`
  * **Name** = `First Name` `Last Initial` *only*
  * **Remove** placeholder line.
  * **Location Factor** = `0.7` (PeopleOps will update at future date)
  * **Role** = *UPDATE* URL to correct [`/job-family`](/job-families/). Make sure `Title` is accurate & matches onboarding issue
  * **Reports to** = Manager `slug` 
  * **Picture** = `../gitlab-logo-extra-whitespace.png` team member is tasked to update
  * **Departments** = *REMOVE* `Vacancy`; Add main department (i.e. Marketing or Sales).
  * **Story** = `Joins on Month XXth` team member is tasked to update.
1. If no vacancy entry to be found for this position, create an entry and make sure the information above is correct. Also:
  * Create a temporary slug, made up of shortened title / abbreviation. Search to be sure that no other team member already has this slug. If the team member will manage team members, make sure that the direct hires have the slug listed in the reports_to section. 


#### Template for New Team Member entry  
{:.no_toc}

``` 
- slug: 
  type: person
  name: 
  start_date: 
  location_factor: 0.7
  locality: 
  country: Remote
  role: <a href="#update-link">ADD TITLE</a>
  reports_to: 
  picture: ../gitlab-logo-extra-whitespace.png
  twitter:
  gitlab:
  departments:
    - Vacancy
  story:  |
          Joins on ...
```

