---
layout: markdown_page
title: "GitLab Communication"
extra_js:
  - libs/moment.min.js
  - libs/moment-timezone-with-data.min.js
  - team-call.js
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

We're an [all-remote](/company/culture/all-remote/#advantages-for-organizations) company that allows people to work from [almost anywhere in the world](/jobs/faq/#country-hiring-guidelines). We hire great people regardless of where they live, but with GitLab team-members across [more than 50 countries](/company/team/) it's important for us to practice clear communication that helps us stay connected and get things done.
For this, we use **asynchronous communication as a start** and are as open as we can be by communicating through public issues, [merge requests](#everything-starts-with-a-merge-request), and [Slack channels](/handbook/communication/chat),
and placing an emphasis on ensuring that conclusions of offline conversations are written down.
When we go **back and forth three times** we jump on a [synchronous video call](/handbook/communication/#video-calls).

We communicate respectfully and professionally at all times. If you see something that concerns you in Slack, Issues, Merge Requests, Video, Emails or any other forum, please reach out to your People Business Partner to discuss.

## External communication

There are 8 key practices to consider during any meeting. They are the following:

1. Screenshare - If this is your first time meeting a customer/prospect/partner/etc., turn on your screenshare when you login to Zoom. This will help to make the customer/prospect feel more comfortable as they are certain your undivided attention is geared towards them.
2. Agenda - Always have an agenda prepped and ready to go. Share this with your audience. Make sure that everything on the agenda is accurate and ask if there’s anything missing that needs to be addressed during this call or for the future. When there is no agenda, it translates to you not caring.
3. 70/30 Rule - Ask open ended questions that leave the audience talking 70% of the time, while you are talking 30% of the time. Please note that this varies based on the type of meeting that you are conducting. Be conscious of what questions needs to be asked and to capture those items.
4. Take Notes - Effective note-taking is a valuable skill that will help you retain and recall any important details. Be the person who remembers all the details of your audience's needs.
5. Adapt to Audience Tone - Before going into the business portion of your meeting, evaluate first the tone of the audience. Adapt your tone accordingly in order to appeal to various types of personalities.
6. Mid-call - Half-way through the meeting, check in with your audience. Ask them what their thoughts are on the progression of this meeting and if what you're presenting is on the right track. This helps both you and the audience by re-aligning expectations and making sure the meeting is going the right direction. 
7. Pre-Close Summary - 10 Minutes (1-hour meetings) or 5 minutes (30 minute meetings) prior to ending the call, ask the audience to build out an agenda for the next step or meeting. This helps to secure next steps and to ensure there are no balls dropped.
8. Post Meeting Action - Immediately write down notes and next steps and input into proper directory (Google Drive, Salesforce, etc.).
9. Two Block Rule - For in person meetings with external parties you should wait until you're more than two blocks from the meeting before discussing the results of the meeting. Nobody wants to hear themselves being discussed in the bathroom.

### Social Media

Please see our [social media guidelines](/handbook/marketing/social-media-guidelines/).

## Everything starts with a Merge Request

It's best practice to start a discussion where possible with a [Merge Request (MR)](https://docs.gitlab.com/ee/user/project/merge_requests/) instead of an issue. An MR is associated with a specific change that is proposed and transparent for everyone to review and openly discuss. The nature of MRs facilitate discussions around a proposed solution to a problem that is actionable. An MR is actionable, while an issue will take longer to take action on.

1. Always **open** an MR for things you are suggesting and/or proposing. Whether something is not working right or we are iterating on new internal process, it is worth opening a merge request with the minimal viable change instead of opening an issue encouraging open feedback on the problem without proposing any specific change directly. Remember, an MR also invites discussion, but it's specific to the proposed change which facilitates focused decision.
1. Starting with a Merge Request is part of [Handbook First](/handbook/handbook-usage/#why-handbook-first) and helps ensure the handbook is up-to-date when a decision is made.
1. Merge Requests, by default are **non-confidential**. However, for [things that are not public by default](/handbook/general-guidelines/#not-public) please open a private issue with suggestions to specific changes that are you are proposing. The ability to create [Confidential Merge Requests](https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html#merge-requests-for-confidential-issues) is also available. When possible, consider not including sensitive information so the wider community can contribute.
1. Not every solution will solve the problem at hand. Keep discussions focused by **defining the problem first** and **explaining your rationale** behind the [Minimal Viable Change (MVC)](/handbook/values/#minimum-viable-change-mvc) proposed in the MR.
1. Be proactive and consistent with communication on discussions that have external stakeholders such as customers. It's important to keep communication flowing to keep everyone up to date. MRs can appear stale if there aren't recent discussions and no clear definition on when another update will be provided, based on feedback. This leaves those subscribed in the dark, causing unnecessary surprise if something ends up delayed and suddenly jumps to the next milestone. It is important that MRs are closed in a timely manner through approving or rejecting the open requests.
1. Have a **bias for action** and [don't aim for consensus](/handbook/leadership/#making-decisions). Every MR is a [proposal](/handbook/values/#make-a-proposal), if an MRs author isn't responsive take ownership of it and complete it. Some improvement is better than none.
1. **Cross link** issues or other MRs with related conversations. E.g. if there’s a ZenDesk ticket that caused you to create a GitLab.com MR, make sure to document the MR link in the ZenDesk ticket and vice versa. And when approving or rejecting the MR, include reason or response from ZenDesk. Put the link at the top of each MR's description with a short mention of the relationship (Report, Dependency, etc.) and use one as the central one and ideally close the alternate if duplicate.
1. If submitting a change for a feature, **update the description with the final conclusions** (Why an MR was rejected or why it was approved). This makes it much easier to see the current state of an issue for everyone involved in the implementation and prevents confusion and discussion later on.
1. Submit the **smallest** item of work that makes sense. When proposing a change, submit the smallest reasonable commit, put suggestions for other enhancements in separate issues/MRs and link them. If you're new to GitLab and are writing documentation or instructions, submit your first merge request for at most 20 lines.
1. Do not leave MRs open for a long time. MR's should be **actionable** -- stakeholders should have a clear understanding of what changed and what they are ultimately approving or rejecting.
1. Make a conscious effort to **prioritize** your work. The priority of items depends on multiple factors: Is someone waiting for the answer? What is the impact if you delay it? How many people does it affect, etc.? This is detailed in [Engineering Work flow](/handbook/engineering/workflow).
1. When submitting a MVC, **ask for feedback** from your peers. For example, if you're a designer and you propose a design, ping a fellow designer to review your work. If they suggest changes, you get the opportunity to improve your design and propose an alternative MR. This promotes collaboration and advances everyone's skills.
1. Respond to comments within a **threaded discussion**. If there isn't a discussion thread yet, you can use the [Reply to comment](https://docs.gitlab.com/ee/user/discussions/#start-a-discussion-by-replying-to-a-standard-comment) button from the comments to create one. This will prevent comments from containing many interweaves discussions with responses that are hard to follow.
1. For GitLab the product merge request guidelines are in the [Contribution
guide](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#merge-request-guidelines) and code review guidelines for reviewers and maintainers are described in our [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html).
1. Even when something is not done, share it internally so people can comment early and prevent rework.
1. Create a **[Work In Progress (WIP)](/2016/01/08/feature-highlight-wip/)** merge request to prevent an accidental early merge. Only use WIP when merging it would **make things worse**, which should rarely be the case when contributing to the handbook. Most merge requests that are in progress don't make things worse, in this case don't use WIP, if someone merges it earlier than you expected just create a new merge request for additional items. Never ask someone to do a final review or merge something that still have WIP status, at that point you should be convinced it is good enough to go out.
1. If any follow up actions are required on the issue after the merge request is merged (like reporting back to any customers or writing documentation), avoid auto closing the issue.
1. If a project requires multiple approvals to accept your MR, feel free to assign multiple reviewers concurrently. This way the earliest available reviewer can start right away rather than being blocked by the preceding reviewer.

## Issues

Issues are useful when there isn't a specific code change that is being proposed or needed. For example, you may want to start an issue for tracking progress or for project management purposes that do not pertain to code commits. This can be particularly useful when tracking team tasks and creating issue boards. However it is still important to maintain focus when opening issues by defining a single specific topic of discussion as well as defining the desired outcome that would result in the resolution of the issue. The point is to not keep issues open-ended and to prevent issues from going stale due to lack of resolution. For example, a team member may open an issue to track the progress of a blog post with associated to-do items that need to be completed by a certain date (e.g. first draft, peer review, publish). Once the specific items are completed, the issue can successfully be closed. Below are a few things to remember when creating issues:

 1. When **closing** an issue leave a comment explaining why you are closing the issue and what the MVC outcome was of the discussion (if it was implemented or not).
 1. We keep our **promises** and do not make external promises without internal agreement.
 1. Be proactive and consistent with communication on discussions that have external stakeholders such as customers. It's important to keep communication flowing to keep everyone up to date. Issues can appear stale if there aren't recent discussions and no clear definition on when another update will be provided, based on feedback. This leaves those subscribed in the dark, causing unnecessary surprise if something ends up delayed and suddenly jumps to the next milestone. It is important that issues are closed in a timely manner. One way of doing this is having the current assignee set a due date for when they will provide another update. This can be days or weeks ahead depending on the situation, prioritization, and available capacity that we may have.
 1. If a user suggests an enhancement, try and find an existing issue that addresses their concern, or create a new one. Ask if they'd like to elaborate on their idea in an issue to help define the first MVC via a subsequent MR.
 1. **Cross link** issues or MRs with related conversations. Another example is to add "Report: " lines to the issue description with links to relevant issues and feature requests. When done, add a comment to relevant issues (and close them if you are responsible for reporting back, or re-assign if you are not). This prevents internal confusion and us failing to report back to the reporters.
 1. Pick issues from the current [milestone](https://gitlab.com/groups/gitlab-org/-/milestones).
 1. Use the public issue trackers on GitLab.com for everything since [we work out in the open](/2015/08/03/almost-everything-we-do-is-now-open/). Issue trackers that can be found on the relevant page in the handbook and in the projects under [the gitlab-com group](https://gitlab.com/gitlab-com/).
 1. Assign an issue to yourself as soon as you start to work on it, but not before that time. If you complete part of an issue and need someone else to take the next step, **re-assign** the issue to that person.
 1. **Regularly update** the issue description with the latest information and its current status, especially when important decisions were made during the discussion. The issue description should be the **single source of truth**.
 1. If you want someone to review an issue, do not assign them to it. Instead, @-mention them in an issue comment. Being assigned to an issue is a signal that the assignee should or intends to work on it. So you should not assign someone to an issue and mis-represent this with a false signal.
 1. Do not close an issue until it is [**done**](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#definition-of-done). It's okay to explicitly ask if everyone is on board and in agreement on how to move forward, whether to iterate, close the open issue, or create a subsequent MR to implement a MVC.

## Internal Communication

1. All written communication happens in English, even when sent one on one, because sometimes you need to forward an email or chat.
1. Use **asynchronous communication** when possible: merge requests (preferred) or issues. Announcements happen on the company call agenda and [people should be able to do their work without getting interrupted by chat](https://m.signalvnoise.com/is-group-chat-making-you-sweat-744659addf7d#.21t7089jk).
1. Discussion in issues or Merge Requests is preferred over everything else. If you need a response urgently, you can Slack someone with a link to your comment on an issue or merge request, asking them to respond there, however be aware that they still may not see it straight away. See [Slack](/handbook/communication#slack) for more.
1. If you choose to email instead of chat it is OK to send an _internal_ email that contains only a short message, similar as you would use in chat.
1. You are not expected to be available all the time. There is no expectation to respond to messages outside of your planned working hours.
1. Sometimes synchronous communication is the better option, but do not default to it. For example, a video call can clear things up quickly when you are blocked. See the [guidelines on video chats](#video-calls) for more detail.
1. It is very OK to ask as many questions as you have. Please ask them so many people can answer them and many people see the answer, so use issues or public chat channels (like #questions) instead of private messages or one-on-one emails. If someone sends you a handbook link they are proud that we have the answer documented, they don't mean that you should have found that yourself or that this is the complete answer, feel free to ask for clarification. If the answer to a question isn't documented yet please immediately make a merge request to add it to the handbook in a place you have looked for it. It is great for the person who answered the question to see you help to ensure they have to answer it only once. A merge request is the best way to say thanks for help.
1. If you mention something (a merge request, issue, commit, webpage, comment, etc.) please include a link to it.
1. All company data should be **shareable** by default. Don't use a local text file but rather leave comments on an issue.
1. When someone asks something, give back a deadline or that you did it. Answers like: 'will do', 'OK', 'it is on my todo list' are not helpful. If it is small it's better to spend 2 minutes and do the tasks so the other person can mentally forget about it. If it is large you need to figure out when you'll do it, by returning that information the other person might decide to solve it in another way if it takes too long.
1. It is OK to bring an issue to someone's attention with a CC ("cc @user"), but CCs alone are not enough if specific action is needed from someone. The mentioned user may read the issue and take no further action. If you need something, please explicitly communicate your need along with @ mentioning who you need it from.
1. Avoid creating private groups for internal discussions:
    1. It's disturbing (all users in the group get notified for each message).
    1. It's not searchable.
    1. It's not shareable: there is no way to add people in the group (and this often leads to multiple groups creation).
    1. They don't have a subject, so everyone has to remember the topic of each private group based on the participants, or open the group again to read the content.
    1. History is lost when leaving the group.
1. It is perfectly fine to create a channel, even for a single customer meeting. These channels should be named "a_<customer-name>-internal" to indicate their "internal" nature (not shared with customers).
1. Use [low-context communications](https://en.wikipedia.org/wiki/High-context_and_low-context_cultures) by being explicit in your communications. We are a remote-only company, located all over the world. Provide as much context as possible to avoid confusion. For example, consider introducing yourself and your function when addressing the company during the company call, since not everyone may know who you are. Relatedly, we use [ubiquitous language](#ubiquitous-language) for communication efficiency.

### Top misused terms

Below are terms people frequently use when they should use another term.
The format is: misused term => correct term, reason why.

1. IPO => becoming a public company, because we might do a direct listing instead of an offering.
1. EE => subscribers or paid users, because [EE is a distribution](/handbook/marketing/product-marketing/tiers/#distributions-ce-and-ee) and not all people who use EE pay us.
1. CE => Core users, since [CE is a distribution](/handbook/marketing/product-marketing/tiers/#distributions-ce-and-ee) and many Core users use EE.
1. Hi guys => Hi people, because we want to use [inclusive language](/handbook/general-guidelines/#behavior-and-language)
1. Aggressive => ambitious, since we we want to [attract a diverse set of people](https://www.huffpost.com/entry/textio-unitive-bias-software_n_7493624)
1. Employees => team-members, since we have team-members who are [contractors](/handbook/people-operations/global-compensation/#contract-factor)
1. Resources => people, since we are more than just our output.
1. Community => wider community, since [people at GitLab are part of the community too](/handbook/communication/#writing-style-guidelines) and we should see it as something outside the company.
1. GitLabber => GitLab team-member, since the wider community shouldn't be excluded from being a GitLabber, we [use team-member instead](/company/team/structure/#team-and-team-members).
1. Radical transparency => Transparency, since radical tends to be absolute and we are thoughtful and have [exceptions to transparency](/handbook/general-guidelines/#not-public).
1. Sprint => iteration, since we are in it [for the long-term](/handbook/values/#under-construction) and [sprint implies fast, not quick](https://hackernoon.com/iterations-not-sprints-efab8032174c).
1. Obviously => skip this word, since using things like "obvious/as we all know/clearly" discourages people who don't understand from asking for clarification.

### Asking "is this known"

1. If something is behaving strangely on [https://gitlab.com](https://gitlab.com), it might be a bug.
   It could also mean that something was changed intentionally.
1. Please search if the issue has already [been reported][issue-list].
    1. If it has not been reported, please [file an issue](#issues).
1. If you are unsure whether the behavior you experience is a bug, you may ask in the Slack channel [#is-this-known][is-this-known-slack].
    1. Make sure that no-one has experienced this issue before, by checking the channel for previous messages
    1. If you know which stage of the DevOps lifecycle is affected, it is also okay to ask in #g_{group}, for example [#g_monitor][g_monitor-slack].
    1. Describe the behavior you are experiencing, this makes it searchable and easier to understand.
       Different people might look for different things in the same screenshots.
    1. Asking in a single channel helps discoverability, duplicated efforts and reduces noise in other channels.
       Please refrain from asking in general purpose channels like [#frontend][fe-slack], [#backend][be-slack], [#development][dev-slack] or [#questions][q-slack].

[issue-list]: https://gitlab.com/groups/gitlab-org/-/issues
[is-this-known-slack]: https://gitlab.slack.com/messages/CETG54GQ0/
[g_monitor-slack]: https://gitlab.slack.com/messages/C1ZCCRWBC
[fe-slack]: https://gitlab.slack.com/messages/C0GQHHPGW/
[be-slack]: https://gitlab.slack.com/messages/C8HG8D9MY/
[dev-slack]: https://gitlab.slack.com/messages/C02PF508L/
[q-slack]: https://gitlab.slack.com/messages/C0AR2KW4B/

### Multimodal communication

Employ multimodal communication to broadcast important decisions. To reach our distributed organization, announce important decisions on the company call, email the appropriate team email lists, Slack the appropriate channels, and target 1:1s or other important meetings on the same day, with the same information.

When doing this, create and link to a single source of truth: ideally the [handbook](/handbook/handbook-usage/#why-handbook-first), otherwise an epic, issue, or Google Doc. The email or Slack message should not be the source of truth.

## Email

1. Send one email per subject as multiple items in one email will cause delays (have to respond to everything) or misses (forgot one of the items).
1. Always reply to emails by replying to all, even when no action is needed. This lets the other person know that you received it. A thread is done when there is a single word reply, such as OK, thanks, or done.
1. If you're sending an email to a large group of people (or a distribution list), put those recipients in BCC (rather than in the "To" field) so that a reply all won't ping hundreds of people.
1. If you forward an email without other comments please add FYI (for your information), FYA (for your action), or FYC (for your consideration). If you forward an external request with FYC it just means the person who forwarded it will not follow up on the request and expects you to decide if you should follow up or not, the terms comes from [movie promotion to voters](https://en.wikipedia.org/wiki/For_Your_Consideration_(advertising)).
1. Email forwarding rules are specified in the shared [_Email, Slack, and GitLab Groups and Aliases_](https://docs.google.com/document/d/1rrVgD2QVixuAf07roYws7Z8rUF2gJKrrmTE4Z3vtENo/) Google Doc accessible only to people in the company. If you want to be added or removed from an internal alias, change a rule, or add a forwarding email alias, please [suggest an edit](https://support.google.com/docs/answer/6033474?hl=en) in the doc and [submit a new access request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request).
1. Only G-Suite domain admins are allowed to provision Google Groups and email distributions.
1. Emails are asynchronous, for example, if your manager emails you on a weekend it is fine to reply during the workweek.
1. If an email is or has become **urgent** feel free to ping people via chat referencing the subject of the email.

## Slack

Slack is to be used for informal communication only. Only 90 days of activity will be retained. Accordingly, Slack should specifically NOT be used for:
   * obtaining approvals;
   * documenting decisions;
   * storing official company records or documents; or
   * sharing personal or sensitive information regarding any individuals

### Avoid Private Messages
1. When using Slack for work-related purposes, please avoid private messages. [Private messages discourage collaboration](http://blog.flowdock.com/2014/04/30/beware-of-private-conversations/). You might actually be contacting the wrong person, and they cannot easily redirect you to the right person. If the person is unavailable at the moment, it is less efficient because other people cannot jump in and help. Use a public channel and mention the person or group you want to reach. This ensures it is easy for other people to chime in, involve other people if needed, and learn from whatever is discussed.
1. If someone sends you a work-related private message, it is okay to let them know you'd like to take the conversation to a public channel, linking to this section of the handbook.  The process might look something like:
  * In the private message: `Thanks for reaching out, that's a great question/idea I think the rest of the team could benefit from.  I'm going to move this to #public-channel based on [our desire to avoid private messages](/handbook/communication/#avoid-private-messages)`
  * In the appropriate public channel: `@Person asked "question" in a DM, pulling that out here if anyone else has input.`
  * Answer the question in a thread on that channel message, allowing others to benefit.
1. If you must send a work-related private message, don't start a conversation with "Hi" or "Hey" as that interrupts their work without communicating anything. If you have a quick question, just ask the question directly, and the person will respond asynchronously. If you truly need to have a synchronous communication, then start by asking for that explicitly, while mentioning the subject. e.g., "I'm having trouble understanding issue #x, can we talk about it quickly?".

### Use Public Channels
1. If you use Slack and plan to message 3 or more people, we recommend a channel for customer/issue/project/problem/partnership.
1. Learn about [common channels and channel-naming conventions](/handbook/communication/chat).
1. If something is important but not urgent - like complimenting or encouraging the entire team - use email or post in the channel without `@`-mentioning the team.
1. It's not rude to leave a channel. When you've had your questions answered or are no longer interested, feel free to leave the channel so it won't distract you anymore.
1. The usage of ChatBots for integrations can sometimes depend upon the name of the channel. You should consult the channel about such integrations before changing the name of commonly used/popular channels to avoid inadvertently breaking integrations.


### Be respectful of others' time
1. If you're only referring to someone, but don't actually need their attention, and want to spare them from getting notified, spell out their name normally without `@` mentioning them.
1. Slack messages should be considered asynchronous communication, and you should not expect an instantaneous response; you have no idea what the other person is doing.
1. Because we work globally, you may receive Slack mentions at any time of day. Please consider enabling [Slack's Do not disturb functionality](/handbook/tools-and-tips/#do-not-disturb-hours) so you don't get interrupted, for example, in your offtime.
1. Do not feel obligated to respond to Slack messages when you are not working.
1. Feel free to send a colleague a link to these guidelines if the communication in Slack should be done **asynchronously**.
1. **Please avoid using @here or @channel unless this is about something urgent and important.** In chat, try to keep the use of keywords that mention the whole channel to a minimum. They should only be used for pings that are both urgent and important, not just important. By overusing channel mentions, you make it harder to respond to personal mentions promptly since people get pinged too frequently. Additionally, if you are planning to `@mention` a specific team ([Slack User Group](https://get.slack.help/hc/en-us/articles/212906697-Create-a-user-group#browse-user-groups-and-view-members)), consider the size of the group you are mentioning ([see group membership](https://gitlab.com/gitlab-com/security-tools/report-slack-group-members)) and the impact of pinging all of these people for the particular situation. If something is urgent and important:
   * Use `@here` to notify all currently _active_ members in the room. Please only use `@here` if the message is important _and_ urgent.
   * Use `@channel` to notify _ALL_ members in the room, irrespective of away status. Please only use `@channel` if the message is important _and_ urgent.
1. If you are aware that your teammate is on vacation, avoid mentioning them in a high volume channel. It will be difficult to find the information or question when they return. If you need to ensure they refer back to the thread, ensure to send them a link to the relevant Slack message through a direct message.

### General Guidelines
1. If the subject is of value to the wider community, consider commenting on an existing issue or opening a new [issue](#everything-starts-with-an-issue) instead.
1. Use the `:white_check_mark:` emoji or similar to indicate an inquiry has been answered. Anyone can add the emoji. If you're not sure, then feel free to leave it up to the person who asked. An emoji indicator is particularly helpful in channels where lots of questions are posted, such as `#questions`, and `#git-help`.
1. In general, you can think of emoji reactions as equivalent to body-language responses we use in real-life conversations, such as nodding your head as encouragement when a verbal (or in Slack, written) response might be too much.
1. Unless you're in an active chat, don't break up a topic into multiple messages as each one will result in a notification which can be disruptive. Use [threads](https://get.slack.help/hc/en-us/articles/115000769927-Message-threads) if you want to provide extra info to the question/comment you posted. In channels like `#questions`, threads are valuable for keeping conversations together.
1. If you are having a hard time keeping up with messages, you can update your preferences to have Slack email you all notifications. To change the setting, go to `Preferences > Notifications > When I'm not active on desktop...` and "send me email notifications."
1. If you agree in a message to start a video call (typically by asking "Call?") the person that didn't leave the last comment starts the call. So either respond to the "Call?" request with a video link or say "Yes" and let the other person start it. Don't say "Yes" and start a call 5 seconds later since it is likely you'll both be creating a video call link at the same time.
1. As an admin of the Slack workspace, if given the option to _"Disable future attachments from this website"_ when removing an attachment from a message **this will blacklist the link/domain from [unfurling](/handbook/tools-and-tips/#unfurling-links-in-messages) in the entire Slack workspace**. Be careful and deliberate when choosing this option as it will impact every user in the workspace.
1. When referencing a Slack thread in a GitLab.com issue, don't _only_ link to the thread. Not only will people outside of the GitLab organization be unable to access the content, but the link will die after the Slack retention period expires. Instead:
   1. Copy and paste the relevant parts of the thread into the issue using blockquote formatting.
   1. Link to the Slack thread and include `(internal)` after the link. For example: https://gitlab.slack.com/archives/C0AR2KW4B/p1555347101079800 (internal)
1. When selecting your Slack display name, please do not have your name in all capital letters as this is often [associated as shouting](https://en.wikipedia.org/wiki/All_caps#Association_with_shouting) in written communications.

### Emergency Chat
#### Slack is down
{:.no_toc}
   1. Use the "Slack Down!" group chat on Zoom.
      1. In the Zoom desktop app go to the *Contacts* tab
      1. Click `+`
      1. Click "Join a Channel"
      1. Search "Slack down!"
      1. Click "Join"

#### Slack and Zoom are down
{:.no_toc}
   1. Join the [Slack Down!](https://chat.google.com/preview/room/AAAAGAd_BaQ) room on Hangouts Chat

**IMPORTANT**: Once service is restored, go back to Slack.

## Google Docs

1. Never use a Google Doc / Presentations for something non-confidential that has to end up on the website or the **handbook**. Work on these edits via commits to a merge request. Then link to the merge request or diff to present the change to people. This prevents a duplication of effort and/or an out of date handbook.
1. If you _do_ need a Google Doc, create one with your company G Suite (formerly Google
Apps) account. By default, share it with the whole company using the _Anyone at GitLab can find and access_ link sharing permission
and the _Anyone within GitLab can edit_ access permission (preferred) or the _Anyone within GitLab can comment_ access permission.
Easily do this by creating Google Docs within a Shared Folder in Google Drive.
1. If you have content in a Google Doc that is later moved to the website or handbook, [deprecate the Google Doc](#how-to-deprecate-a-google-doc).
1. When referring to a Google Doc or folder on the Google Drive in the handbook, refrain from directly linking it. Instead, indicate the name of the doc. If you link the URL people from outside the organization can request access, creating workload and the potential for mistakes.
(In the past, linking a Google Doc has led to inadvertently opening the sharing settings beyond what was intended.)
This also helps prevent spam from people outside GitLab requesting access to a doc when clicking its link.
1. If you are having trouble finding a shared Google Doc, make sure you [Search &lt;your domain&gt;](https://support.google.com/a/answer/3187967?hl=en) in Google Drive.
1. In our handbook, if you find yourself wondering whether it is better to provide a public link to a Google Doc vs. writing out the content on the website, use the following guideline: Is this document frequently adapted / customized? If yes, then provide a link, making sure that the document can be _commented on_ by _anyone_ with the link. For instance, this is how we share our employment [contracts](/handbook/contracts/). If the document is rarely customized, then provide the content directly on the site and deprecate the Google Doc.
1. If you want to quickly find where a team member's cursor is in a Google Doc, click their icon at the top of the document and the app will jump you to the current location. This works in Sheets and Presentations as well.
1. You can set the notifications on a Google Doc so you only get emailed with someone tags you directly instead of getting emails for every comment. Click on "notifications" and select "Only yours". By the way, when you create the doc, it defaults to All, but when you are just shared with it, it defaults to Only yours. There is [no global default](https://productforums.google.com/forum/#!msg/docs/1C3PZX1AY7Q/6EXXQKQSPCMJ).
![Google Doc Notifications](./google-docs-notifications.png).
1. You can find a template of the [GitLab letterhead](https://docs.google.com/document/d/1gN1Z2FHPIfPk7QLIey1KF9dR1yTl0R7QrMSb5_Iqfh4/edit) on the shared Google Drive. Please be sure to make a copy of the document to your Google Drive before customizing the template for your use.

### How to deprecate a Google Doc

1. Add 'Deprecated: ' to the start of the title.
1. Remove the content you moved.
1. Add a link to the new location at the beginning of the doc/first slide/first tab.
1. Add a link to the merge request or commit that moved it (if applicable).

## Presentations

1. All presentations are made in Google Slides using [our templates](/handbook/tools-and-tips/#google-slides-templates).
1. Please allow anyone at GitLab to edit the presentation (preferred) or at least comment on the presentation.
1. If the content can be public use File > Publish to the web > Publish to get a URL and paste that in the speaker notes of the first slide (commonly the title slide).
1. The title of every slide should be the message you want the audience to take away, not the subject matter. So use 'Our revenue more than doubled' instead of 'Revenue growth'.
1. At the end of the presentation when you go to Q&A stop presenting in Zoom. This way the other people can see the person who is speaking much better.

## Say Thanks

1. Thank people that did a great job in our "Thanks" Slack channel.
1. If someone is a team member just @ mention them, if multiple people were working on something try @ mentioning each person.
1. If possible please include a link with your thanks that points to the subject matter that you are giving thanks for, for example a link to a merge request.
1. Add emoji reactions to other peoples thank you messages. Use the value emojis when you see a thank you message ties to [GitLab's values](/handbook/values/).
  - `:collaboration-value: :results-value: :efficiency-value: :diversity-value: :iteration-value: :transparency-value:`
  - ![CREDIT emoji](/images/handbook/credit-emoji.png)
1. Please do not mention working outside of working hours, we want to minimize the pressure to do so.
1. Almost [everyone in the company is active in this channel](https://twitter.com/sytses/status/1100071442576633856) so please don't be shy.
1. Don't thank the CEO or other executives for something that the company paid for, thank GitLab instead.
1. To thank someone who is not a team member please mention a Community Advocate, the name of the person, a quirky gift
and link to their work. For example, "@manager, @communityadvocate: Joe deserves a lawnmower for _link_". The Community Advocate will approach the person in question for their address saying we want to send some swag. We'll ship it in gift wrap with "Thanks for your great work on _link_, love
from @gitlab".

## Communicate directly

When working on a problem or issue, communicate directly with the people you need support from rather than working through reporting lines. Direct communication with the people you need to collaborate with is more efficient than working through your manager, their manager, or another intermediary.
Escalate to management if you are not getting the support you need. Remember that everyone is a [manager of one](/handbook/values/#managers-of-one) and they might have to complete their own assignments and inform the reporting lines.

## Not sure where to go?

If there is something that you want to discuss, but you do not feel that it is
a reasonable option to discuss with either your manager or CEO, then you can reach
out to any of the other C-level GitLab team-members or our board member Bruce Armstrong.

## Company Call

1. Company calls happen Monday to Thursday.
1. Every Friday we have an AMA with an executive to talk about anything the people at our company are thinking about.
1. Schedule
   * PST: <span id="main-PST"></span>
   * UTC: <span id="main-UTC"></span>
   * <span id="main-abbr"></span>: <span id="main-USER"></span>
1. APAC schedule
   * PST: <span id="apac-PST"></span>
   * UTC: <span id="apac-UTC"></span>
   * <span id="apac-abbr"></span>: <span id="apac-USER"></span>
1. Everyone at GitLab is invited to the company call.
1. We also have a company call for GitLab team-members in the APAC region, which occurs every Tuesday 14:00 Australia/Sydney time.
   This call will also be recorded so the rest of the company can see what their colleagues have been up to! Everyone is encouraged to join this call as well, but it is not mandatory.
1. We use [Zoom](https://gitlab.zoom.us), (please be sure to mute your microphone). If using the Zoom web app, you can go to settings and check always mute microphone when joining a meeting.
1. The link is in the calendar invite and also listed at the top of the company call agenda Google Doc called _Company Call Agenda_.
1. The call is recorded automatically, and all calls are transferred every hour to a Google Drive folder called "GitLab Videos". There is a subfolder called "GitLab Company Call", which is accessible to all users with a GitLab.com e-mail account.
1. We start on time and do not wait for people.
1. The person who has the first item on the agenda starts the call.
1. We start by discussing the subjects that are on the agenda for today.
   * Everyone is free to add subjects. Please start with your name and be sure to link to an issue, merge request, or commit if it is relevant.
   * When done with a point mention the subject of the next item and hand over to the next person.
   * When someone passes the call to you, no need to say, “Can you hear me?” Just begin talking. If we can’t hear you, we’ll let you know.
1. After the general announcements we do a [breakout call](#breakout-call).
1. It is OK to talk over people or interrupt people to ask questions, cheer for them or show your compassion. This encourages more conversation and feedback in the call. Also see the interruption item in [video calls](#video-calls).
1. Please look if the person you hand over to is present in the participant list so you don't hand over to someone who is not present.
1. The last person on the agenda announces that we are transfering to Breakout calls and mentions the Breakout rooms spreadsheet, which is linked in the company call agenda. Instruct team members to find their name in the document and click the hyperlink of the room number. We encourage everyone to make this announcement fun and ask that they read the discussion question for the day. Please note that nearly every day we have new team members joining the company. Please give the full instructions as there is likely new team members in the call.
1. Even if you cannot join the call, consider reviewing the recorded call or at minimum read through the agenda and the links from there. We often use the company call to make announcements or discuss changes in processes, so make sure to catch up on the news if you have missed a company call (or more).
1. If you are scheduling a meeting, avoid scheduling during the company call so that meeting attendees do not need to choose between your meeting and the company call. As a remote workforce, the company call is an important part of our culture.

### Breakout call

1. We split into small groups of at the end of the [Company Call](#company-call) mostly after 5 minutes into the call.
1. Every call has a theme, but you can talk about anything, as long as it isn't about results at work.
1. [Breakout Groups](https://docs.google.com/spreadsheets/d/1M-6NjK8_gAqcU1BehQVkKuDa1Ua2hTGBy8TuEDXHh3g/edit?usp=sharing)
1. Every two months the groups will be changed:
    - Each group with high attendance will keep half of the attendees while the other half is moved intact and given a new group number.
    - Groups with low attendance will be shuffled out to the groups with higher attendance.
    - We will do our best to ensure that at least half of the team members that frequently attend stay in the same group.
1. The consistent group allows you to build a deeper relation with a group of people all over the company. We had daily random groups before but that didn't work. As someone said since switching to stable groups: "I really like the breakout discussion format with the constant group of attendees we have now. It’s good to get some repeat conversations to get to know each other better. I find the conversation is going faster and is more engaging as we add a little context with the group each time. This is a change for me as I was not getting nearly as much out of the breakouts in the old format."

### Call Structure

These calls are unstructured to allow for natural interaction, but everyone should get a chance to share at least once.

It helps to pause slightly between talking about different topics to allow for discussion.

If you are unsure of who should start or speak next, follow the order listed in Zoom.

Prompts for things you might share:

- Hobbies and interests.
- What you've been up to recently.
- Things you're looking forward to.
- Sports and wellness.
- Cooking, entertaining, and creative projects.
- Travel, kids, family, and pets.
- Music, books, TV & movies, and video/board games.

### Setting up the calls

The calls take place using the People Ops zoom account, which has Breakout Rooms enabled. Make sure to be logged in to the People Ops account to host the calls.

Join the Group Conversation 5 minutes prior to the start time to make sure everything is working properly, and to assist the presenter with any needs. If for some reason the call does not automatically begin recording, press the record button in the bottom right of the Zoom window. Observe over the call and mute any users that are unmuted on accident by clicking the mute button over their image. End the call for all participants at the end of the discussion. If the call runs long, and it is at the 29 minute mark, alert the presenter and end the call.

Next, join the company call. Double check that the call is recording, and that participants can enter the room. If there is any issues in joining the room, double check that the Group Conversation call was ended properly. Once the call begins, make sure to mute any unmuted users that aren't talking, or inform them so they can mute themselves. After the company updates on Tues-Thurs see the following instructions:

##### Company Call Groups

After the announcements have ended, the host will inform the group that they will be heading to their breakout rooms. Inform the group to join their [predesignated groups](https://docs.google.com/spreadsheets/d/1M-6NjK8_gAqcU1BehQVkKuDa1Ua2hTGBy8TuEDXHh3g/edit?usp=sharing) in the chat, and let the team know to inform you (the host) if they are not on the list, and to join another room if they find their room to be empty.

If a team member is not on the listing, inform them to go to another room, and inform the host to add them to the most fitting room.

#### Archiving the Company Call Agenda

The company call agenda document will begin to perform poorly (e.g. slow load times, freezing, restricted write access) as it grows. To curb that, events should be [archived](https://docs.google.com/document/d/1-X_oRGICNiOVUw8asuNP90tg6l6EjRVNSDvXoZAVeR8/edit) each Friday. Events older than one week may be moved to the archive, which follows the same basic structure as the company call agenda document with most recent events appearing towards the top of the doc.

A link to the archive can be found at the top of the company call agenda document.


## Release Retrospectives and Kickoffs
{: #kickoffs}

After GitLab releases a new version on the 22nd of each month, we have a
30-minute call a few days later reflecting on what could have been
better:

1. What went well this month?
1. What went wrong this month?
1. What could we have done better?

We spend the first part of the retrospective meeting reviewing the action
items from the previous month.

On the 8th of each month (or the next business day), we have a kickoff meeting
for the version that will be released in the following month. The product team and other leads will have already had
discussions on what should be prioritized for that release. The purpose of this kickoff is
to get everyone on the same page and to invite comments.

Both the retrospectives and kickoffs are [live streamed](/handbook/communication/youtube/#live-streaming)
(and posted) to the [GitLab YouTube channel](https://www.youtube.com/gitlab).

## Random
{: #random-room}

1. The [#random](https://gitlab.slack.com/archives/random) Slack channel is your go-to place to share random ideas, pictures, articles, and more. It's a great channel to check out when you need a mental break.

## Scheduling Meetings

1. If you want to ask GitLab team-members if they are available for an event please send a calendar invite with Google Calendar with your Google GitLab account to their Google GitLab account. This makes it easier for people to check availability and to put on their calendar. It automatically shows up on calendars even when the email is not opened. It is easier to signal presence and to see the status of everyone. Please respond quickly to invites so people can make plans.
1. Every scheduled meeting should either have a Google Presentation (for example for functional updates that don't require participation) or a Google Doc (for most meetings) linked. If it is a Google Doc it should have an agenda, including any preparation materials (can be a presentation). Put the agenda in a Google Doc that has edits rights for all participants (including people not part of GitLab Inc.). Link the Google Doc from the meeting invite. Take notes of the points and todos during the meeting. Nobody wants to write up a meeting after the fact and this helps to structure the thought process and everyone can contribute. Being able to structure conclusions and follow up actions in realtime makes a video call more effective than an in-person meeting. If it is important enough to schedule a meeting it is important enough to have a Doc linked. If we want to be on the same page we should be looking at that page.
1. If you want to check if a team member is available for an outside meeting, create a calendar appointment and invite the team member only after they respond yes. Then invite outside people.
1. When scheduling a call with multiple people, invite them using a Google Calendar that is your own, or one specific to the people joining, so the calendar item
doesn't unnecessarily appear on other people's calendars.
1. If you want to move a meeting just move the calendar appointment instead of reaching out via other channels. Note the change at the top of the description.
1. Please click 'Guests can modify event' so people can update the time in the calendar instead of having to reach out via other channels. You can configure this to be checked by default under [Event Settings](https://calendar.google.com/calendar/r/settings).)
1. When scheduling a meeting we value people's time and prefer the "speedy meetings" [setting in our Google Calendar](https://calendar.google.com/calendar/r/settings).
   This gives us meetings of, for example, 25 or 50 minutes leaving some time to:
   1. Write notes and reflect
   1. Respond to urgent messages
   1. Take a [bio break](https://www.merriam-webster.com/words-at-play/bio-break-meaning-and-origin)
   1. Stretch your legs
   1. Grab a snack
1. When scheduling a meeting, please try to have it start at :00 (hour) or :30 (mid-hour) to leave common start times available for other meetings on your attendees' calendars. Meetings should be for the time needed, so if you need 15 minutes just book that.
1. When creating a calendar event that will be used company wide, please place it on the GitLab Team Meetings Calendar. That way the event is easily located by all individuals.
1. When you need to cancel a meeting, make sure to delete/decline the meeting and choose the option **Delete & update guests** to make sure everyone knows you can't attend and don't wait for you.
1. If you want to schedule a meeting with a person not on the team please use [Calendly](/handbook/tools-and-tips/#calendly). Use Google Calendar directly if scheduling with a GitLab team-member.

## Indicating Availability

Indicate your availability by updating your own calendar using Google's ["out of office"](https://www.theverge.com/2018/6/27/17510656/google-calendar-out-of-office-option) feature and include the dates you plan to be away in your automated response. Note that this feature will automatically decline any meeting invitations during the time frame you select.

1. Put your planned away time including holidays, vacation, travel time, and other leave in your own calendar. Please see [Communicating your time off](/handbook/paid-time-off#communicating-your-time-off) for more.
1. Set your working hours in your Google Calendar settings.
1. Utilize [PTO Ninja](/handbook/paid-time-off/#pto-ninja) to keep other GitLab team-members aware of your planned time away within Slack.

## Video Calls

1. Use video calls if you find yourself going back and forth in an issue/via email
or over chat. Rule of thumb: if you have gone **back and forth 3 times**, it's time
for a video call.
1. Sometimes it's better to _not_ have a video call. Consider these tradeoffs:
   1. It is difficult (or impossible) to multi-task in a video call.
   1. It may be more efficient to have an async conversation in an issue, depending on the topic.
   1. A video call is limited in time: A conversation in an issue can start or stop at any time, whenever there's interest. It is async.
   1. A video call is limited in people: You can invite anybody into an async conversation at any time in an issue. You don't have to know who are the relevant parties ahead of time. Everyone can contribute at any time. A video call is limited to invited attendees (and those who have accepted).
   1. You can easily "promote" an async conversation from an issue to a video call, as needed. The reverse is harder. So there is lower risk to start with an async conversation.
   1. For a newcomer to the conversation, it's easier and more efficient to parse an issue, than read a video transcript or watch it.
   1. Conversations in issues are easily searchable. Video calls are not.
1. Try to have your video on at all times because it's much more engaging for participants
   1. Don't worry if you can't pay attention at the meeting because you're doing something else, you are the manager of your attention
   1. The flip-side of being the manager of your own attention is that others should not hesitate to request your attention when it is needed
   1. It's okay to eat on video if you're hungry (please turn your mic off)
   1. However you are groomed when you meet a family member or close friend is fine for GitLab internal meetings (including if you just got back from a run)
   1. Having pets, children, significant others, friends, and family visible during video chats is encouraged. If they are human, ask them to wave at your remote team member to say "Hi".
   1. Do not feel forced to have your video on, use your best judgement
1. Additional points for video calls with customers or partners
   1. Results come first. Your appearance, location and background is less important than making customers successful so don't wait for the perfect time / place when you can engage a customer right away.
   1. Communicating that GitLab is an enterprise grade product and service provider is supported by the way you present yourself. Most of the time, if you would not wear something or present yourself in a certain way at a customer's office, candidate interview, or partner meeting in person then it's probably not the right choice on a video call with them either.
   1. Green screens are a great background solution. It's great to work in your garage or basement! Just get a green screen behind you and put up a professional background image to present well externally and still use the rest of the room how you want!
1. We prefer [Zoom](https://gitlab.zoom.us/).
1. Google Calendar also has a [Zoom plugin](https://chrome.google.com/webstore/detail/zoom-scheduler/kgjfgplpablkjnlkjmjdecgdpfankdle?hl=en-US) where you can easily add a Zoom link for a video call to the invite
1. For meetings that are scheduled with Zoom:
   1. Make sure to take out the Google Hangouts link to avoid confusion.
   1. If you need more privileges on Zoom (longer meeting times, more people in the meeting, etc.), please contact People Operations Specialist as described [specifically for Zoom](/handbook/tools-and-tips/#sts=Zoom).
   1. Note that if you select to record meetings to the cloud (setting within Zoom), they will be automatically placed in the GitLab Videos folder in Google Drive on an hourly basis via a [scheduled pipeline](https://gitlab.com/gitlab-com/zoom-sync/pipelines). You can find these videos in Google Drive by entering in the search bar: `title:"GitLab Videos" source:domain`. The [script for syncing the files is here](https://gitlab.com/gitlab-com/zoom-sync).
   1. Note also that after a meeting ends, Zoom may take some time to process the recording before it is actually available. The sync to Google Drive happens on the hour mark, so if the recording is not available, it may take another hour to be transferred.
1. As a remote company we are always striving to have the highest fidelity, collaborative conversations. Use of a headset with a microphone, is strongly suggested.
   1. If other people are using headphones then no-headphones works fine. But if multiple people aren't using headphones you get distractions.
   1. Reasons to use headphones:
      * Computer speakers can cause an echo and accentuate background noise.
      * Using headphones decreases the likelihood of talking over one another, enabling a more lively conversation.
      * Better sound quality, avoiding dynamic volume suppression due to echo cancellation.
   1. Leave the no headphones to:
      * People who don't have them handy at that time
      * People from outside the company
   1. Suggested headphone models can be found in the handbook under [spending company money](/handbook/spending-company-money/equipment-examples/#headphones-and-earbuds).
   1. If you want to use your [Bose headphones](https://www.bose.com/en_us/products/headphones/noise_cancelling_headphones.html) that is fine but please ensure the microphone is active.

1. Consider using a utility to easily mute/unmute yourself, see [Shush](/handbook/tools-and-tips/#shush) in the tools section.
1. [Hybrid calls are horrible](#hybrid-calls-are-horrible)
1. Please speak up asap when someone on the call hasn't muted their mic to avoid disturbances to the person speaking.
1. We start on time and do not wait for people. People are expected to join no later than the scheduled minute of the meeting (before :01 if it is scheduled for :00). The question 'is everyone here' is not needed.
1. It feels rude in video calls to interrupt people. This is because the latency causes you to talk over the speaker for longer than during an in-person meeting. We should not be discouraged by this, the questions and context provided by interruptions are valuable.
This is a situation where we have to do something counter-intuitive to make all-remote meetings work. In GitLab, everyone is encouraged to interrupt the speaker in a video call to ask a question or offer context. We want everyone to contribute instead of a monologue.
Just like in-person meetings be cognizant of when, who, and how you interrupt, we don't want [manterrupting](http://time.com/3666135/sheryl-sandberg-talking-while-female-manterruptions/).
1. We end on the scheduled time. It might feel rude to end a meeting, but you're actually allowing all attendees to be on time for their next meeting.
1. Do not use the chat of products like Zoom to communicate during the call, use the linked document instead. This allows everyone to contribute additional questions, answers, and links in the relevant place. It also makes it easier for people in conflicting timezones to contribute questions before the call and makes it easier to review questions and answers after the call, which can be before watching the recording.
1. Every comment is document worthy, even small support comments such as `+1` or `Very Cool!`.
2. We encourage the recording and [sharing of everything to our YouTube channel](/handbook/communication/youtube/#post-everything)
1. It is unusual to smoke in an open office or video conference, vaping is associated with this. For this reason we ask that you don't vape during calls, and if you absolutely have to, kindly switch your camera off.

## Hybrid calls are horrible

In calls that have remote participants everyone should use have their own equipment (camera, headset, screen).

When multiple people share equipment the following **problems arise for remote participants**:

1. Can't hear the sharing people well.
1. Background noise since the microphone of the sharing people on all the time.
1. Can't clearly see facial expressions since each face takes up only a small part of the screen.
1. Can't easily see who is talking since the screen shows multiple people.
1. Hard getting a word in since their delay is longer than for the sharing people.

The **people sharing equipment also have problems** because they don't have their own equipment:

1. Can't easily screen share something themselves.
1. Trouble seeing details in screen sharing since the screen is further away from them.
1. Can't scroll through a slide deck at their own pace.
1. Sharing people can't easily participate (view or type) in a shared document with the agenda and meeting notes.

The disadvantages for remote people are much greater than for the sharing people and hard to notice for the sharing people.
The disadvantages cause previously remote participants to travel to the meeting to be in person for a better experience.
The extra travel is inefficient since it is time consuming, expensive, bad for the environment, and unhealthy.

Theoretically you can have multiple people in a room with their own equipment but in practise it is much better to be in separate rooms:

1. It is annoying to first hear someone talk in the room and then hear it over audio with a delay.
1. It is hard to consistently mute yourself when not talking to prevent someone else's voice coming through your microphone as well.

## Google Calendar

We recommend you set your Google Calendar access permissions to 'Make available for GitLab - See all event details'. Consider marking the following appointments as 'Private':

- Personal appointments.
- Confidential & sensitive meetings with third-parties outside of GitLab.
- 1-1 performance or evaluation meetings.
- Meetings on organizational changes.

There are several benefits and reasons to sharing your calendar with everyone at GitLab:

1. Transparency is one of our values and sharing what you work on is in line with our message of "be open about as many things as possible".
1. Due to our timezone differences, there are small windows of time where our availabilities overlap. If other members need to schedule a new meeting, seeing the details of recurring meetings (such as 1-1s) will allow for more flexibility in scheduling without needing to wait for a confirmation from the team member. This speaks to our value to be more efficient.

![Google Calendar - make calendar available setting](/images/handbook/tools-and-tips/google-calendar-share.png)

If you add blocks of time spent on recurring tasks to your Google Calendar to remind yourself to do things (e.g. "Check Google Analytics"), consider marking yourself "Free" for those events so that coworkers know they may schedule a meeting during that time if they can't find another convenient time.

## User Communication Guidelines

1. Keep conversations positive, friendly, real, and productive while adding value.
1. If you make a mistake, admit it. Be upfront and be quick with your correction. If you're posting to a blog, you may choose to modify an earlier post. Just make it clear that you have done so.
1. There can be a fine line between healthy debate and incendiary reaction. Try to frame what you write to invite differing points of view without inflaming others. You don’t need to respond to every criticism or barb. Be careful and considerate.
1. [Assume positive intent](/handbook/values/#assume-positive-intent) and explicitly state the strongest plausible interpretation of what someone says before your respond, not a weaker one that's easier to criticize. [Rapoport's Rules](https://rationalwiki.org/wiki/Rapoport%27s_Rules) also implores you to list points of agreement and mention anything you learned.
1. Answer questions, thank people even if it’s just a few words. Make it a two way conversation.
1. Appreciate suggestions and feedback.
1. Don't make promises that you can't keep.
1. Guide users who ask for help or give a suggestion and share links. [Improving Open Development for Everyone](/2015/12/16/improving-open-development-for-everyone/), [Types of requests](/2014/12/08/explaining-gitlab-bugs/).
1. When facing negative comment, respond patiently and treat every user as an individual, people with the strongest opinions can turn into [the strongest supporters](/2015/05/20/gitlab-gitorious-free-software/).
1. Adhere to the [Code of Conduct](/community/contribute/code-of-conduct/) in all communication. Similarly, expect users to adhere to the same code when communicating with the GitLab team and the rest of the GitLab community. No one should accept being mistreated.

## Writing Style Guidelines

1. {: #american-english} At GitLab, we use American English as the standard written language.
1. Do not use rich text, it makes it hard to copy/paste. Use [Markdown](/handbook/product/technical-writing/markdown-guide) to format text that is stored in a Git repository. In Google Docs use "Normal text" using the style/heading/formatting dropdown and paste without formatting.
1. Don't use ALL CAPS because it [feels like shouting](http://netiquette.wikia.com/wiki/Rule_number_2_-_Do_not_use_all_caps).
1. We use Unix style (lf) line endings, not Windows style (crlf), please ensure `*.md text eol=lf` is set in the repository's `.gitattributes` and run `git config --global core.autocrlf input` on your client.
1. Always write paragraphs on a single line. Use soft breaks ("word wrap") for readability. Don't put in a hard return at a certain character limit (e.g. 80 characters) and don't set your IDE to automatically insert hard breaks. MRs for the blog and handbook are very difficult to edit when hard breaks are inserted. 
1. Do not create links like "here" or "click here". All links should have relevant anchor text that describes what they link to, such as: "GitLab CI source installation documentation". Using [meaningful links](https://www.futurehosting.com/blog/links-should-have-meaningful-anchor-text-heres-why/){:rel="nofollow noindex"} is important to both search engine crawlers (SEO) and people with accessibility issues.
This guidance should be followed in all places links are provided, whether in the handbook, website, GoogleDocs, or any other content.
Avoid writing GoogleDocs content which states - `Zoom Link [Link]`.
Rather, paste the full link directly following the word `Zoom`.
This makes the link more prominent and makes it easier to follow while viewing the document.
1. Always use [ISO dates](https://en.wikipedia.org/wiki/ISO_8601#Calendar_dates) in all writing and legal documents since other formats [lead to online confusion](http://xkcd.com/1179/). Use `yyyy-mm-dd`, for example 2015-04-13, and never 04-13-2015, 13-04-2015, 2015/04/13, nor April 13, 2015. Even if you use an unambiguous alternative format it is still harder to search for a date, sort on a date, and for other team members to know we use the ISO standard. For months use `yyyy-mm`, so 2018-01 for January. Refer to a year with CY18 (never with 2018) and a quarter with CY18-Q1 to prevent confusion with fiscal years and quarters.
1. When referring to our quarterly and annual cadence, remember that our Fiscal Year runs from February 1 to January 31. When referring to a fiscal year or quarter, please use the following abbreviations:
   1. "FY20" is the preferred format and means: Fiscal Year 2020, the period running from February 1, 2019 through January 31, 2020
   1. "Q1" = the first quarter of the current Fiscal Year, so on Feb 1, 2020, "Q1" is the period from Feb. 1, 2020 through April 30, 2020. Note that Epics in GitLab follow Calendar Years and Quarters.
   1. When referring to a quarter in a future or past year, combine the two above: "FY21-Q1"
1. Remember that not everyone is working in the same timezone; what may be morning for you is evening for someone else. Try to say 3 hours ago or 4 hours from now, or use a timestamp, including a timezone reference.
1. We use UTC as the timezone for engineering (for example production postmortems) and all cross-functional activities related to the monthly release. We use Pacific Time (PT) for all other uses since we are a San Francisco-based company. Please refer to time as "9:00 Pacific" or `9:00 PT`. It isn't often necessary to specify whether a timezone is currently observing Daylight Saving Time, and such references are often incorrect, so prefer "PT" to "PDT" or "PST" unless you have a specific need to differentiate between PDT and PST.
1. Although we're a San Francisco based company we're also an internationally diverse one. Please do not refer to team-members outside the US as international, instead use non-US. Please also avoid the use of offshore/overseas to refer to non-American continents.
1. If you have multiple points in a comment or email, please number them. Numbered lists are easier to reference during a discussion over bulleted lists.
1. When you reference an issue, merge request, comment, commit, page, doc, etc. and you have the URL available please paste that in.
1. In making URLs, always prefer hyphens to underscores, and always use lowercase.
1. The community includes users, contributors, core team members, customers, people working for GitLab Inc., and friends of GitLab. If you want to refer to "people not working for GitLab Inc." just say that and don't use the word community. If you want to refer to people working for GitLab Inc. you can also use "the GitLab Inc. team" but don't use the "GitLab Inc. employees".
1. When we refer to the GitLab community excluding GitLab team-members please say "wider community" instead of "community".
1. All people working for GitLab (the company) are the [GitLab team](/company/team). We also have the [Core team](/core-team) that consists of volunteers.
1. Please always refer to GitLab Inc. people as GitLab team-members, not employees.
1. Use [inclusive and gender-neutral language](https://techwhirl.com/gender-neutral-technical-writing/) in all writing.
1. Always write "GitLab" with "G" and "L" capitalized, even when writing "GitLab.com", except within URLs. When "gitlab.com" is part of a URL it should be lowercase.
1. Always capitalize the names of GitLab [features](/features).
1. Do not use a hyphen when writing the term "open source" except where doing so eliminates ambiguity or clumsiness.
1. Monetary amounts shouldn't have one digit, so prefer $19.90 to $19.9.
1. If an email needs a response, write the answer at the top of it.
1. Use the future version of words, just like we don't write internet with a capital letter anymore. We write frontend and webhook without a hyphen or space.
1. Our homepage is https://about.gitlab.com/ (with the `about.` and with `https`).
1. Try to use the [active voice](https://writing.wisc.edu/Handbook/CCS_activevoice.html) whenever possible.
1. Refer to environments that are installed and run "on-premises" by the end-user as "self-managed."
1. If you use headers, properly format them (`##` in Markdown, "Heading 2" in Google Docs); start at the second header level because header level 1 is for titles. Do not end headers with a colon.
1. Always use a [serial comma](https://en.wikipedia.org/wiki/Serial_comma) (a.k.a. an "Oxford comma") before the coordinating conjunction in a list of three, four, or more items.
1. Always use a single space between sentences rather than two.
1. Read our [Documentation Styleguide](https://docs.gitlab.com/ee/development/doc_styleguide.html) for more information when writing documentation.
1. Do not use acronyms when you can avoid it as you can't assume people know what you are talking about. Example: instead of `MR`, write `merge request`.
1. We segment our customers/prospects into 4 segments [Strategic, Large, Mid-Market, and Small Medium Business (SMB)](/handbook/business-ops/resources/#segmentation).

## Visuals
Many times an explanation can be aided by a visual. 
Whenever presenting a diagram, we should still allow everyone to contribute. 
Where possible, take advantage of the handbook's support for [Mermaid](https://docs.gitlab.com/ee/user/markdown.html#mermaid).
Where taking advantage of Mermaid isn't possible, link to the original in our Google Drive so that the diagram can be edited by anyone. 

## Situation-Complication-Implication-Position-Action-Benefit (SCIPAB)

Mandel Communications refers to SCIPAB at the "surefire, six-step method for starting any conversation or presentation." When you only have a few minutes to present your case or grab your listener's attention, this six-step process can help you communicate better and faster.

* Situation - Expresses the current state for discussion.
* Complication - Summarizes the critical issues, challenges, or opportunities.
* Implication - Provides insight into the consequences that will be a result of if the Complications are not addressed.
* Position - Notes the presenter's opinion on the necessary changes which should be made.
* Action - Defines the expectations of the target audience/listeners.
* Benefit - Clearly concludes how the Position and Action sections will address the Complications.
This method can be used in presentations, emails, and everyday conversations.

Example - The Management team asking for time to resolve a problem

* S - The failure rate last year for product X1 was an acceptable 1.5%.
* C - Because of supply shortages in the current fiscal year we are forced to change the material of a key component.
* I - Unfortunately, that resulted in the failure rate doubling this year.
* P - It is critical we address this problem immediately.
* A - Please approve the team 5 days to investigate the specific causes of the increase and establish the necessary next steps.
* B - By doing this we will reduce the failure rate to an acceptable level and develop guidelines for preventing such problems in the future.

More information can be found at [SCIPAB- Six Steps To Reach Your Audience](https://dzone.com/articles/scipab-six-steps-to-reach-your-audience).

## Beamy Guidelines

Beamy is our company conference call robot. It lives at the San Francisco Mission Control room.
Its main purpose is to give those outside of Mission Control a view into the space and people.
Practically speaking, in our case it serves two primary functions - helping prove that there
really is no central headquarters office full of employees on a regular basis, and secondly,
to show off to your kids, as a remote equivalent to bringing your kids in to work...

When you call in to the beam your face will appear on the screen and audio interaction will be
possible (so make sure your webcam and microphone are working first) and you can drive the
robot around the boardroom. It simulates being in the space without physically being there.
It is really cool and everyone should try it and have fun with it.

* You should have received an invite during onboarding to connect and to download a desktop and/or mobile client. If you did not receive the invite, please request one in the Slack #peopleops channel.
* **Beamy times**: 8am until 6pm Pacific time. [Please check the current time!](https://time.is/San_Francisco) on workdays and during all company events, for other times please @mention Sid in the Slack #ceo channel to see if it is OK.
* Your account should automatically log right in and give you immediate access to the Beamy. If attempting to beam in results in a long wait and timeout, please post in the #peopleops channel, as it is possible that our account is misconfigured and requires an answer on the other end.
* The email invite will come from "Suitable Tech". Once you are sent an invite you can beam in at any time and drive around. Don’t forget to park it back on the charger when you are done. You can do so by driving up to the charger, when you see a green outline press AND HOLD 'p' until it's parked. Make sure it is charging, otherwise try again.
* If you don't use headphones be careful about your volume and microphone placement, it might start singing, if so immediately mute your microphone and switch to headphones.
* Beamy lives at GitLab's Mission Control. If there is a meeting going on at Mission Control (which you were not invited to) when you are testing out Beamy, that is great! Do not feel like you are interrupting the meeting. It actually adds to the "cool" factor that we are an all-remote company and can interact with people from all over the world at any given time. Introduce yourself and say "Hi" to the room and move on with your tour of Beamy. Be sure to place it back on it's charging station when you're done.
* More info about Beam can be found at [Suitable Tech](https://suitabletech.com).
* Please report any questions or issues you have about the beam in the Slack #peopleops channel.

## Company phone number
{: #phone-number}

If you need to provide the details of GitLab's contact information you can take the [address from the visiting page](/company/visiting/) for reference; or the [mailing address](/handbook/people-operations/#addresses) of the office in the Netherlands if that is more applicable.

If a phone number is required, leave this field empty by default. If that is not possible, then use
the general number (+1-415-761-1791), but be aware that this number simply guides to a voice message that refers the caller back to contacting us via email.

## Organization code names

* Listed in Google Sheet under 'Organization code names'
* To make it easier to recognize code names we base them on classic car models.

There are two types of code names:

* **Don't mention publicly** We can use code names anywhere we regularly share items in that medium with people outside the company: issue trackers, Group Conversations, etc. For these we don't have to use code names in things that are never published: SalesForce, Zuora, Zendesk, and verbal conversations.
* **Don't write down** there are organizations that we shouldn't write down anywhere.

## Ubiquitous language

At GitLab we use [ubiquitous language](https://martinfowler.com/bliki/UbiquitousLanguage.html) to increase communication efficiency.
This is defined in [Domain-driven design](https://en.wikipedia.org/wiki/Domain-driven_design) as: "A language structured around the domain model and used by all team members to connect all the activities of the team with the software.".
We use it for activities in GitLab, even ones not implemented in software.
By having ubiquitous words to identify concepts we prevent confusion over what is meant, for example we refer to [parts of our organization](/company/team/structure/) as a function, department, or group depending on exactly what is meant.
Make sure that domains don't overlap, for example [organization size](/handbook/sales/#organization-size) and [deal size](/handbook/sales/#deal-sizes) don't reuse words to prevent overlap.
If a term is ambiguous don't use it, for example our [hiring definitions](/handbook/hiring/#definitions) have roles and vacancies but avoid the ambiguous word job.
Make sure that people can infer as much as possible from the word, for example our [subscription options](/handbook/marketing/product-marketing/tiers/) allow you to know if someone if using self-managed or GitLab.com.
Make sure terms don't overlap without clearly defining how and why, for example see our [tier definitions](/handbook/marketing/product-marketing/#definitions).
Keep terms to one or at most two words to prevent people from introducing ambiguity by shortening a term. When using two words make the first word unique because people tend to drop the second word more often.

## MECEFU terms

MECEFU is an acronym for Mutually Exclusive Collectively Exhaustive Few words Ubiquitous-language.

You pronounce it: MessiFu. Think of the great soccer player Lionel Messi and his [kung fu](https://en.wikipedia.org/wiki/Chinese_martial_arts) or soccer fu skills.

We want to use MECEFU terms to describe a domain to ensure efficient communication. MECEFU terms have 4 characteristics that help with efficiency:

1. Mutually Exclusive: nothing is referenced by more than one term
1. Collectively Exhaustive: everything is covered by one of the terms
1. Few words: the longer terms are the more likely it is people will not use all of them and cause confusion, avoid acronyms because they are hard to remember (we're open to a few words to replace MECEFU as an acronyms :)
1. Ubiquitous language: [defined above](#ubiquitous-language)

An example of a MECEFU term is our [sales segmentation](/handbook/business-ops/resources/#segmentation):

1. Mutually Exclusive: There is no overlap between the numbers and there is a single dimension.
1. Collectively Exhaustive: Everything for 0 to infinite employees is covered.
1. Few words: Mid-market is a natural combination and SMB is abbreviated.
1. Ubiquitous language: We're not using the word 'Enterprise' which already can refer to our Enterprise Edition distribution.

One nit-pick is that the Medium of SMB and Mid of Mid-Market sound very similar.

## YouTube

See the [YouTube page](/handbook/communication/youtube).

## Knowledge Sharing

As GitLab continues to grow, sharing knowledge across the community becomes even more important. 
The [Knowledge Sharing page](/handbook/communication/knowledge-sharing) describes initiatives we are trying to encourage this. 
This aligns with how we work since everything at GitLab is [public by default](/handbook/values/#public-by-default). 
