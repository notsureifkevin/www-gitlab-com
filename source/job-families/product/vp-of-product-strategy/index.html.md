---
layout: job_family_page
title: "VP of Product Strategy"
---

The VP of Product Strategy reports to our CEO. The key focus of this role is on product vision and strategy, with a [timescale of about 4 years](https://about.gitlab.com/handbook/ceo/cadence/#strategy). Also to identify and facilitate acquisitions, research prototypes, and provide essential product marketing.

## Responsibilities

- Own the product vision and strategy.
- Contribute to product planning.
- Identify acquisition areas and targets, including a technical integration vision.
- Produce research prototypes in far-future areas.
- Provide essential product marketing, especially to convey our vision and strategy.
- Talk to customers regularly and provide that connection to the rest of the company.
- Be passionate about and use the product management features within GitLab like epics and portfolio management.

## Requirements

- Have a strong, articulate, and correct vision for the future of GitLab and current shortcomings.
- Enterprise software experience.
- Developer tool or platform industry experience.
- SaaS and self-managed (on-premises) experience.
- Experience with hyper-growth, more than 25% QoQ.
- You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).
- [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#s-group)

## Links

- [Product Handbook](/handbook/product)
- [Engineering Workflow](/handbook/engineering/workflow)
