---
layout: job_family_page
title: "Chief Financial Officer"
---

## Responsibilities

- Financial reporting to Board, Investors and Bank
- Forecasting and financial analysis to deliver predictable and repeatable business model
- Work collaboratively with CEO to develop and execute financing plan
- Develop, define and ensure validity of key operating metrics
- Create and execute international tax strategy and compliance in all jurisdictions
- Legal affairs of GitLab including sales, vendor and corporate issues
- Financial, legal and operational assessment, diligence and negotiation of potential M&A transactions
- Oversee accounting and billing system that can scale with company growth
- Accurate, timely, detailed, and easy-to-access metrics on sales, sales enablement, customer success and adoption, peopleops, marketing, engineering, product, and finance.
- Must successfully complete a [background check](/handbook/people-operations/code-of-conduct/#background-checks).
- [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#e-group)

## Performance Indicators
- [Plan vs Actual](/handbook/finance/financial-planning-and-analysis/#plan-vs-actual)
- [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor)
- [Runway](/finance/accounting/#cash-burn-average-cash-burn-and-runway)
- Effective Tax Rate 

