---
layout: job_family_page
title: "Sales Development Representative"
---
## Job Description
If you love talking about tech, learning about the latest and greatest, and want to continue to grow your career in software sales, GitLab's Sales Development Representative (SDR) role could be a role worth exploring for you. In addition to having an affinity for tech and experience in sales, we want to build a team that thrives on asking questions, addressing challenges, and nurturing customers.
A successful SDR will take initiative to meet sales goals, qualify leads, build positive and lasting relationships with prospective clients, and support future and existing clients in their adoption of GitLab. Most importantly, our marketing team, including SDRs will use GitLab itself to document and update sales best practices and the company [handbook](/handbook).
As part of our marketing organization, the SDR will report to the Territory SDR Manager, and will work very closely with both Strategic Account Leaders (SAL) and Regional Directors (RDs).

### Requirements
* Twelve (12) months minimum experience in sales or client success in a software/tech company
* Excellent spoken and written English
* Experience in sales, marketing, or customer service
* Experience with CRM and/or marketing automation software
* An understanding of B2B software sales, open source software, and the developer product space
* Affinity for software and knowledge of the software development process
* Ability to carefully listen
* Desire for a continued career in software sales
* Passionate about technology and strong desire to learn
* Be ready to learn how to use GitLab and Git
* You share our [values](/handbook/values/), and work in accordance with those values
* If in EMEA, fluency in spoken and written German and/or other European languages is preferred
* If in APAC, fluency in Mandarin or literacy in Simplified Chinese is required
* If in LATAM, fluency in Portuguese and Spanish is required

### Responsibilities
* Meet or exceed Sales Development Representative (SDR) sourced Sales Accepted Opportunity (SAO) volume targets.
* Identify and create new qualified opportunities from inbound [MQLs](/handbook/business-ops/resources/#mql-definition) 
* Be able to identify where a prospect is in the sales and marketing funnel and nurture appropriately 
* Participate in documenting all processes in the handbook and update as needed with our business development manager
* Be accountable for your lead data and prospecting activities, log everything, take notes early and often
* Enhance prospective customer GitLab trial experiences through tailored and timely outreach
* Engage in conversations with prospective GitLab customers via live chat 
* Speed to lead; maintain [one-day SLA](/handbook/business-ops/resources/#contact-requests) for all contact requests
* Discretion to qualify and disqualify leads when appropriate
* Develop and execute nurture sequences
* Work with designated strategic account leader to identify and prioritize key accounts to develop
* Working with the sales team to identify net-new opportunities and nurture existing opportunities within target accounts.
* Work in collaboration with field and corporate marketing to drive attendance at marketing events
* Attend field marketing events to engage with participants identify opportunities, and to schedule meetings
* Act as a mentor for new SDR hires in helping them navigate their key accounts.
* Work in collaboration with Digital Marketing to develop targeted marketing tactics against your assigned target accounts.

## Levels  

### SDR 2
After attaining an SDR 2 role, you can expect an increase in your compensation and your pipeline generating targets. You will also discuss your ideal career path with your manager and set action steps to achieve it. Topics of discussion should consist of functionalities of interest and steps to be taken by both SDR and Manager to get there. For a minimum of 6 months in the SDR 2 role, you will continue improving your sales skills while also aiding in the development of these skills with other members of your teams through training and coaching. Upon reaching target goals and acquiring certification on these skills, the next step is an SDR 3 role. All the while the SDR and Manager should have clear conversations and setting milestones around career progression at GitLab.

#### Requirements
* 1-2 years proven SDR experience (Either at GitLab or a past role)
* Consistently hits/exceed quota
* Will present at an SDR Quarterly Business Review (QBR) or all hands
* Networked at local DevOps events (non-GitLab)
* Understands, executes and presents an Account Based Marketing (ABM) approach

#### Responsibilities
* Attends GitLab sponsored events
* Participates and owns projects benefiting the SDR organization
* Assists with campaigns - lead pulls and feedback
* Attend field marketing events to engage with participants identify opportunities, and to schedule meetings
* Work in collaboration with Digital Marketing to develop targeted marketing tactics against your assigned target accounts

### SDR 3
After attaining an SDR 3 role, you can expect an increase in compensation and a reduction in your Sales targets as the SDR 3 role is a team coach and mentor for more junior members of the team. You will receive substantial Sales training to expand your current knowledge into pricing conversations and negotiations. You will be expected to pass this knowledge along to your team and set them up for success in their individual performance. Upon reaching target goals and acquiring certification on these skills, the next career step is either an SDR Management role or a career branch into another function in GitLab. The next step should be driven by the SDR and Manager together.

#### Requirements
* Six (6) months in an SDR 2 role at GitLab with successful quota attainment
* Demonstrates thought leadership and training that benefits the SDR team
* Demonstrates exemplary account strategy/process acumen
* GitLab leadership recommends for promotion
* Demonstrates dedication to continuous improvement by investing time to read, study, and share learnings from books, Udemy classes, Toastmasters, Certifications, Approved Sales Training, CEO Shadow Program, etc.

#### Responsibilities
* Leads territory team role play calls
* Regularly performs call coaching evaluations for SDR team members
* Helps craft Outreach sequences for events/campaigns and outbound. Conduct A/B Testing of sequences to determine effectiveness and share best practices with team.
* Assists new hires as an onboarding buddy and acts as a mentor for new SDR hires in helping them navigate their key accounts.

### Team Lead
The SDR Team Lead is a step in the career path geared towards SDRs getting exposed to what it’s like to manage a team at GitLab. You’ll need to have stellar organizational and time management skills because you will be taking point on various SDR leadership tasks such as: on-boarding new employees, managing event followup, interviewing, etc. all while continuing to exceed your quota targets. The SDR Team Lead will also be included in some management level meetings. This concept is to help foster those who have expressed interest in leading a team and progressing to management within GitLab.

#### Requirements
* 18 months of SDR experience (six (6) months at GitLab)
* Proficient in the use of Command of Message (CoM) framework and SDR tools
* In-depth product knowledge
* Stellar time management and organizational skills
* Is seen as a leader/coach by peers as well as the SDR leadership team

#### Responsibilities
* Will be assigned as a Dedicated Onboarding Buddy
* Assists in interviewing process
* Leads team role play and SDR weekly team calls
* Covers when SDR's are out of office (OOO)

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates receive a short questionnaire from our Global Recruiters
* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the SDR Manager for the region they have applied for
* Candidates will complete a writing assessment where they will choose between two companies, decide which personas to target, and create sample introductory emails.
* Candidates will perform a mock role play call with either the SDR Manager or Team Lead. 
* Should the candidiates move forward after the mock role play, they will then be invited to schedule an interview with our Regional Sr. Manager or Director of Revenue Marketing
* Successful candidates will subsequently be made an offer

Additional details about our process can be found on our [hiring page](/handbook/hiring).
