---
layout: job_family_page
title: “Account Based Marketing Manager”
---
# **Account Based Marketing Manager**

Account Based Marketing Manager is responsible for the strategy and implementation of effective Account Based Marketing programs that meet the growth objectives of the business, with a strong emphasis on driving pipeline generation exclusively from a top target account list, and accelerating existing opportunities. The role will also drive strong partnership with key stakeholders within the Marketing organization. The Account Based Marketing Manager, reports to the Manager, Field Marketing, NAM.

## **Responsibilities**

*   Define Account Based Marketing strategy and execution planning, encompassing our multiple ABM tools. 
*   Owning and managing our Account Based Marketing platform, DemandBase. 
*   Partner with the regional Field Marketing Managers to develop regional, account focused playbooks including, but not limited to, outbound prospecting, inbound lead follow up, nurture, channel activities, live/online events, meeting setting with the goal of building pipeline and accelerating bookings.
*   Collaborate and partner with core marketing functions on campaign concept, custom content requirements, digital marketing support/message testing and creative development to ensure effectiveness for regional named accounts.
*   Identify the most effective marketing messages, value proposition, materials, channels, and calls to action for personas and key accounts in the regions.
*   Build then obsess over ABM metrics to ensure business impact measured by named account pipeline/bookings (sourced/influenced), database growth and accuracy for named accounts, account/person engagement scores and sales/partner utilization of programs.
*   Manage project timelines, quality issues, resources, &  budget.

## **Requirements**

*   5+ years of proven multi-channel B2B marketing experience within enterprise software/SaaS.
*   DemandBase experience is a plus. 
*   You are a proactive self-starter, demonstrating high initiative and critical thinking. Proven ability to think strategically, but with exceptional attention to detail in execution.
*   Positive, can-do attitude, with the ability to work with marketing and sales professionals, and ability to prioritize numerous projects simultaneously.
*   Experience building and delivering of field marketing campaigns, events, executive and key account programs and partner activity.
*   Understanding of pipeline management and metrics.
*   Demonstrated experience sourcing and overseeing vendors for marketing program execution.
*   Excellent verbal and written communication skills. Strong interpersonal skills a must. Strong influencing and relationship-building skills.
*   Team player with ability to work independently and autonomously.
*   Flexibility to adjust to the dynamic nature of a startup.
*   Occasional domestic travel to support events - less than 30%.

## **Hiring Process**

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/company/team/). Additional details about our process can be found on our [hiring page](/handbook/hiring).

*   Selected candidates will be invited to schedule a screening call with one of our Global Recruiters. 
*   Next, candidates will be invited to schedule a first-round interview with the hiring manager - the Manager, Field Marketing North America.
*   Candidates will then be invited to schedule a series of 45 minute interviews with our Manager, Digital Marketing Programs and the Sr. Manager of NORAM Sales Development. 
*   Candidates will then be required to submit a 30/60/90 day plan to the recruiter for the hiring manager to review. Upon review, the candidate may be asked additional questions about the plan. 
*   Candidates will then be invited to schedule an interview with our Sr. Director of Revenue Marketing. 
*   Our CMO may choose to conduct an interview.
*   Finally, our CEO may choose to conduct a final interview.
*    Successful candidates will subsequently be made an offer via email.

