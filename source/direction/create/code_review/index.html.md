---
layout: markdown_page
title: "Category Strategy - Code Review"
---

- TOC
{:toc}

## Code Review

| Stage | Maturity |
| --- | --- |
| [Create](/direction/create/) | [Loveable](/direction/maturity/) |

## Introduction and how you can help

The Code Review strategy page belongs to the [Source Code](/handbook/product/categories/#source-code-group) group of the [Create](/direction/create) stage,
and is maintained by [James Ramsay](https://gitlab.com/jramsay).

This strategy is a work in progress, and everyone can contribute.
Please comment and contribute in the linked issues and epics.
Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.

- [Issue list](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=code%20review)
- [Epic list](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=code%20review)

## Overview

<!--
A good description of what your category is.
If there are special considerations for your strategy or how you plan to prioritize, the description is a great place to include it.
Please include usecases, personas, and user journeys into this section.
-->

Code Review is an essential practice of every successful project.
It is necessary for maintaining and improving code quality,
and is one of the primary avenues of mentorship for software engineers,
but it is also time consuming.

GitLab's vision for code review is a place where:

- changes can be discussed,
- developers can be mentored,
- knowledge can be shared, and
- defects identified.

GitLab should make these tasks efficient and easy,
so that velocity and code quality both increase.

### Target Audience

<!--
An overview of the personas involved in this category.
An overview of the evolving user journeys as the category progresses through minimal, viable, complete and lovable maturity levels.
-->

Code review is used by software engineers and individual contributors of all kinds. Depending on their context, however, the workflow and experience of code review can vary significantly.

- **full time contributor** to a commercial product where reducing cycle time is very important. The review cycle is very tight and focussed as a consequence of best practices where keeping merge requests small and iterating at a high velocity are objectives. Code review workflows for these users are **Complete**
- **occasional contributor** to an open source product where cycle time is typically longer as a consequence that they are not working on the project full time. This results in longer review times. When long review times occur, the participants in the merge request will need to spend more time reacquainting themselves with the change. When there are non-trivial amounts of feedback this can be difficult to understand. Code review workflows for these users are **Complete**
- **scientific projects** frequently have a different flow to typical projects, where the development is sporadic, and changes are often reviewed after they have been merged to master. This is a consequence of the high code churn associated with high exploratory work, and having infrequent access to potential reviewers. Post-merge code review workflows are not yet viable in GitLab.

<!-- 
### Challenges to address

- What needs, goals, or jobs to be done do the users have?
- How do users address these challenges today? What products or work-arounds are utilized?

Provide links to UX Research issues, which validate these problems exist.
-->

## Where we are Headed

<!--
Describe the future state for your category. 
- What problems are we intending to solve? 
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this category once your strategy is at least minimally realized.
-->

The code review process involves at least two roles (author, and reviewer) but may involve many people,
who work together to achieve code quality standards and mentor the author.
Furthermore, many reviewers are often not Developers.
Reviewers may be Developers, UX Designers, Product Managers, Technical Writers, Security Engineers and more.

In support of GitLab's vision for code review,
areas of interest and improvement can be organized by the following goals:

- **efficiency** directly influences velocity within the time span of a single merge request
    - *author efficiency* considers how a merge request author can create and address code review feedback,
    - *reviewer efficiency* considers how an individual reviewer can review a code change, leave feedback, and also verify their own feedback has been addressed,
    - *team efficiency* considers a team can coordinate and communicate responsibilities, progress and status of a merge request, and quickly the entire process can be completed
- **best practices** influence efficiency of teams and projects over a longer time scale, and can include fostering norms and behaviours that aren't explicitly enforced through the application. Amplifying best practices, great defaults and documentation play a significant role in this.
- **love-ability** captures the essence that GitLab is enjoyable to use, which may mean that it is fast, invisible and allows you to get your work done. Particularly, GitLab should encourage the best of communication between colleagues and contributors, helping teams celebrate great contributions of all kinds, and express their ideas without misunderstandings. How GitLab communicates with people, will influence how people communicate with each other inside GitLab.
- **policy** controls that allows code review requirements to be set and enforced, going above and beyond amplifying and encouraging best practice.

### What's Next & Why

<!--
This is almost always sourced from the following sections, which describe top priorities for a few stakeholders.
This section must provide a link to an issue or [epic](/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.
-->

- **In Progress:** [Progressively load merge request diffs](https://gitlab.com/groups/gitlab-org/-/epics/1816)

    The product discovery sprint conducted to kick off [File-by-file merge request diffs](https://gitlab.com/groups/gitlab-org/-/epics/516)
    made it clear that loading the merge request diffs progressively in any way requires significant work.

    Merge requests are used by the majority of GitLab customers,
    and for these customers it is a critical path in ensuring quality and getting work done.
    Many large numbers of customers have reported performance problems working with large diffs, or those with lots of discussions.
    Conversations with customers, and experience says show although it is best practice to have many small merge requests,
    this is not always possible in situations like refactors, large changes, or pulling in changes from a diverged fork managed by a different company.
    
    Today the merge request interface loads all diffs and disscussions in a single request,
    which causes timeouts, poor page performance, and frustration for only moderately large merge requests.
    Addressing this is the highest priority because the merge request is a critical workflow,
    other capabilities of GitLab are integrated into and rely on the merge request,
    and we are obstructed from addressing the most important UX feedback.

- **Next:** [Smarter merge request diffs using merge refs](https://gitlab.com/groups/gitlab-org/-/epics/854)

    Early backend preparation work has been made in support of GitLab CI.

    Code reviews are time consuming,
    requiring engineers to carefully review and understand the proposed change.
    The accuracy of the diff is therefore critical.
    
    Additionally, both Atlassian and GitHub have made their diffs smarter,
    showing the actual difference between the source and target branch,
    not the source branch and the merge base of the target branch.

### What is Not Planned Right Now

<!--
Often it's just as important to talk about what you're not doing as it is to discuss what you are.
This section should include items that people might hope or think we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should in fact do.
We should limit this to a few items that are at a high enough level so someone with not a lot of detailed information about the product can understand the reasoning.
-->

### Maturity Plan

<!--
It's important your users know where you're headed next.
The maturity plan section captures this by showing what's required to achieve the next level.
-->

This category is currently at the **Loveable** maturity level (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).

## Competitive Landscape

<!--
Lost the top two or three competitors.
What the next one or two items we should work on to displace the competitor at customers?
Ideally these should be discovered through [customer meetings](/handbook/product/#customer-meetings).

We’re not aiming for feature parity with competitors,
and we’re not just looking at the features competitors talk about,
but we’re talking with customers about what they actually use,
and ultimately what they need.
-->

GitLab competes with both integrated and dedicated code review tools. Because merge requests (which is the code review interface), and more specifically the merge widget, is the single source of truth about a code change and a critical control point in the GitLab workflow, it is important that merge requests and code review in GitLab is excellent. Our primary source of competition and comparison is to dedicated code review tools.

Prospects and new customers, who previously used dedicated code review tools typically have high expectations and accustomed to a high degree of product depth. Given that developers spend a significant portion (majority?) of their in application time in merge requests, limitations are quickly noticed and become a source of frustration.

Integrated code review packaged with source code management:

- [Phabricator](https://www.phacility.com/phabricator/) by Phacility (**very mature**) ([example](https://phabricator.haskell.org/D4953))
- [Gerrit](https://www.gerritcodereview.com/index.html) _(free, open source)_ (**very mature**) ([example](https://gerrit-review.googlesource.com/q/status:open+project:gerrit))
- [GitHub](https://github.com/features/code-review/)
- [Bitbucket](https://bitbucket.org/product/features) by Atlassian
- [Azure DevOps](https://azure.microsoft.com/en-us/services/devops/) by Microsoft

Dedicated code review tools:

- [Crucible](https://www.atlassian.com/software/crucible) by Atlassian  (**very mature**) ([Bitbucket vs Crucible](https://confluence.atlassian.com/bitbucketserverkb/what-s-the-difference-between-crucible-and-bitbucket-server-do-i-need-both-779171640.html))
- [Review Board](https://www.reviewboard.org/) ([example](http://demo.reviewboard.org/r/844/diff/1/#index_header))
- [Reviewable](https://reviewable.io/) ([example](https://reviewable.io/reviews/Reviewable/demo/1))

## Analyst Landscape

<!--
What are analysts and/or thought leaders in the space talking about?
What are one or two issues that will help us stay relevant from their perspective?
-->

## Top Customer Success/Sales issue(s)

<!--
These can be sourced from the CS/Sales top issue labels when available,
internal surveys, or from your conversations with them.
-->

The highest priority customer requests are for improved application performance, accuracy and efficiency for reviewing merge request diffs of all sizes, small and extremely large.

- [Smarter merge request diffs using merge refs](https://gitlab.com/groups/gitlab-org/-/epics/854) address accuracy problems in some situations, thereby improving **efficiency** of reviews by showing the expected diff contents.
- [File-by-file merge request diffs](https://gitlab.com/groups/gitlab-org/-/epics/516) improves application performance, thus improving **reviewer efficiency** by reducing the time waiting to view diffs.
- [Track unread diffs, files, and discussions](https://gitlab.com/groups/gitlab-org/-/epics/1409) improves usability. primarily improves **reviewer efficiency** by allowing reviews to be performed incrementally over multiple sittings, and better handling the iterative process of leaving feedback and the author proposing improvements.

Other notable requests include:

- [Cross-project code review (group merge requests)](https://gitlab.com/groups/gitlab-org/-/epics/457)
- [Post-merge code review](https://gitlab.com/groups/gitlab-org/-/epics/872) is  of interest to a variety of organizations where changes are merged with a very high velocity (e.g daily) and they desire to review aggregate set of changes semi-regularly.

## Top user issue(s)

<!--
This is probably the top popular issue from the category (i.e. the one with the most thumbs-up),
but you may have a different item coming out of customer calls.
-->

- [Smarter merge request diffs using merge refs](https://gitlab.com/groups/gitlab-org/-/epics/854)
- [Increased focus of merge request changes tab](https://gitlab.com/groups/gitlab-org/-/epics/1406) will make code review more **love-able** by reducing distraction, and making use of the navigational affordances of the top of the page which is quickly accessed by mouse and keyboard.

## Top internal customer issue(s)

<!--
These are sourced from internal customers wanting to [dogfood](/handbook/product/#dogfood-everything) the product.
-->

- [Suggest and assign reviewers and maintainers](https://gitlab.com/groups/gitlab-org/-/epics/1823) will replace the Reviewer Roulette implemented with Danger.

## Top Vision Item(s)

<!--
What's the most important thing to move your vision forward?
-->

Coordination of responsibilities and handoff can be time consuming, particularly for changes spanning backend, frontend, database and perhaps other services. Tools to make requesting a code review, understand the progress of the code review, and hand off from reviewer back to engineer is important.

- [Merge request reviewers](https://gitlab.com/groups/gitlab-org/-/epics/1330) improves **team efficiency** by improving hand off between team members, and making it clear the different roles of participation in a merge request.

It is critical the merge request is reliably fast, accurate and efficient, as it is a blocking step in the software development lifecycle. It is important that GitLab smooth and streamline the process, and not become a source of friction or frustration to the adoption of best practice or high velocity.

- [File-by-file merge request diffs](https://gitlab.com/groups/gitlab-org/-/epics/516) primarily improves **reviewer efficiency** be allow faster access to each file, and improved usability for large diffs.
- [Track unread diffs, files, and discussions](https://gitlab.com/groups/gitlab-org/-/epics/1409) primarily improves **reviewer efficiency** by allowing reviews to be performed incrementally over multiple sittings, and better handling the iterative process of leaving feedback and the author proposing improvements.

Commits are the critical unit of work in Git, and used well are tremendously valuable. GitLab should be enabling and amplifying best practice behaviours using commits, so that the commit messages and each commit is a valuable asset for years. Great commit messages and meaningful commits allow developers to move quickly, and effectively make use of the entire ecosystem of Git tooling.

- [Commit messages as part of code review](https://gitlab.com/groups/gitlab-org/-/epics/286) helps teams adopt **best practice** commit messages by making them reviewable. Commit messages are part of the distributed nature of Git and should used to maximum extent so that valuable information is portable and easily accessed through local Git tooling.
- [Commit by commit code review](https://gitlab.com/groups/gitlab-org/-/epics/285) will teams that perform commit by commit reviews, which are a **best practice** for some teams.
