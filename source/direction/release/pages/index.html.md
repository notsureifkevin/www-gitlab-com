---
layout: markdown_page
title: "Category Vision - GitLab Pages"
---

- TOC
{:toc}

## GitLab Pages

GitLab Pages allows you to create a statically generated website from your project that
is automatically built using GitLab CI and hosted on our infrastructure.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3APages)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)
- [Documentation](https://docs.gitlab.com/ee/user/project/pages/) 

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1296) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

### Overall Prioritization

Pages is not strategically our most important Release feature, but it's a popular
feature and one that people really enjoy engaging with as part of the GitLab
experience; it's truly one of our most "personal" features in the Release stage.
We do not plan to provide a market-leading solution for static web page hosting,
but we do want to offer one that is capable for most basic needs, in particular
for hosting static content and documentation that is a part of your software
release.

## What's Next & Why

Automatic certificate renewal ([gitlab-ce#28996](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996))
is our most popular issue and one that can be quite irritating to manually
manage. We're excited to address this next in order to make using Pages
easier and require less ongoing maintenance.

## Maturity Plan

This category is currently at the "Complete" maturity level, and
our next maturity target is Lovable (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- [Automatic certificate renewal](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996) (Complete)
- [Multiple version support](https://gitlab.com/gitlab-org/gitlab-ce/issues/35141)
- [Pages without DNS wildcard](https://gitlab.com/gitlab-org/gitlab-ce/issues/29963)
- [Per-site, in-repo configuration](https://gitlab.com/gitlab-org/gitlab-pages/issues/57)

## Competitive Landscape

Gitlab pages are offered as a service, but we do not position ourselves as a static
site market leader. Other providers, such as [Netlify] (https://www.netlify.com/) provide
a more comprehensive solution. There are project templates available in Gitlab that offer
the use of  Netlify for static site CI/CD, while also still taking advantage of GitLab for
repos, merge requests, issues, and everything else. [gitlab-ce#57785)](https://gitlab.com/gitlab-org/gitlab-ce/issues/57785)

## Top Customer Success/Sales Issue(s)

Our TAM team has identified [gitlab-ce#29963](https://gitlab.com/gitlab-org/gitlab-ce/issues/29963)
## Top Customer Issue(s)

The most popular customer issue is [gitlab-ce#29963](https://gitlab.com/gitlab-org/gitlab-ce/issues/29963).
Creating Gitlab pages today requires admins to setup wildcard DNS records and SSL/TLS certificates. Some services
and/or corporate security policies forbid wildcard DNS records, preventing users from
benefitting from using Gitlab pages. This issue will remove the need for wildcard DNS and allow
many more users to enjoy the pages experience.
(GitLab Pages without DNS wildcard) as important for their customers.

## Top Internal Customer Issue(s)

Our top internal customer issue is [gitlab-ce#35141](https://gitlab.com/gitlab-org/gitlab-ce/issues/35141)
which enables having multiple GitLab Pages generated based on branches or tags.

Our own docs team is considering moving to a different hosting provider; details
on reasons why can be found at [gitlab-docs#313](https://gitlab.com/gitlab-org/gitlab-docs/issues/313).
The main difficulty with using Pages at [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com) 
/ [GitLab documentation](https://gitlab.com/gitlab-org/gitlab-docs) scale is:

- Caching artifacts between pages runs
  - We clear and recreate everything every time, but we could mark certain directories (mark certain folders as "do not rebuild", potentially detect what's different via only/except data), in which case these would just be persisted from the previous run. See comment [gitlab-ce#29496](https://gitlab.com/gitlab-org/gitlab-ce/issues/29498#note_149246568) for how this might look.
  - The alternative is having your own server, where you can obviously just leave files there.
## Top Vision Item(s)

From a vision standpoint, adding Review Apps for Pages ([gitlab-ce#26621](https://gitlab.com/gitlab-org/gitlab-ce/issues/26621))
is interesting because it allows for more sophisticated development flows
involving testing, where at the moment the only environment that GitLab
understands is production. This would level up our ability for Pages to
be a more mission-critical part of projects and groups.
