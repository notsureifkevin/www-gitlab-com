---
layout: markdown_page
title: "Product Section Vision - CI/CD"
---

{:.no_toc}

- TOC
{:toc}

## CI/CD Overview
<!-- Provide a general overview of the section, what is covered within it and introduce themes.
Include details on our current market share (if available), the total addressable market (TAM),
our competitive position, and high level feedback from customers on current features -->

The CI/CD section focuses on the code build/verification ([Verify](/direction/verify)), packaging/distribution ([Package](/direction/package)), and delivery ([Release](/direction/release)) stages of the [DevOps Lifecycle](/stages-devops-lifecycle/). Each of these areas has their own page with upcoming features, strategic considerations, and more; this page ties them together via the important concepts that unify the direction across CI/CD. Also related is the [mobile use case](/direction/mobile), which touches on features across GitLab but has a large component in making build, test, and release for mobile apps better and easier to do. You can see how the CI/CD stages relate to each other in the following infographic:

![Pipeline Infographic](/images/cicd/gitlab_cicd_workflow_verify_package_release_v12_3.png "Pipeline Infographic")

CI/CD represents a large portion of the DevOps market with significant growth upside. The `DevOps Ready` portion of the market, representing application release automation/CD and configuration management (mainly aligned to the Release stage) supported 984.7MM USD revenue, projected by 2022 to grow to 1617.0MM. The `DevOps Enabled` portion of the market, representing Continuous Integration, integrated quality/code review/static analysis/test automation, and environment management (mainly aligned to the Verify stage) represented an even larger 1785.0MM market, projected to grow to 2624.0MM. With a combined projected total of 4241.0MM by 2022, it's critical that we continue to expand our share of this important segment. [Data Link, GitLab Internal](https://docs.google.com/spreadsheets/d/14j-C-9AzSRI2pEvX2zh42O34Y_EgDocEqffwv_KyJJ4/edit#gid=0)

The CI/CD section currently has [41 members with 22 vacancies open](/company/team/?department=cicd-section). Tracking to our hiring plan can be seen on our [hiring chart](https://about.gitlab.com/handbook/hiring/charts/cicd-section/). Verify is in [year 7 maturity](/direction/maturity/#verify), Package is [year 3 maturity](/direction/maturity/#package), and Release is [year 4 maturity](/direction/maturity/#release).

## Competitive Space and Positioning

CI/CD is a highly competitive and evolving area within DevOps, with a rapid pace of technological innovation. Kubernetes itself, a massive driver of innovation, just turned five years old in 2019. Docker, now considered a mature and foundational technology, was only released in 2013. We do not expect this pace of innovation to slow down, so it's important we balance supporting the current technology winners (Kubernetes, AWS, Azure, and Google Cloud) with best-of-breed, built-in solutions while at the same time avoiding over-investment on any single technology solution such that could make us irrelevant as it becomes dated.

There are dangers on both sides; Pivotal resisted embracing Kubernetes early and has [suffered for it](https://fortune.com/2019/07/29/ford-pivotal-write-down/). At the same time, technologies like Jenkins that too fully embraced what are now considered legacy paradigms are enjoying the long tail of relevancy, but are having to [scramble to remain relevant for the future](https://jenkins.io/blog/2018/08/31/shifting-gears/) resulting in confusing product messaging and adoption challenges.

### Trend Towards Consolidation

Since the beginning of 2019, we’ve seen a trend towards consolidation in the DevOps industry. In January, [Travis CI was acquired by Idera](https://techcrunch.com/2019/01/23/idera-acquires-travis-ci/), and in February we saw [Shippable acquired by JFrog](https://techcrunch.com/2019/02/21/jfrog-acquires-shippable-adding-continuous-integration-and-delivery-to-its-devops-platform/). Atlassian and GitHub now both bundle CI/CD with SCM, alongside their ever-growing related suite of products.

It's normal for technology markets go through stages as they mature: when a young technology is first becoming popular, there tends to be an explosion of tools to support it. New technologies have a lot of rough edges that make them difficult to use and early tools tend to center around making the experience easier to adopt and use. Once a technology matures, tool consolidation is a natural part of the life cycle. We're in a great position to be ahead of the curve on consolidation, but it's a position we more and more are going to need to defend against well-funded competitors who also have a vision for a single DevOps application.

### Making Continuous Delivery Real

Our product positioning for CI/CD is aligned to the one set forth in Jez Humble's classic [Continuous Delivery](https://continuousdelivery.com/) book. The ideas of reducing risk, getting features and fixes out quickly, and reducing costs/creating happier teams by removing the barriers to getting things done have stood the test of time. We've also embraced recent innovation happening here that ties these original concepts into Kubernetes and incremental rollout, that is emerging under the banner of Progressive Delivery. Still, the [5 principles at the heart of continuous delivery](https://continuousdelivery.com/principles/) remain illustrative of the problems we are focused on solving in our CI/CD solution: 

1. Build quality in
1. Work in small batches
1. Computers perform repetitive tasks, people solve problems
1. Relentlessly pursue continuous improvement
1. Everyone is responsible

With a head start on a single DevOps application, we're uniquely positioned to make Continuous (and Progressive) Delivery a reality for our users, supporting them through the transformations they need to achieve these goals. No point solution is able to provide a comprehensive view on software delivery in the way that we are, unifying execution teams rather than providing segmented tools/views for each department or role.

### Challenges
<!-- What are our constraints? (team size, product maturity, lack of brand, GTM challenges, etc). What are our market/competitive challenges? -->

- Highly competitive market with large competitors joining the fray providing full DevOps solutions delivered as a single application, and smaller competitors innovating on features.
- GitHub in particular has a very strong product positioning and is set to be a powerful competitor now that they have completed their acquisition by Microsoft.
- Spinnaker (and to a certain extent Drone) are doing well on the smaller side, innovating on CD technology. Spinnaker in particular is interesting around how they are enabling [collaborative CI/CD workflows using automation](https://opensource.com/article/19/8/why-spinnaker-matters-cicd).

### Opportunities

- Our complete DevOps platform delivered as a single application is an incredible foundation on which to build. CI/CD in particular performs very well under analyst review and represents our "foot in the door" for many customers to the GitLab application at large.
- Continuous Delivery (Release stage) and Package stage features represent a exciting new markets for us to go after. In particular we see large, successful competitors like LaunchDarkly doing well in this space around Feature Flags for Release, and GitHub launching features like Package Management to bring themselves to parity with our Package offering.
- Middle of the road point CI/CD solutions like TravisCI and CircleCI are suffering from losing ground at both ends, and we have an opportunity as well to solve problems for these customers and bring them on board. Jenkins remains seen as legacy and we have an opportunity to help users of that platform also onboard to GitLab.

## Strategy
<!-- Where will the product be in 3 years? How will the customer's life/workflow be different in 3 years as a
result of our product? Also, should call out explicitly what's part of the three year plan but not being done
in the next year. -->

Our unified strategy for CI/CD is oriented around the idea of enabling [GitOps](https://www.youtube.com/watch?v=5ykRuaZvY-E) enabled workflows throughout our single application. GitOps is an emerging concept that wraps all of CI/CD into a git-first workflow: pipeline configuration, code, and infrastructure are all stored in git as the single source of truth, and the entire development flow from ideation through delivery is interacted with in git-first tooling. Events in the system are oriented around state changes relating to source control, issues and merge requests, facilitating change management that is easier to use than previous paradigms, and all of this is tightly connected through a single GitLab pane of glass. All of this is to enable the continued evolution from waterfall (week or monthly cycles), to agile (cycles in terms of days), to DevOps (real, immediate feedback through progressive delivery.)

GitLab is uniquely positioned to make this reality, being a provider of a mature SCM and CI/CD solution that has always been git-first. Disparate sources of truth for the state of the world across a fleet of point applications just doesn't scale, and we believe this is a better way. CI pipelines flowing information about builds into the package management system and ultimately out via progressively-deployed releases, all with no setup required, and all using git as the common reference point will be very powerful. A great example of the kind of features enabled by this connectedness is reflected in our vision for [automatic evidence collection in the release process](https://gitlab.com/groups/gitlab-org/-/epics/1856).

We also have a specific strategy for each stage, focused on the unique elements of that area:

### Verify

<%= partial("direction/cicd/strategies/verify") %>

### Package

<%= partial("direction/cicd/strategies/package") %>

### Release

<%= partial("direction/cicd/strategies/release") %>

## Plan & Themes

Given our vision and strategy, our plan for the next year has been organized around a number of themes that support our direction. These are listed here in roughly priority order:

### What we're doing

#### Single Application CI/CD

<%= partial("direction/cicd/themes/integrated_solutions") %>

#### Multi-Platform Support

<%= partial("direction/cicd/themes/multi_platform") %>

#### Speedy, Reliable Pipelines

<%= partial("direction/cicd/themes/speedy_pipelines") %>

#### Do Powerful Things Easily

<%= partial("direction/cicd/themes/cool_things") %>

#### Progressive Delivery

<%= partial("direction/cicd/themes/progressive_delivery") %>

#### Mobile Support

<%= partial("direction/cicd/themes/mobile_support") %>

#### Compliance as Code & Secure Secrets

<%= partial("direction/cicd/themes/compliance_as_code") %>

#### Universal Package Management

<%= partial("direction/cicd/themes/universal_package_management") %>

### What we're not doing

#### Event-Based CI/CD 

<%= partial("direction/cicd/themes/event_based") %>

#### GitOps Pull-Based Deployment

<%= partial("direction/cicd/themes/pull_pipelines") %>

## What's Next
<!-- Conclude with What must we get done in the next 12 months? What won't be done? Then
transition into direction items highlighting those themes across the relevant stages -->

With the above vision, strategy, and plan accounted for, here's our plan for what we will be delivering over the next few releases:

<%= direction %>
