---
layout: markdown_page
title: "Category Vision - Accessibility Testing"
---

- TOC
{:toc}

## Accessibility Testing

Beyond being a compliance requirement in many cases, accessibility testing is the right thing to do for your users. Accessibility testing is similar to UAT or Usability (which we track [here]/direction/verify/usability_testing/)), but we see accessibility as a primary concern of its own.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AAccessibility%20Testing)
- [Overall Vision](/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1301) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

Up next is [gitlab-ce#54806](https://gitlab.com/gitlab-org/gitlab-ce/issues/54806) (accessibility testing for review apps MVC) which establishes our footing in the accessibility testing space. This will enable us to start offering out-of-the-box accessibility testing via artifacts, and provide a foundation for us to build more features on top of such as making the accessibility results embedded in the MR widget or provide more detailed CI views for accessibility testing.

## Maturity Plan

This category is currently at the "Planned" maturity level, and
our next maturity target is Minimal (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- [Automated a11y scanning of Review Apps](https://gitlab.com/gitlab-org/gitlab-ce/issues/54806)

## Competitive Landscape

Competitors in this space are not providing first-party accessibility testing platforms, but do integrate with pa11y or other tools to generate results via the CI/CD pipeline. Implementing [gitlab-ce#54806](https://gitlab.com/gitlab-org/gitlab-ce/issues/54806) (accessibility testing for review apps MVC) will help us meet (or in many cases exceed) competitor capability be embedding it into our review apps, a natural home for them.

## Top Customer Success/Sales Issue(s)

There are no top CS/Sales issues for this category yet.

## Top Customer Issue(s)

There are no top customer issues for this category yet.

## Top Internal Customer Issue(s)

Automatic a11y ([gitlab-ce#54806](https://gitlab.com/gitlab-org/gitlab-ce/issues/54806)) testing has been requested by our internal UX teams as a way to ensure our release process is managing accessibility over time within the application.

A related Epic is the GitLab accessibility epic that contains items about making GitLab itself more accessible: [gitlab-org#567](https://gitlab.com/groups/gitlab-org/-/epics/567).  Loveable for this category is defined as being able to use GitLab to detect all of those issues in an automated way and see that they are addressed when fixed.

## Top Vision Item(s)

Similar to other sections where this issue is repeated, [gitlab-ce#54806](https://gitlab.com/gitlab-org/gitlab-ce/issues/54806) will establish our presence with out-of-the-box accessibility testing.
