---
layout: markdown_page
title: "Category Vision - Load Testing"
---

- TOC
{:toc}

## Load Testing

Load testing is a common and typically important stage in the CI process, ensuring changes to the app can scale before they are introduced into production. Be confident in the performance of your changes by ensuring that they are validated against real-world load scenarios.

Advanced deployment strategies like Canary and Blue/Green can mitigate the risk of deploying code that is not performant or does not scale. However, that is not a valid alternative for a few reasons:
* You can't improve what you don't measure. Running repeatable and consistent performance tests lets you compare results between runs, to ensure performance doesn't slowly degrade over time.
* Canary and Blue/Green do minimize risk, but you are still essentially testing in production. Best practices would dictate that you use CI test what you can, to reduce the frequency and occurrence of rollbacks and errors in production.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ALoad%20Testing)
- [Overall Vision](/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1305) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

Up next is the Integrated Load Testing MVC ([gitlab-org#952](https://gitlab.com/groups/gitlab-org/-/epics/952)).  This epic brings performance testing as a category to viable maturity.

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- [Integrate artillery.io for load testing](https://gitlab.com/gitlab-org/gitlab-ee/issues/10683)
- [Add integrated load testing to AutoDevOps](https://gitlab.com/gitlab-org/gitlab-ee/issues/10681)

## Competitive Landscape

### Azure DevOps

Azure DevOps offers in-product load testing: https://docs.microsoft.com/en-us/azure/devops/test/load-test/get-started-simple-cloud-load-test?view=azure-devops.  This consists of different types of tests including:

* HTTP Archive Based tests
* URL based tests
* Apache JMeter Tests

For URL type tests, the output contains information about the average response time, user load, requests per second, failed requests and errors (if any).

### Travis CI/CircleCI

While, just as one could orchestrate any number of performance testing tools with GitLab CI today, Travis or CircleCI could be used to orchestrate performance testing tools, it does not have any built-in capabilities around this.

## Top Customer Success/Sales Issue(s)

TBD

## Top Customer Issue(s)

TBD

## Top Internal Customer Issue(s)

Integrated Load Testing ([gitlab-org&952](https://gitlab.com/groups/gitlab-org/-/epics/952)) is the most sought after internal customer issue from Quality. Specifically, integrating with artillery.io ([gitlab-ee#10683](https://gitlab.com/gitlab-org/gitlab-ee/issues/10683)) has been asked for.

## Top Vision Item(s)
The top Vision item is [gitlab-ee#10681](https://gitlab.com/gitlab-org/gitlab-ee/issues/10681) which will add Integrated Load Testing as an Auto DevOps step.  This will mean that what we have defined as viable for this category will now be added to every Auto DevOps pipeline.
