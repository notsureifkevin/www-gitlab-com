---
layout: markdown_page
title: "Category Vision - Audit Management"
---

- TOC
{:toc}

## Audit Management

|  |  | 
| ------ | ------ |
| Stage | [Manage](https://about.gitlab.com/direction/manage) | 
| Maturity | [Minimal](https://about.gitlab.com/handbook/product/categories/maturity/) |

### Introduction and how you can help

Thanks for visiting this category page on audit management in GitLab. This page belongs to the Access group of the Manage stage, and is maintained by Jeremy Watson.

This vision is a work in progress, and everyone can contribute:
* Please comment and contribute in the linked issues and epics on this category page. Sharing your feedback directly on GitLab.com is the best way to contribute to our vision.
* Please share feedback directly via email, Twitter, or on a video call. If you’re a GitLab user and have direct knowledge of your need for compliance and auditing, we’d especially love to hear from you.

### Overview

“Compliance” is a term that evokes images of a large and slow-moving organization, but every instance with more than one user manages compliance in some way or form. At GitLab, we think of “compliance” as simply managing and knowing what your users are doing. Whether you’re collaborating with a couple of contractors on a small CE instance or managing thousands of users for a HIPAA-compliant healthcare company, GitLab should be able to provide the controls and traceability that your instance needs to be successful.

### Problems to solve

GitLab is an application used by people. These users interact with shared resources, like projects, and with each other in comments and groups. Administrators, especially in instances with tons of activity and membership, want to ensure that user-driven changes are adhering to the rules - and that they have the ability to see and backtrace activity if an event requires further investigation. If we’re not offering a comprehensive set of logs, instances won’t be able to feel confident that they’ll be able to have the information needed to answer critical questions when the unexpected occurs. Activity in GitLab should be **fully transparent** to administrators.

* What success looks like: GitLab should log 100% of user-driven events. These records should be optionally retained for a long period of time (years), and should be easily queryable in GitLab and in 3rd party tools.

Building on the need for transparency, it’s equally important that we’re presenting information in a way that makes answering questions simple. It’s frustrating to comb through logs. Separations of concerns questions like “who opened and merged this MR?” aren’t readily addressable in a logging system that logs individual events. GitLab’s approach should promote **traceability**; when a change is logged, we should enable an administrator to see the full context around the change.

* What success looks like: an administrator should be able to explore a logged event and see related changes and users. Clicking on the recorded event for a comment in a merge request, for instance, should show more contextual data around the merge request itself (when it was opened, merged, and who interacted with it). This should also be possible to do via the API.

The best guard against undesired events, however, is to actively guard against them. For most organizations, being overly permissive and reacting to threats and events just isn’t an option. This problem is doubly challenging, since internal rules and processes may differ radically from organization to organization; any system that provides configuration here must have flexibility. Some instances may want to resolve threats automatically (e.g. by locking a user's account until an administrator can evaluate the event), while others may want to take a lighter touch and simply warn on suspicious activity.

* What success looks like: an instance should be able to modify and flag user behavior. These restrictions should be highly configurable, and range from the simple to the complex.

Finally, even with the above problems solved, administrators want to sleep well at night knowing that their instance is secure and fully compliant. There’s rarely a single source of truth that signals to an auditor that everything is OK. Even with logging and a highly configured instance, there’s rarely a resource that an auditor can turn to that provides an easy to understand status on the instance’s compliance status (against an internal compliance framework, or an external one like HIPAA). 

* What success looks like: an administrator should be able to risk manage from a  a “bird’s eye” view of their instance, getting a summary view of their compliance status. An administrator should be able to monitor for threats, and receive/mitigate alerts on suspicious behavior (e.g. a user’s location based on IP is appearing somewhere it shouldn’t) using an intelligent UEBA system. This status should also include clear evidence of an instance's compliance against a defined framework like PCI, HIPAA, GDPR, SAFe.

Excellence in this category means that we’re solving for all of these needs: **transparency**, **traceability**, **configuration**, and **monitoring**.

### Our approach

Audit management is a category that touches many areas of GitLab. This includes audit events, permissions, policies, abuse reporting, user management, and others.

To solve the need for **transparency**, we’ll iterate on the existing audit event system in GitLab to include 100% of user-driven events. Along side enriching our logs, we’ll scale our audit event system to accommodate long retention periods and better support other systems that may ingest these logs, like Splunk and Elasticsearch.

Other problem spaces - **traceability**, **configuration**, and **monitoring** - demand new features.

Since the buyer type for compliance features tends to be a medium to large enterprise undergoing digital transformation, most of the planned additions are targeted at GitLab’s Premium and Ultimate tier. 

### Maturity

Currently, GitLab’s maturity in Audit Management is **minimal**. Here’s why:
* GitLab currently offers an audit log system, but it does not capture 100% of user-driven events. While we’re iterating and capturing more events over time, our users demand a comprehensive view of activity in application logs.

**Viability** for this category is having a robust, complete set of logs that captures the vast majority of user activity. These logs are structured and easily parsed; while GitLab may not actively guard against threats, an administrator can easily track down what happened.

A **complete** audit management category would allow an administrator to guard against threats by configuring custom policies they can apply to their users. This configuration should be powerful and flexible, allowing an organization to write custom permissions for capabilities they want to protect (or later, permit). GitLab won’t actively warn when a model is breached by suspicious behavior (e.g. data exfiltration), but these configurable guardrails should defuse a majority of problems.

**Lovable** compliance adds on active monitoring and makes GitLab truly trusted by users. The application is able to demonstrate security and compliance on an ongoing basis in a single view, and actively monitors for (and defends against) suspicious behavior. An administrator or auditor can check a dashboard to be brought up to speed - or simply get some peace of mind.

### What’s next and why

Since this category is currently minimally mature, we’re pursuing viability and **transparency** by adding additional activities to our audit logs. You can view the continuous compliance viability epic [here](https://gitlab.com/groups/gitlab-org/-/epics/1217).

Immediate next steps in this epic are ensuring that audit logs can [scale sufficiently](https://gitlab.com/groups/gitlab-org/-/epics/641) without losing data, and that GitLab's audit log is [comprehensive](https://gitlab.com/groups/gitlab-org/-/epics/736).

After compliance in GitLab is [viable](https://gitlab.com/groups/gitlab-org/-/epics/1217), we’ll pursue improving **configuration** on the way to a complete set of compliance tools by building an MVC for defining a [user policy](https://gitlab.com/groups/gitlab-org/-/epics/366).

### Competitive landscape

To be completed. Tools that assist companies with security and compliance range across categories and use cases; these include SIEM tools like [Splunk](https://www.splunk.com/en_us/cyber-security/compliance.html), compliance products for deployed applications like [Tigera](https://www.tigera.io/),
and tools that make software auditing easier.

### Analyst landscape

We do not currently engage with analysts in this category.

### Top Customer Success/Sales issue(s)

* [Comprehensive audit log](https://gitlab.com/groups/gitlab-org/-/epics/736). CS, sales, and users all want a set of logs that captures the vast majority of events in GitLab. Since you don't know what you need until you need it, customers expect us to log everything.

### Top user issue(s)

* [Comprehensive audit log](https://gitlab.com/groups/gitlab-org/-/epics/736).

### Top Vision issue(s)

* [Policies](https://gitlab.com/groups/gitlab-org/-/epics/366). Writing custom policies on top of GitLab's existing permissions/roles system will be a powerful, flexible tool that enables enterprises to stay compliant and secure.
* [Behavioral monitoring MVC](https://gitlab.com/groups/gitlab-org/-/epics/259). With the volume of events in GitLab, effective monitoring won't be possible without automated alerting.

