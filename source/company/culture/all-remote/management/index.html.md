---
layout: markdown_page
title: "All Remote Management"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction 

On this page, we're detailing what it takes to effectively and efficiently manage an all-remote company. 

## How do you manage a 100% remote team?

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/IFBj9KQSQXA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [video](https://www.youtube.com/watch?v=IFBj9KQSQXA) above, GitLab co-founder and CEO Sid Sijbrandij and InVision Chief People Officer Mark Frein discuss the future of remote work, including managing all-remote teams at scale.*

"How do you manage when everyone is remote?" is a common question for those leading or managing within an all-remote company. 

In truth, managing an all-remote company is much like managing any other company. It comes down to trust, communication, and company-wide support of shared goals, all of which aid in [avoiding dysfunction](/handbook/values/#five-dysfunctions).

The pillars of managing an all-remote company are similar to managing any company, but there are certain areas where all-remote leaders should pay particular attention to.

## Embracing total transparency

Transparency is a term that is often tossed around as a value within most companies. In all-remote environments, it is vital that transparency be more than a buzzword, but something that is embraced and allowed to guide every decision.

This will often feel unnatural and uncomfortable — a sign that your organization truly is living out the [value of transparency](/handbook/values/#transparency). 

It helps to recognize all-remote organizations not as a collection of rigidly structured machines, but as a living, evolving organism. Leaders must trust their colleagues to operate with empathy, kindness, and concern for the well-being of one another, seeing the free flow of information as universally beneficial.

Learn more on how GitLab [defines and implements transparency in our Handbook](/handbook/values/#transparency). 

## Asynchronous

When you [open your recruiting pipeline to the world](/company/culture/all-remote/hiring/), you create an opportunity to hire people in an array of time zones. The ability to hand projects off across time zones is a [competitive advantage](/company/culture/all-remote/benefits/), but minimizing disconnects, frustrations, and awkwardly-timed meetings requires an intentional approach.

The first step in creating an atmosphere where colleagues are comfortable working asynchronously is to avoid the default mentality as it applies to [meetings](/company/culture/all-remote/meetings/). By making meetings [optional](/company/culture/all-remote/meetings/#make-meetings-optional), recording and [documenting everything](/company/culture/all-remote/meetings/#document-everything-live-yes-everything), being diligent to follow an [agenda](/company/culture/all-remote/meetings/#have-an-agenda), and leveraging tools like [GitLab Issues](https://docs.gitlab.com/ee/user/project/issues/) and Slack, all-remote companies are less reliant on colleagues being online at the same time. 

This mentality must be actively reinforced. For example, in [social calls](/company/culture/all-remote/informal-communication/#social-calls) where dozens of people join a video chat to bond as a team, an agenda allows those who cannot make it to add [shout-outs](/handbook/values/#say-thanks) or discussion points that a fellow colleague can verbalize. This is an intentional approach to not only working asynchronously, but socializing asynchronously. 

As is documented in the [Communication section of GitLab's Handbook](/handbook/communication/), there are limits to asynchronous communication. When we go **back and forth three times,** we jump on a [synchronous video call](https://about.gitlab.com/handbook/communication/#video-calls).

All-remote companies that have colleagues spread out across time zones will encounter scenarios where one has to [compromise](/company/culture/all-remote/drawbacks/) in order to be online at the same time for critical calls, meetings, or projects. However, there is great freedom in being able to disconnect from work at an appointed time with the understanding that your colleagues will communicate asynchronously rather than pressuring you to be available outside of your work hours.

Learn more about GitLab's approach to asynchronous working by viewing our [Efficiency](/handbook/values/#efficiency) and [Collaboration](/handbook/values/#collaboration) values. 

## Separating decision gathering from decision making

Paralysis by analysis is something all companies should seek to avoid. In managing through this at an all-remote company, leaders should ensure that all colleagues understand that consensus doesn't scale.

Thus, there should be [no goal to achieve concensus](/handbook/general-guidelines/#getting-things-done). This may feel awkward or unnatural to those coming from colocated corporate environments, but trusting decision makers and living out [the value of iteration](/handbook/values/#iteration) prevents unnecessary slowdowns in your organization. 

By intentionally seperating the process of decision *gathering* and decision *making*, you provide ample opportunity for everyone to add input, offering up fresh angles for consideration that may well sway the mind of the DRI ([directly responsible individual](/handbook/people-operations/directly-responsible-individuals/)). 

It is vital for all-remote companies to foster an atmopshere of [trust and learning](/handbook/values/#five-dysfunctions), such that grudges are not held against decision makers after decision gathering has occured. At GitLab, this is manifested in our [Collaboration](/handbook/values/#collaboration) value, which includes kindness, sharing, [short toes](/handbook/values/#short-toes), no ego, and assuming positive intent. 

## Applying iteration to everything

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/T4fQp9jtKWU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [video](https://www.youtube.com/watch?v=T4fQp9jtKWU) above, published on the [GitLab Unfiltered YouTube channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A), Emilie Schario, Data Analyst, and Sid Sijbrandij, co-founder and CEO, discuss the best way to organize the Metrics pages in the company Handbook.*

Iteration is oft applied to engineering, but asking only part of the company to iterate can create [discord](/handbook/values/#five-dysfunctions). All-remote companies must empower every member of the team, across every function and job level, to approach their work with an [iterative mindset](/handbook/values/#iteration). 

By applying [iteration](/handbook/values/#iteration) to everything, it removes the barrier of fear and judgement. It also enables faster cycles, and it makes miscues far less damaging. 

For example, don't write a large plan, only write the first step. Trust that you'll know better how to proceed after something is released. Iteration can be uncomfortable, even painful. If you're doing iteration correctly, it should be.

Instilling this in an all-remote team is difficult. Most people are naturally inclined to only showcase polished work. In turn, managing this aspect of an all-remote team requires reminders that it's *preferred* to share unfinished work.

Leaders should work diligently to ensure that teams have a low level of shame and believe that everything is in draft and subject to change. 

Learn more about [GitLab's value of iteration in our Handbook](/handbook/values/#iteration), and read how [one team used survey results to iterate on culture](/2018/06/26/iterating-improving-frontend-culture/).

## Company-wide organizational chart

There are no corner offices in an all-remote company. Although you should consider the organizational structure that makes the most sense for you (and [iterate](/handbook/values/#iteration) as the company evolves), one thing that should not change is the level of transparency.

To give each member of the company an equal view of how the organization is structured, how job levels/families are established, and what reporting structures look like, it's wise to consider a publicly accessible org chart. 

This removes ambiguity and enables clearer lines of communication. 

We invite other all-remote companies to mirror GitLab's approach to publishing its [team structure](/company/team/structure/index.html) and [org chart](/company/team/org-chart/). 

## Avoiding dotted lines and matrix organization

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/tSp5se9BudA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [video](https://www.youtube.com/watch?v=tSp5se9BudA) above, published on [GitLab's YouTube channel](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg), GitLab co-founder and CEO Sid Sijbrandij and [Arch Systems](https://archsystemsinc.com/) CEO Andrew Scheuermann discuss structure within distributed companies.*

In all-remote companies, it is easy to fall into a situation where you work with a day-to-day lead but report to someone else. There are no physical office structures to reinforce reporting structures. 

Leaders in an all-remote company must work to avoid dotted lines and matrix organization. Everyone should report to exactly one person, and that person should understand your day-to-day tasks and be well-positioned to assist you in removing obstacles to thriving in your role. 

Whenever there is need to work on a specific, high-level, cross functional business problem, a [working group](/company/team/structure/working-groups/) should be established for that need.

Learn more about GitLab's approach in the [Leadership section of our Handbook](/handbook/leadership/#no-matrix-organization).

## Focusing on results

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/6QC1OwoddD8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [video](https://www.youtube.com/watch?v=6QC1OwoddD8) above, published to [GitLab's YouTube channel](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg), members of the engineering team demonstrate a focus on metrics.*

Perhaps the easiest way to avoid overanalyzing management in an all-remote company is to focus on [results](/handbook/values/#results). Focusing on results over hours worked creates an atmosphere where colleagues direct effort on the [right things](/2018/09/07/mvcs-with-big-results/) — shipping [great code](/2019/03/20/enabling-global-search-elasticsearch-gitlab-com/), making a client happy, solving a teammate's problem, etc. 

This enables team members to complete their work and turn their attention to non-work activities (family, exercise, reading, caregiving, philanthrophy, etc.) as quickly as possible. 

By focusing on results, each team member has less of a mental burden to carry. They're aware that results are what matter as opposed to items like [personal success, status, and ego](/handbook/values/#five-dysfunctions). 

Learn more on GitLab's [Results value in our Handbook](/handbook/values/#results).

### Objectives and Key Results (OKRs)

All-remote companies should go beyond striving for results. They should add as much detail and clarity as possible to what those results are, and what is measured along the way. 

This can be achieved by implementing Objectives and Key Results (OKRs), a widely used framework for setting strategy and removing ambiguity over what matters. 

Learn more about [GitLab's implementation of OKRs](/company/okrs/). 

### Key Performance Indicators (KPIs)

Managing results requires clear communication of what's being measured. KPIs strip away guesswork and allow global teams to look at uniform data for making decisions. 

It's vital that all-remote companies make KPIs available to all. This not only helps each team member focus their efforts on driving results that move needles that matter, but it creates empathy. 

Although KPIs are emotionless, allowing teams to understand what other teams are working towards creates an atmopsphere of compassion, and enables team members to more easily offer help that is specific to an indicator. 

Learn more about [KPIs at GitLab](/handbook/ceo/kpis/).

----

Return to the main [all-remote page](/company/culture/all-remote/).
