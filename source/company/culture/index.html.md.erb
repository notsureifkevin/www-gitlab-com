---
layout: markdown_page
title: "GitLab Culture"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

Please see our [company page](/company/) for more general information about GitLab. You can see how our team has grown at the [GitLab Contribute page.](/company/culture/contribute)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/V2Z1h_2gLNU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Life at GitLab 

It’s an exciting time to be part of GitLab. We're a fast-growing, all-remote team, and we're looking for people to join us around the world. 
Here's a look at what you can expect from our culture and all-remote environment.

### Everyone can contribute 

Our size and [our mission](/company/strategy/#mission) (that everyone can contribute) mean that our team members can — and are expected to — make an impact across the company. 

Because we all use our product internally, you don't have to be a developer to learn to collaborate in the GitLab tool. 
From your very first week, no matter your role, you'll gain the technical skills needed to access, question, and contribute to projects far beyond your job description.  

This unique approach works because we're a team of helpful, passionate people who want to see each other, the company, and the broader GitLab community succeed. 
We learn from each other, challenge each other, and thank each other.     
   
Come prepared to do meaningful work that will help shape the future of the company. 

While the opportunities to contribute are boundless in a growing organization like GitLab, they may not be clearly defined. 
You'll need to think creativity, speak up to see how you can help, and be willing to try something new.   
 
### Freedom to iterate

At GitLab, our [value of iteration](/handbook/values/#iteration) has a unique impact on the way we operate and get things done. 

Working this way means our team members are expected to quickly deliver the minimun viable change in their work instead of waiting to produce a polished, completed product.

While this can be a challenging practice to adopt at first, it's liberating to be able to make mistakes, get feedback quickly, and course correct to reach a better outcome, faster. 

As our company and the industry continue to grow, you'll have the freedom to change and constantly evolve everyting from your schedule and your workspace to your job description and your skills. 


### All-remote work

Being part of our all-remote team offers unique advantages beyond the requisite flexibility you'll find in many organizations. 

As a GitLab team member, you can work from anywhere with good internet. Whether you’re an adventurer looking to travel the world while still pursuing your career, 
a parent or caregiver who wants a job that allows you to spend more time with family, or somewhere in between, you'll have the freedom to contribute when and where you do your best work.

But there's more to our all-remote culture than the daily flexibility it provides. 
By nature, having no offices or headquarters makes us more inclusive, more transparent, and more efficient in everything we do. 
With a team spread across almost 60 countries around the globe, we invite diverse perspectives, we document everything, and we collaborate asynchronously. 

Despite all of its benefits for team members, our company, and the world, remote work isn't for everyone. 
Learn more about [all-remote work](/company/culture/all-remote/) at GitLab and decide if it's right for you. 

## Advantages

<%= partial "includes/reasons_to_work_for_gitlab" %>

## Other pages related to culture

- [GitLab 101](/company/culture/gitlab-101/)
- [GitLab Contribute](/company/culture/contribute)
- [Internal Feedback](/company/culture/internal-feedback)
- [Diversity and Inclusion](/company/culture/inclusion)
- [Top Team Member](/company/culture/top-team-member)
- [All Remote](/company/culture/all-remote/)

## Historical Anecdotes

#### _October 8th, 2011_

Dmitriy started GitLab when he pushed the [initial commit](https://gitlab.com/gitlab-org/gitlab-ce/commit/9ba1224867665844b117fa037e1465bb706b3685).

#### _August 24th, 2012_

Sid announced [GitLab on HN](https://news.ycombinator.com/item?id=4428278).

#### _September 14th, 2012_

[First 10 people get access](/2012/09/14/first-10-people-got-access/)
to GitLab Cloud (now known as GitLab.com).

#### _November 13th, 2012_

[GitLab CI is officially announced](/2012/11/13/continuous-integration-server-from-gitlab/).

#### _July 22nd, 2013_

[GitLab Enterprise Edition is announced](/2013/07/22/announcing-gitlab-enterprise-edition/).

#### _April 18th, 2014_

[GitLab Cloud renamed to GitLab.com](/2014/04/18/gitlab-cloud-becomes-gitlab-com/).

#### _March 4th, 2015_

[GitLab in Y Combinator winter 2015 batch](/2015/03/04/gitlab-is-part-of-the-y-combinator-family/).

#### _August 15th, 2015_

Series A Funding was signed.

#### _October 10th, 2015_

Anniversary of our first ever summit in Amsterdam with 25 GitLab team-members.

## Team Stories

What better way to convey a sense of who we are and how we work together, than by sharing the stories about it?


### The Boat

<!-- HTML blocks below - applied to make the images and the video more harmonic than 1 single column with each on a different "row" -->

<br>

<div class="row">
  <div class="col-sm-8 col-xm-12"><img src="/images/blogimages/boat.jpg" alt="The Boat"></div>
  <div class="col-md-4 col-xm-12">
    <p><a href="/2016/01/06/our-y-combinator-experience/">Back then</a>, the whole team used to fit in one car. And the car was called "the Boat".</p>
  </div>
</div>

<br>

<div class="row">
  <div class="col-sm-8 col-xm-12">
    <figure class="video_container">
      <iframe src="https://www.youtube.com/embed/4TnKmrpiSgQ" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>
  </div>
  <div class="col-sm-4 col-xm-12">
    <p>We even took the Boat from San Francisco to Las Vegas to celebrate Job's bachelor party, but as you can see in this video, he thought we were going to visit a customer in Los Angeles!</p>
  </div>
</div>

<br>

### The cattle

<br>

<div class="row">
  <div class="col-md-4 col-xm-12"><img class="cattle" src="/images/cattle_stare.JPG" alt="Staring down cattle"></div>
  <div class="col-md-8 col-xm-12">
    <h4 class="media-heading">Staring down the cattle?</h4>
    <p class="justify-1">Our CFO, Paul, was on vacation on a cattle ranch,
    during a time of fundraising. Normally vacation is vacation of course, but
    in this case it was necessary to have some calls now and again which
    required strong internet. To get to strong internet, Paul had to cross
    fields with cattle in them, and stare them down. Over the course of many
    trips he learned that cattle are docile, mostly... but don't turn your back
    on them because they can't be outrun!</p>
  </div>
</div>

<br>

### IPO date comes in handy... 2 years out

<div class="row">
  <div class="col-sm-8 col-xm-12">
    <figure class="video_container">
      <iframe src="https://www.youtube.com/embed/4BIsON95fl8?start=1825" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>
  </div>
  <div class="col-sm-4 col-xm-12">
    <p>After spending a couple of days in meetings with customers in New York City, USA, Sid and Kirsten had a few hours before their flight and wanted to visit the WTC Observatory deck. It didn't work out but our IPO date did work out in their favor. In the keynote at our Cape Town event, Sid explains what happened.</p>
  </div>
</div>

<br>

### So that's what it's like to work at GitLab...

<div class="row">
  <div class="col-sm-8 col-xm-12">
    <figure class="video_container">
      <iframe src="https://www.youtube.com/embed/4BIsON95fl8?start=1143" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>
  </div>
  <div class="col-sm-4 col-xm-12">
    <p>Being new to GitLab, our CRO, Michael McBride joined Sid in meeting with customers in New York City, USA where customers got a glimpse of what it's like to work at GitLab for him</p>
  </div>
</div>

<br>

### Planned date of November 18th, 2020 to take GitLab public
Many people ask "why are you going public on November 18th, 2020?" November 18th 2020 was set for the following reasons:
* It is roughly 10 years after DZ started working on the project
* It is roughly 5 years after the first employees recieved 4 year stock option vesting schedules
* When determined to do a public offering in 2020, it was as late as possible in 2020, as markets do not typically move in December
* It was originally decided to be November 16th, which was the CFO's twins' birthday
* After determining a company shouldn't go public on a Monday, the date was moved Wednesday, November 18th, 2020
* This date also coincides with Sid's grandfather’s 100th birthday

<!-- particular styles for the pictures and video at the beginning -->

<style>
.justify,.justify-1 { text-align: justify; }
.media-heading { margin-bottom: 5px; }

@media (max-width: 767px) {
.col-sm-8 h4,.justify { padding-top: 10px; }
}
@media (max-width: 991px) {
.col-md-8 h4,.justify-1 { padding-top: 10px; }
}
</style>
