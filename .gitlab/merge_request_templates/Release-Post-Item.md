Engineer(s): `@engineers` | Product Marketing: `@PMM` | Tech Writer: `@techwriter`

### Content checklist

* [ ] YML filled out for feature
* [ ] Description is informative and focuses on the problem we're solving and the value we're delivering
* [ ] Documentation is updated and linked
* [ ] Screenshot/video is included (optional for secondary items)

### Review

When the above is complete and the content is ready for review, it should be reviewed by Product Marketing and Tech Writing:

* [ ]  PMM review
* [ ]  Tech writing

#### Checklist for reference

* Make sure feature description is positive and cheerful
* Check Feature name
* Check Feature availability (Core, Starter, Premium, Ultimate badges)
* Feature is added to [`data/features.yml`](https://about.gitlab.com/handbook/marketing/website/#adding-features-to-webpages) (with accompanying screenshots)
* Check all images size < 300KB ([compressed](https://tinypng.com/), max width 1000 pixels)
* Check that documentation is updated and easy to understand
* Check Documentation links (all feature blocks contain `documentation_link`)
* Run the content through an automated spelling and grammar check
* Validate all links are functional

### References (optional)

* Issue:
* Release post:

/label ~"release post" `<DEVOPS::STAGE>` `<GROUP::NAME>`
/milestone `<RELEASE_MILESTONE>`