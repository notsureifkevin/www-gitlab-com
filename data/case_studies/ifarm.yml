title: iFarm
cover_image: '/images/blogimages/ifarm-case-study-image.png'
cover_title: |
  iFarm plants the seeds for operational efficiency
cover_description: |
  Committed to simplifying farming operations, iFarm uses the GitLab + Bugsee integration to grow end-to-end visibility and communication
twitter_image: '/images/blogimages/ifarm-case-study-image.png'

customer_logo: 'images/case_study_logos/ifarm-logo.svg'
customer_logo_css_class: brand-logo-tall
customer_industry: Agriculture Technology
customer_location: Seattle, Washington, USA
customer_employees: 2-5
customer_overview: |
  A software application aims to simplify day-to-day farming operations by tracking harvests and crop protection methods to increase efficiency and productivity.
customer_challenge: |
  Improving the quality of software when faced with user language barriers and a lack of development visibility.

key_benefits:
  - |
    Increased app development and iterations
  - |
    Full visibility into crashes and user navigation
  - |
    Streamlined issue tracking increases productivity

customer_stats:
  - stat: 63
    label: automatically-reported issues without user involvement
  - stat: 424
    label: hours saved for each "bug to resolution" time
  - stat: 7.5x
    label: faster engineering time per bug

customer_study_content:
  - title: the customer
    subtitle: iFarm develops agriculture technology to increase farming efficiency
    content:
     - |
       As the son of cherry farmers, Will MacHugh, understands the importance of
       optimizing farming operations to increase productivity. Successfully
       farming and harvesting large areas of land is a challenging feat that can
       be made insurmountable with a lack of efficient processes. Drawing upon
       his upbringing and childhood memories of tracking boxes of handpicked
       cherries, MacHugh, Founder and President of Elitopia Communications,
       worked with his team at iFarm to use technology to help farmers streamline
       operations.
     - |
       iFarm develops agriculture technology, leveraging the mobile network, to
       record the application of products to various crops throughout a field,
       such as herbicides and fertilizers, and tracks harvest boxes, quantities
       that can be difficult to manage with handpicked harvests. The inefficiency
       of tracking such operations by hand led iFarm to develop an application
       that allows farmhands to quickly input information while they are working
       in the fields, allowing individuals to quickly resume their efforts rather
       than engaging in long pauses to complete calculations and mark geographic
       areas.
     - |
       Aiming to simplify operations, iFarm creates a bridge between farmhands
       in the fields with decision makers behind desks.

  - title: the challenge
    subtitle: Language barriers and visibility issues stymie productivity and app development
    content:
      - |
        As iFarm grew, MacHugh hired a team of global developers to improve
        functionality, but after encountering a lack of visibility into the
        development lifecycle and an inability to fix recurring bugs, he traveled
        to India to meet with the team. “I spent a lot of time trying to
        understand the problems," says MacHugh, "and the gaps I found were a
        solid source control system and a communications tool to identify bugs.”
        MacHugh learned that his off-site team coded without knowledge of how it
        was to be used, saying, "We got a product out, but it was barely usable."
        The off-site team had difficulty understanding the software and did not
        know how to test whether their efforts produced meaningful results. As
        MacHugh explains, "This was a constant problem. I wouldn’t know what the
        team had actually done, and I didn’t know where the software had crashed.
        It was just a constant communications issue."
      - |
        Compounding the communications issues within the team, iFarm experience
        language barriers with the end user. Because farmhands are often
        non-English speaking migrant workers, any crashes or bugs could not be
        communicated in a way that included the technical steps and errors
        experienced. As MacHugh describes, "We have a non-English speaker who
        experiences a software crash in the field, and he restarts the app, but
        that information is never recorded, so the developer never knows what
        led to that bug." In situations in which the steps were communicated,
        technical aspects were lost in translation, since information needed to
        pass from Spanish to English to Hindi where it finally arrived to the
        developer.
      - |
       How could iFarm improve the quality of software when faced with users who
       experience difficulty communicating their crashes and a development team
       that struggles to effectively and efficiently code?

  - title: the solution
    subtitle: GitLab + Bugsee create a full circle development cycle
    content:
      - |
       MacHugh hoped to find a single solution that could both streamline
       technical support to resolve end user communication issues while offering
       his developers a way to easily iterate. As a first step, MacHugh spoke
       with the development team to learn of their preferences. "When I asked my
       developers what they wanted to use, they suggested GitLab, so I spent more
       time with GitLab, and thought, 'Yes, this does a good job. It's got all
       the right communications tools in it, I can put bugs in it, and I can do
       all these great things.' I was really satisfied." Using GitLab for source
       control management, the team had full visibility into the entire software
       development lifecycle. Developers could easily test code and MacHugh could
       see iterations in real time.

       ![GitLab + Bugsee](/images/blogimages/gitlab-bugsee-ifarm-case-study.svg){: .small.center}
      - |
        While GitLab solved visibility issues, MacHugh still struggled with end
        user communication problems, and he sought to take out the user when
        reporting crashes and bugs. "We had to find a better solution to help the
        end user convey that he's having a problem. So, I looked at different
        solutions, and I really started looking at Bugsee." [Bugsee](https://www.bugsee.com)
        is a mobile software kit that monitors and records the state of a system
        in the background of a user's mobile device and quietly transmits the
        information back to the Bugsee development team. The developer receives
        all the build and system relevant information, video of users' actions,
        console logs, network logs, and all system events, allowing him to
        determine what led to crashes. "Bugsee eliminates the difficult,
        time-consuming task of bug and crash reporting," explains MacHugh.

  - blockquote: Bugsee is clean and ubiquitous, operating in the background so that users never see it.
    attribution: Will MacHugh
    attribution_title: Founder and President

  - content:
      - |
        With Bugsee, iFarm could gain immediate insight into crashes, overcoming
        end user language hurdles. To bring developers even closer to crash
        reports, MacHugh relied on the GitLab + Bugsee integration, which
        "fluidly transfers Bugsee data into a GitLab issue so that everything
        that is ever needed to diagnose a crash is given directly to an engineer."
        GitLab's interface enables the entire iFarm team to have full visibility
        into Bugsee reports. “Anyone on the team can look at the issues and
        designate a fix as a priority, allowing us to produce a much better
        product," says MacHugh.

  - blockquote: The integration fluidly transfers Bugsee data into a GitLab issue so that everything that is ever needed to diagnose a crash is given directly to an engineer.
    attribution: Will MacHugh
    attribution_title: Founder and President

  - content:
      - |
        As Bugsee runs in the background, collecting information and sending
        crash reports to GitLab, end users no longer have to call iFarm to share
        software problems or ensure that issues are prioritized. MacHugh
        discovered that customers appreciate that their bugs are automatically
        reported by Bugsee and identified in GitLab, giving them confidence that
        their concerns have been prioritized and placed on the stack for engineers
        without their having to go through customer support to share their
        difficulties.
      - |
        The GitLab + Bugsee integration eliminates communication steps, allowing
        farmhands to continue using the app while iFarm analyzes crash reports
        and prioritizes fixes to re-release in an upcoming cycle, offering a new
        level of visibility and bringing the development cycle full circle. As
        MacHuh explains, "With the integration, our users no longer have to make
        a technical support engine incident, which would then be escalated to
        determine what is going on. This communication channel has end-to-end
        visibility from user experience to engineer. No bypass. No filter. Now,
        when software crashes out in the field, we know exactly which buttons
        were pressed, and that information is relayed directly to engineers. All
        the language barriers we experienced evaporated overnight."

  - blockquote: GitLab and Bugsee, as a combination, has been absolutely fantastic.
    attribution: Will MacHugh
    attribution_title: Founder and President

  - title: the results
    subtitle: End-to-end visibility and communication cultivates iteration and productivity
    content:
      - |
        MacHugh describes iFarm's experience with the GitLab + Bugsee integration
        by saying, "It has taken our tiny software team of four people and has
        given us comprehensive, end-to-end communications and testing, so the
        quality of the product is comfortable, and I’m completely confident in
        both Bugsee and GitLab."
      - |
        The integration has increased iFarm's app development and ability to
        iterate, since "it narrowed the communication gaps and helped engineers
        focus on specific bugs, helping the team accelerate app development."
        The end-to-end visibility has made full regression testing a possibility,
        which MacHugh says is responsible for "a couple hundred iterations before
        software is ever released."

  - blockquote: The benefits we gained from having full visibility into detailed crash reports and generating issues made the difference between survival and loss.
    attribution: Will MacHugh
    attribution_title: Founder and President

  - content:
      - |
        The integration between Bugsee's comprehensive crash reports and GitLab's
        source control management has increased the team's productivity. "Instead
        of going through four hands, the data goes straight from a crash to the
        developer who needs to fix it. The GitLab + Bugsee integration helped
        both our development cycle and our product get inspirationally faster in
        under a week," explains MacHugh.

  - blockquote: The GitLab + Bugsee integration helped both our development cycle and our product get inspirationally faster in under a week.
    attribution: Will MacHugh
    attribution_title: Founder and President

  - content:
      - |
        Dedicated to providing a seamless user experience to increase operational
        efficiency on farms, iFarm uses GitLab + Bugsee to achieve end-to-end
        visibility and communications.
